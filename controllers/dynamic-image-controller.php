<?php

global $REQUEST_PATHS;

$filename = BASE_PATH . '/' . implode('/', array_slice($REQUEST_PATHS, 3));

$request_query_string = null;

parse_str(QUERY_STRING, $request_query_string);

if (!file_exists($filename)) {
    header("HTTP/1.0 404 Not Found");
    exit;
}

//header('Content-Type: image/jpeg');
//header("Expires: Sat, 26 Jul " . (date('Y') + 1) . " 05:00:00 GMT");

$src = ImageCreateFromJPEG($filename);
$width = ImageSx($src);
$height = ImageSy($src);

$thumb = new easyphpthumbnail;
$thumb->Backgroundcolor = '#0000FF';
$thumb->Clipcorner = array(2, 50, 0, 1, 1, 1, 1);
$thumb->Maketransparent = array(1, 1, '#0000FF', 30);
$thumb->Createthumb($filename);

if (isset($request_query_string['h']) && isset($request_query_string['w'])) {
    $src = ImageCreateFromJPEG($filename);
    $width = ImageSx($src);
    $height = ImageSy($src);
    $x = $width / $request_query_string['w'];
    $y = $height / $request_query_string['h'];
    $new_width = 0;
    $new_height = 0;
    if ($y > $x) {
        $new_width = round($width * (1 / $y));
        $new_height = round($height * (1 / $y));
    } else {
        $new_width = round($width * (1 / $x));
        $new_height = round($height * (1 / $x));
    }
    $dst = ImageCreateTrueColor($new_width, $new_height);
    ImageCopyResampled($dst, $src, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
    imagejpeg($dst, null, 80);
}
