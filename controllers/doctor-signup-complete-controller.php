<?php

use HealthCare\Site\Impl\FrontendService;

global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;

$values['url'] = "doctor-signup-complete";
$page_result = FrontendService::GetPageByUrl($values);

if (!$page_result['status']) {
    header('location: ' . CONTEXT_PATH . '/error');
    exit;
}

$REQUEST_ATTRIBUTES['page'] = $page_result['page'];

require BASE_PATH . '/views/doctor-signup-complete.php';

