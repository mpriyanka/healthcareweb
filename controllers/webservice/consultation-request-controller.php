<?php

use HealthCare\App\Impl\ConsultationRequestService;
use HealthCare\Util\Util;

global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;

header('Content-Type: application/json');

$doctor_id = filter_input(INPUT_POST, "doctor_id", FILTER_SANITIZE_STRING);
$phone = filter_input(INPUT_POST, "phone", FILTER_SANITIZE_STRING);
$message = filter_input(INPUT_POST, "message", FILTER_SANITIZE_STRING);
$hash_token = filter_input(INPUT_POST, "hash_token", FILTER_SANITIZE_STRING);

if (isset($doctor_id) && isset($phone) && isset($message) && isset($hash_token)) {
    
    $values['doctor_id'] = $doctor_id;
    $values['phone'] = $phone;
    $values['message'] = $message;
    $values['hash_token'] = $hash_token;
    
    /*if (!Util::VerifyRequestHashToken($values)) {
        
        $result['status'] = 3;

        echo json_encode($result);
        exit;
    }*/
    
    $values = array_map('trim', $values);
    
    $result_consulation_request = ConsultationRequestService::CreateConsultationRequestWS($values);

    echo json_encode($result_consulation_request, true);
} else {
    $result['status'] = 2;

    echo json_encode($result);
}
