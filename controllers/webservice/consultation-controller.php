<?php

use HealthCare\App\Impl\ConsultationService;
use HealthCare\Util\Util;

global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;

header('Content-Type: application/json');

$username = filter_input(INPUT_POST, "username", FILTER_SANITIZE_STRING);
$user_id = filter_input(INPUT_POST, "user_id", FILTER_SANITIZE_STRING);
$remote_doctor_id = filter_input(INPUT_POST, "remote_doctor_id", FILTER_SANITIZE_STRING);
$start_date = filter_input(INPUT_POST, "start_date", FILTER_SANITIZE_STRING);
$hash_token = filter_input(INPUT_POST, "hash_token", FILTER_SANITIZE_STRING);

if (isset($username) && isset($user_id) && isset($remote_doctor_id) && isset($start_date) && isset($hash_token)) {
    
    $values['username'] = $username;
    $values['user_id'] = $user_id;
    $values['remote_doctor_id'] = $remote_doctor_id;
    $values['start_date'] = $start_date;
    $values['hash_token'] = $hash_token;
    
    /*if (!Util::VerifyRequestHashToken($values)) {
        
        $result['status'] = 3;

        echo json_encode($result);
        exit;
    }*/
    
    $values = array_map('trim', $values);
    
    $result_consulation = ConsultationService::CreateConsultationWS($values);

    echo json_encode($result_consulation, true);
} else {
    $result['status'] = 2;

    echo json_encode($result);
}
