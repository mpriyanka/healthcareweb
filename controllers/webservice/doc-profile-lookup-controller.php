<?php

use HealthCare\Site\Impl\DoctorService;
use HealthCare\Util\Util;

global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;

if (isset($REQUEST_PATHS[2])) {

    $values['id'] = $REQUEST_PATHS[2];

    $values = array_map('trim', $values);

    $result_doctor = DoctorService::GetDoctor($values);

    if ($result_doctor['status']) {
        $REQUEST_ATTRIBUTES['profile'] = $result_doctor['profile'];
        
        require BASE_PATH.'/views/webservice/doc-profile.php';
    } else {
        echo "Doctor not profile not found";
    }
} else {
    echo "Invalid Request";
}
