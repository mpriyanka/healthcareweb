<?php

use HealthCare\Site\Impl\DoctorService;
use HealthCare\Util\Util;

global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;

header('Content-Type: application/json');

$username = filter_input(INPUT_POST, "username", FILTER_SANITIZE_EMAIL);
$password = filter_input(INPUT_POST, "password", FILTER_SANITIZE_STRING);
$hash_token = filter_input(INPUT_POST, "hash_token", FILTER_SANITIZE_STRING);

if (isset($username) && isset($password) && isset($hash_token)) {

    $values['username'] = $username;
    $values['password'] = $password;
    $values['hash_token'] = $hash_token;

    /*if (!Util::VerifyRequestHashToken($values)) {
        
        $result['status'] = 3;

        echo json_encode($result);
        exit;
    }*/

    $values = array_map('trim', $values);
    
    $result_login = DoctorService::LoginWS($values);

    echo json_encode($result_login);
} else {
    $result['status'] = 2;

    echo json_encode($result);
}
