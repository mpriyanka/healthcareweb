<?php

use HealthCare\App\Impl\AppUserService;
use HealthCare\Util\Util;

global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;

header('Content-Type: application/json');

$phone = filter_input(INPUT_POST, "phone", FILTER_SANITIZE_STRING);
$firstname = filter_input(INPUT_POST, "firstname", FILTER_SANITIZE_STRING);
$lastname = filter_input(INPUT_POST, "lastname", FILTER_SANITIZE_STRING);
$dob = filter_input(INPUT_POST, "dob", FILTER_SANITIZE_STRING);
$hash_token = filter_input(INPUT_POST, "hash_token", FILTER_SANITIZE_STRING);

if (isset($phone) && isset($firstname) && isset($lastname) && isset($dob) && isset($hash_token)) {
    
    $values['firstname'] = $firstname;
    $values['lastname'] = $lastname;
    $values['dob'] = $dob;
    $values['phone'] = $phone;
    $values['hash_token'] = $hash_token;
    
    /*if (!Util::VerifyRequestHashToken($values)) {
        
        $result['status'] = 3;

        echo json_encode($result);
        exit;
    }*/
    
    $values = array_map('trim', $values);
    
    $result_app_user = AppUserService::CreateAppUserWS($values);

    echo json_encode($result_app_user);
} else {
    $result['status'] = 2;

    echo json_encode($result);
}
