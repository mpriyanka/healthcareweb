<?php

use HealthCare\App\Impl\ConsultationChatMessageService;
use HealthCare\Util\Util;

global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;

header('Content-Type: application/json');

$username = html_entity_decode(filter_input(INPUT_POST, "username", FILTER_SANITIZE_STRING));
$creation_date = html_entity_decode(filter_input(INPUT_POST, "creation_date", FILTER_SANITIZE_STRING));
$image_data = html_entity_decode(filter_input(INPUT_POST, "image_data", FILTER_SANITIZE_STRING));
$hash_token = html_entity_decode(filter_input(INPUT_POST, "hash_token", FILTER_SANITIZE_STRING));

if (isset($username) && isset($creation_date) && isset($image_data) && isset($hash_token)) {

    $values['username'] = $username;
    $values['creation_date'] = $creation_date;
    $values['image_data'] = $image_data;
    $values['hash_token'] = $hash_token;

    /* if (!Util::VerifyRequestHashToken($values)) {

      $result['status'] = 3;

      echo json_encode($result);
      exit;
      } */

    $values = array_map('trim', $values);

    $result_ccm = ConsultationChatMessageService::UploadCMMImageDoctorWS($values);

    echo json_encode($result_ccm, true);
} else {
    $result['status'] = 2;

    echo json_encode($result);
}
