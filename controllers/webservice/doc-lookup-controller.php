<?php

use HealthCare\Site\Impl\DoctorService;
use HealthCare\Util\Util;

global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;

header('Content-Type: application/json');

$index = filter_input(INPUT_GET, "index", FILTER_SANITIZE_NUMBER_INT);
$size = filter_input(INPUT_GET, "size", FILTER_SANITIZE_NUMBER_INT);
$hash_token = filter_input(INPUT_GET, "hash_token", FILTER_SANITIZE_STRING);

if (isset($index) && isset($size) && isset($hash_token)) {

    $values['index'] = $index;
    $values['size'] = $size;
    $values['hash_token'] = $hash_token;

    /*if (!Util::VerifyRequestHashToken($values)) {
        
        $result['status'] = 3;

        echo json_encode($result);
        exit;
    }*/

    $values = array_map('trim', $values);

    $result_doctors = DoctorService::GetDoctorsWS($values);

    if ($result_doctors['status']) {
        $REQUEST_ATTRIBUTES['doctors'] = $result_doctors['doctors'];
        $REQUEST_ATTRIBUTES['pagination'] = $result_doctors['pagination'];
        
        require BASE_PATH.'/views/webservice/doc-lookup.php';
    } else {
        $result['status'] = 0;

        echo json_encode($result);
    }
} else {
    $result['status'] = 2;

    echo json_encode($result);
}
