<?php

use HealthCare\Util\Util;

global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;

header('Content-Type: application/json');

$phone = filter_input(INPUT_GET, "phone", FILTER_SANITIZE_SPECIAL_CHARS);
$random_number = filter_input(INPUT_GET, "random_number", FILTER_SANITIZE_NUMBER_INT);
$hash_token = filter_input(INPUT_GET, "hash_token", FILTER_SANITIZE_STRING);

if (isset($phone) && isset($random_number) && isset($hash_token)) {

    $values['phone'] = $phone;
    $values['random_number'] = $random_number;
    $values['hash_token'] = $hash_token;

    /*if (!Util::VerifyRequestHashToken($values)) {
        
        $result['status'] = 3;

        echo json_encode($result);
        exit;
    }*/

    $sender = "iDocare";
    $message = SITE_NAME . ". Your verification code: " . $random_number;

    $header = array();
    $header[] = 'Content-type: application/json';
    $header[] = 'Accept: application/json';
    $header[] = 'Authorization: Basic ' . base64_encode("Genesisville:Happy1234");

    $data['from'] = $sender;
    $data['to'] = $phone;
    $data['text'] = str_replace("+", " ", $message);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.infobip.com/sms/1/text/single");
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    //curl_exec($ch);
    //curl_close($ch);

    $result['status'] = 1;

    echo json_encode($result);
} else {
    $result['status'] = 2;

    echo json_encode($result);
}
