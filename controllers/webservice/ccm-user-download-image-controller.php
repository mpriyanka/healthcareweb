<?php

use HealthCare\App\Impl\ConsultationChatMessageService;
use HealthCare\Util\Util;

global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;

header('Content-Type: application/json');

$phone = html_entity_decode(filter_input(INPUT_POST, "phone", FILTER_SANITIZE_STRING));
$remote_id = html_entity_decode(filter_input(INPUT_POST, "remote_id", FILTER_SANITIZE_STRING));
$hash_token = html_entity_decode(filter_input(INPUT_POST, "hash_token", FILTER_SANITIZE_STRING));

if (isset($phone) && isset($remote_id) && isset($hash_token)) {
    
    $values['phone'] = $phone;
    $values['remote_id'] = $remote_id;
    $values['hash_token'] = $hash_token;
    
    /*if (!Util::VerifyRequestHashToken($values)) {
        
        $result['status'] = 3;

        echo json_encode($result);
        exit;
    }*/
    
    $values = array_map('trim', $values);
    
    $result_ccm = ConsultationChatMessageService::DownloadCCMImageUserWS($values);

    echo json_encode($result_ccm, true);
} else {
    $result['status'] = 2;

    echo json_encode($result);
}
