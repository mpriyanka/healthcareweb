<?php

use HealthCare\Util\Util;

global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;

header('Content-Type: application/json');

$username = filter_input(INPUT_POST, "username", FILTER_SANITIZE_EMAIL);
$gcm_token = filter_input(INPUT_POST, "gcm_token", FILTER_SANITIZE_STRING);
$hash_token = filter_input(INPUT_POST, "hash_token", FILTER_SANITIZE_STRING);

if (isset($username) && isset($gcm_token) && isset($hash_token)) {

    $values['username'] = $username;
    $values['gcm_token'] = $gcm_token;
    $values['hash_token'] = $hash_token;

    /*if (!Util::VerifyRequestHashToken($values)) {
        
        $result['status'] = 3;

        echo json_encode($result);
        exit;
    }*/

    $values = array_map('trim', $values);
    
    $result_update_token = Util::UpdateDoctorGCMTokenWS($values);

    echo json_encode($result_update_token);
} else {
    $result['status'] = 2;

    echo json_encode($result);
}
