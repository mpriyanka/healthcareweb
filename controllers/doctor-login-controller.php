<?php

use HealthCare\Site\Impl\DoctorService;

global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;

$username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
$password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);

if (DoctorService::IsAuthenticated()) {
    DoctorService::Logout();
} else if (isset($username) && isset($password)) {
    $login_result = DoctorService::Login($username, $password, CONTEXT_PATH.'/doctors');
    $REQUEST_ATTRIBUTES = $login_result;
}

require BASE_PATH . '/views/doctor-login.php';
