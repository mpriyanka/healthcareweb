<?php

use HealthCare\Site\Impl\DoctorService;
use HealthCare\Site\Impl\FrontendService;
use HealthCare\Util\Impl\CountryService;

global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;


$firstname = filter_input(INPUT_POST, 'firstname', FILTER_SANITIZE_STRING);
$middlename = filter_input(INPUT_POST, 'middlename', FILTER_SANITIZE_STRING);
$lastname = filter_input(INPUT_POST, 'lastname', FILTER_SANITIZE_STRING);
$phone = filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_STRING);
$mobile = filter_input(INPUT_POST, 'mobile', FILTER_SANITIZE_STRING);
$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
$line_1 = filter_input(INPUT_POST, 'line_1', FILTER_SANITIZE_STRING);
$line_2 = filter_input(INPUT_POST, 'line_2', FILTER_SANITIZE_STRING);
$city = filter_input(INPUT_POST, 'city', FILTER_SANITIZE_STRING);
$state = filter_input(INPUT_POST, 'state', FILTER_SANITIZE_STRING);
$country = filter_input(INPUT_POST, 'country', FILTER_SANITIZE_STRING);

if (isset($firstname) && isset($middlename) && isset($lastname) && isset($phone) && isset($mobile) && isset($email) && isset($line_1) && isset($line_2) && isset($city) && isset($state) && isset($country)) {

    $values['firstname'] = $firstname;
    $values['middlename'] = $middlename;
    $values['lastname'] = $lastname;
    $values['phone'] = $phone;
    $values['mobile'] = $mobile;
    $values['email'] = $email;
    $values['line_1'] = $line_1;
    $values['line_2'] = $line_2;
    $values['city'] = $city;
    $values['state'] = $state;
    $values['country'] = $country;

    $values = array_map('trim', $values);

    $result_doctor = DoctorService::CreateDoctor($values);

    if ($result_doctor['status']) {
        header("location: " . CONTEXT_PATH . '/doctor-signup/complete');
        exit;
    }

    $REQUEST_ATTRIBUTES['profile'] = $result_doctor['profile'];
    $REQUEST_ATTRIBUTES['address'] = $result_doctor['address'];
    $REQUEST_ATTRIBUTES['status'] = $result_doctor['status'];
    $REQUEST_ATTRIBUTES['error-msg'] = $result_doctor['error-msg'];
} else {
    $result_doctor = DoctorService::GetCreateDoctorTemplate();

    $REQUEST_ATTRIBUTES['profile'] = $result_doctor['profile'];
    $REQUEST_ATTRIBUTES['address'] = $result_doctor['address'];
}

$values['url'] = "pre-doctor-application";
$page_result = FrontendService::GetPageByUrl($values);

$country_result = CountryService::GetCountryNames();

if (!$page_result['status'] || !$country_result['status']) {
    header('location: ' . CONTEXT_PATH . '/error');
    exit;
}

$REQUEST_ATTRIBUTES['page'] = $page_result['page'];
$REQUEST_ATTRIBUTES['country-names'] = $country_result['country-names'];

require BASE_PATH . '/views/doctor-signup.php';