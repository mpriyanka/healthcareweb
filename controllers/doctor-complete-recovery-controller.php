<?php

use HealthCare\Site\Impl\DoctorService;


global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;

if(isset($REQUEST_PATHS[1]) && isset($REQUEST_PATHS[2])){
    $values['id'] = $REQUEST_PATHS[1];
    $values['hash'] = $REQUEST_PATHS[2];
    DoctorService::CompleteRecovery($values, CONTEXT_PATH.'/doctors/change-password');
}
