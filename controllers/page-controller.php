<?php

use HealthCare\Site\Impl\FrontendService;
use HealthCare\Widgets\GalleryWidget;

global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;

$values['url'] = $REQUEST_PATHS[count($REQUEST_PATHS) - 3];
$page_result = FrontendService::GetPageByUrl($values);

if (!$page_result['status']) {
    header('location: ' . CONTEXT_PATH . '/error');
    exit;
}

$page = $page_result['page'];

$tags = explode(',', $page->getValue('tags'));

if (in_array("Gallery", $tags)) {
    $body = $page->getValue('body');
    GalleryWidget::LoadWidgets($body);
    $page->setValue('body', $body);
}

$REQUEST_ATTRIBUTES['page'] = $page;

require BASE_PATH . '/views/page.php';
