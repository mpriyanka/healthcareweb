<?php

use HealthCare\Site\Impl\PageService;
use HealthCare\User\Impl\UserService;

require 'require-login.php';

global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;

UserService::AssertPermission("Manage Page|View Page", null, CONTEXT_PATH . "/access-denied");

if (isset($REQUEST_PATHS[3])) {
    UserService::AssertPermission("Manage Page", null, CONTEXT_PATH . "/access-denied");
    switch ($REQUEST_PATHS[3]) {
        case "create":
            $tags = filter_input(INPUT_POST, 'tags', FILTER_SANITIZE_STRING);
            $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            $url = filter_input(INPUT_POST, 'url', FILTER_SANITIZE_STRING);
            $type = filter_input(INPUT_POST, 'type', FILTER_SANITIZE_STRING);
            $keywords = filter_input(INPUT_POST, 'keywords', FILTER_SANITIZE_STRING);

            if (isset($tags) && isset($title) && isset($url) && isset($keywords) && isset($type)) {

                $values['tags'] = $tags;
                $values['type'] = $type;
                $values['keywords'] = $keywords;
                $values['title'] = $title;
                $values['url'] = str_replace(" ", "-", strtolower($url));

                $values = array_map('trim', $values);

                $page_result = PageService::CreatePage($values);

                $REQUEST_ATTRIBUTES['page'] = $page_result['page'];
                $REQUEST_ATTRIBUTES['status'] = $page_result['status'];
                $REQUEST_ATTRIBUTES['error-msg'] = $page_result['error-msg'];
                $REQUEST_ATTRIBUTES['view'] = "create.php";
                $REQUEST_ATTRIBUTES['title'] = "Create Page";
            } else {
                $page_result = PageService::GetCreatePageTemplate();
                if ($page_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['page'] = $page_result['page'];
                    $REQUEST_ATTRIBUTES['view'] = "create.php";
                    $REQUEST_ATTRIBUTES['title'] = "Create Page";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        case "edit":
            if (!isset($REQUEST_PATHS[4])) {
                header('location: ' . CONTEXT_PATH . '/user-generated-error');
                exit;
            }

            $values['id'] = $REQUEST_PATHS[4];

            $tags = filter_input(INPUT_POST, 'tags', FILTER_SANITIZE_STRING);
            $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            $url = filter_input(INPUT_POST, 'url', FILTER_SANITIZE_STRING);
            $type = filter_input(INPUT_POST, 'type', FILTER_SANITIZE_STRING);
            $keywords = filter_input(INPUT_POST, 'keywords', FILTER_SANITIZE_STRING);

            if (isset($tags) && isset($title) && isset($url) && isset($keywords) && isset($type)) {

                $values['tags'] = $tags;
                $values['type'] = $type;
                $values['keywords'] = $keywords;
                $values['title'] = $title;
                $values['url'] = str_replace(" ", "-", strtolower($url));

                $values = array_map('trim', $values);

                $page_result = PageService::UpdatePage($values);

                $REQUEST_ATTRIBUTES['page'] = $page_result['page'];
                $REQUEST_ATTRIBUTES['status'] = $page_result['status'];
                $REQUEST_ATTRIBUTES['error-msg'] = $page_result['error-msg'];
                $REQUEST_ATTRIBUTES['view'] = "edit.php";
                $REQUEST_ATTRIBUTES['title'] = "Edit Page";
            } else {
                $values['id'] = $REQUEST_PATHS[4];

                $page_result = PageService::GetPage($values);
                if ($page_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['page'] = $page_result['page'];
                    $REQUEST_ATTRIBUTES['view'] = "edit.php";
                    $REQUEST_ATTRIBUTES['title'] = "Edit Page";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        case "edit-page-body":
            if (!isset($REQUEST_PATHS[4])) {
                header('location: ' . CONTEXT_PATH . '/user-generated-error');
                exit;
            }

            $values['id'] = $REQUEST_PATHS[4];

            $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
            $body = htmlspecialchars(filter_input(INPUT_POST, 'body', FILTER_SANITIZE_MAGIC_QUOTES));

            if (isset($id) && isset($body)) {

                $values['id'] = $id;
                $values['body'] = $body;

                $values = array_map('trim', $values);

                $page_result = PageService::UpdatePageBody($values);

                $REQUEST_ATTRIBUTES['page'] = $page_result['page'];
                $REQUEST_ATTRIBUTES['status'] = $page_result['status'];
                $REQUEST_ATTRIBUTES['error-msg'] = $page_result['error-msg'];
                $REQUEST_ATTRIBUTES['view'] = "edit-page-body.php";
                $REQUEST_ATTRIBUTES['title'] = "Edit Page";
            } else {

                $page_result = PageService::GetPage($values);
                if ($page_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['page'] = $page_result['page'];
                    $REQUEST_ATTRIBUTES['view'] = "edit-page-body.php";
                    $REQUEST_ATTRIBUTES['title'] = "Edit Page";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        case "publish":
            if (!isset($REQUEST_PATHS[4])) {
                header('location: ' . CONTEXT_PATH . '/user-generated-error');
                exit;
            }

            $values['id'] = $REQUEST_PATHS[4];

            $confirmed = filter_input(INPUT_POST, 'confirmed', FILTER_SANITIZE_STRING);

            if (isset($confirmed)) {

                $values['published'] = 1;

                $values = array_map('trim', $values);

                $page_result = PageService::UpdatePublishStatus($values);

                $REQUEST_ATTRIBUTES['page'] = $page_result['page'];
                $REQUEST_ATTRIBUTES['status'] = $page_result['status'];
                $REQUEST_ATTRIBUTES['error-msg'] = $page_result['error-msg'];
                $REQUEST_ATTRIBUTES['view'] = "publish.php";
                $REQUEST_ATTRIBUTES['title'] = "Page Publishing";
            } else {

                $page_result = PageService::GetPage($values);
                if ($page_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['page'] = $page_result['page'];
                    $REQUEST_ATTRIBUTES['view'] = "confirm-publish.php";
                    $REQUEST_ATTRIBUTES['title'] = "Confirm Publishing";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        case "unpublish":
            if (!isset($REQUEST_PATHS[4])) {
                header('location: ' . CONTEXT_PATH . '/user-generated-error');
                exit;
            }

            $values['id'] = $REQUEST_PATHS[4];

            $confirmed = filter_input(INPUT_POST, 'confirmed', FILTER_SANITIZE_STRING);

            if (isset($confirmed)) {

                $values['published'] = 0;

                $values = array_map('trim', $values);

                $page_result = PageService::UpdatePublishStatus($values);

                $REQUEST_ATTRIBUTES['page'] = $page_result['page'];
                $REQUEST_ATTRIBUTES['status'] = $page_result['status'];
                $REQUEST_ATTRIBUTES['error-msg'] = $page_result['error-msg'];
                $REQUEST_ATTRIBUTES['view'] = "unpublish.php";
                $REQUEST_ATTRIBUTES['title'] = "Page UnPublishing";
            } else {

                $page_result = PageService::GetPage($values);
                if ($page_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['page'] = $page_result['page'];
                    $REQUEST_ATTRIBUTES['view'] = "confirm-unpublish.php";
                    $REQUEST_ATTRIBUTES['title'] = "Confirm UnPublishing";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        case "delete":
            if (!isset($REQUEST_PATHS[4])) {
                header('location: ' . CONTEXT_PATH . '/user-generated-error');
                exit;
            }

            $values['id'] = $REQUEST_PATHS[4];

            $confirmed = filter_input(INPUT_POST, 'confirmed', FILTER_SANITIZE_STRING);
            if (isset($confirmed)) {
                $page_result = PageService::DeletePage($values);

                $REQUEST_ATTRIBUTES['status'] = $page_result['status'];
                $REQUEST_ATTRIBUTES['error-msg'] = $page_result['error-msg'];
                $REQUEST_ATTRIBUTES['view'] = "delete.php";
                $REQUEST_ATTRIBUTES['title'] = "Delete Page";
            } else {

                $page_result = PageService::GetPage($values);
                if ($page_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['page'] = $page_result['page'];
                    $REQUEST_ATTRIBUTES['view'] = "confirm-deletion.php";
                    $REQUEST_ATTRIBUTES['title'] = "Confirm Page Deletion";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        default :
            header('location: ' . CONTEXT_PATH . '/404');
            exit;
            break;
    }
} else {
    $page_result = PageService::GetPages();
    if ($page_result['status'] == 1) {
        $REQUEST_ATTRIBUTES['pages'] = $page_result['pages'];
        $REQUEST_ATTRIBUTES['pagination'] = $page_result['pagination'];
        $REQUEST_ATTRIBUTES['columns'] = $page_result['columns'];
        $REQUEST_ATTRIBUTES['view'] = "get.php";
        $REQUEST_ATTRIBUTES['title'] = "Pages";
    } else {
        header('location: ' . CONTEXT_PATH . '/user-generated-error');
        exit;
    }
}

require BASE_PATH . '/views/backend/site-management/page.php';
