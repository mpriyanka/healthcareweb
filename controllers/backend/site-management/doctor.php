<?php

use HealthCare\Site\Impl\DoctorService;
use HealthCare\User\Impl\UserService;
use HealthCare\Util\Impl\CountryService;

require 'require-login.php';

global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;

UserService::AssertPermission("Manage Member|View Doctor", null, CONTEXT_PATH . "/access-denied");

if (isset($REQUEST_PATHS[3])) {
    UserService::AssertPermission("Manage Member", null, CONTEXT_PATH . "/access-denied");
    switch ($REQUEST_PATHS[3]) {
        case "create":
            $country_result = CountryService::GetCountryNames();

            if (!$country_result['status']) {
                header('location: ' . CONTEXT_PATH . '/user-generated-error');
                exit;
            }

            $REQUEST_ATTRIBUTES['country-names'] = $country_result['country-names'];

            $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
            $confirm_username = filter_input(INPUT_POST, 'confirm_username', FILTER_SANITIZE_STRING);
            $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);

            $affiliations = filter_input(INPUT_POST, 'affiliations', FILTER_SANITIZE_STRING);
            $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            $firstname = filter_input(INPUT_POST, 'firstname', FILTER_SANITIZE_STRING);
            $middlename = filter_input(INPUT_POST, 'middlename', FILTER_SANITIZE_STRING);
            $lastname = filter_input(INPUT_POST, 'lastname', FILTER_SANITIZE_STRING);
            $phone = filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_STRING);
            $mobile = filter_input(INPUT_POST, 'mobile', FILTER_SANITIZE_STRING);
            $line_1 = filter_input(INPUT_POST, 'line_1', FILTER_SANITIZE_STRING);
            $line_2 = filter_input(INPUT_POST, 'line_2', FILTER_SANITIZE_STRING);
            $city = filter_input(INPUT_POST, 'city', FILTER_SANITIZE_STRING);
            $state = filter_input(INPUT_POST, 'state', FILTER_SANITIZE_STRING);
            $country = filter_input(INPUT_POST, 'country', FILTER_SANITIZE_STRING);

            if (isset($username) && isset($confirm_username) && isset($password) && isset($affiliations) && isset($title) && isset($firstname) && isset($middlename) && isset($lastname) && isset($phone) && isset($mobile) && isset($line_1) && isset($line_2) && isset($city) && isset($state) && isset($country)) {

                $values['username'] = $username;
                $values['confirm_username'] = $confirm_username;
                $values['password'] = $password;

                $values['affiliations'] = $affiliations;
                $values['title'] = $title;
                $values['firstname'] = $firstname;
                $values['middlename'] = $middlename;
                $values['lastname'] = $lastname;
                $values['phone'] = $phone;
                $values['mobile'] = $mobile;
                $values['line_1'] = $line_1;
                $values['line_2'] = $line_2;
                $values['city'] = $city;
                $values['state'] = $state;
                $values['country'] = $country;

                $values = array_map('trim', $values);

                $doctor_result = MemberService::CreateMember($values);
                $REQUEST_ATTRIBUTES['doctor'] = $doctor_result['doctor'];
                $REQUEST_ATTRIBUTES['profile'] = $doctor_result['profile'];
                $REQUEST_ATTRIBUTES['address'] = $doctor_result['address'];
                $REQUEST_ATTRIBUTES['status'] = $doctor_result['status'];
                $REQUEST_ATTRIBUTES['error-msg'] = $doctor_result['error-msg'];
                $REQUEST_ATTRIBUTES['view'] = "create.php";
                $REQUEST_ATTRIBUTES['title'] = "Create Member";
            } else {
                $doctor_result = MemberService::GetCreateMemberTemplate();
                if ($doctor_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['author'] = $doctor_result['author'];
                    $REQUEST_ATTRIBUTES['doctor'] = $doctor_result['doctor'];
                    $REQUEST_ATTRIBUTES['profile'] = $doctor_result['profile'];
                    $REQUEST_ATTRIBUTES['address'] = $doctor_result['address'];
                    $REQUEST_ATTRIBUTES['view'] = "create.php";
                    $REQUEST_ATTRIBUTES['title'] = "Create Member";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        case "edit":
            $country_result = CountryService::GetCountryNames();

            if (!$country_result['status'] || !isset($REQUEST_PATHS[4])) {
                header('location: ' . CONTEXT_PATH . '/user-generated-error');
                exit;
            }

            $values['id'] = $REQUEST_PATHS[4];

            $REQUEST_ATTRIBUTES['country-names'] = $country_result['country-names'];

            $firstname = filter_input(INPUT_POST, 'firstname', FILTER_SANITIZE_STRING);
            $middlename = filter_input(INPUT_POST, 'middlename', FILTER_SANITIZE_STRING);
            $lastname = filter_input(INPUT_POST, 'lastname', FILTER_SANITIZE_STRING);
            $phone = filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_STRING);
            $mobile = filter_input(INPUT_POST, 'mobile', FILTER_SANITIZE_STRING);
            $line_1 = filter_input(INPUT_POST, 'line_1', FILTER_SANITIZE_STRING);
            $line_2 = filter_input(INPUT_POST, 'line_2', FILTER_SANITIZE_STRING);
            $city = filter_input(INPUT_POST, 'city', FILTER_SANITIZE_STRING);
            $state = filter_input(INPUT_POST, 'state', FILTER_SANITIZE_STRING);
            $country = filter_input(INPUT_POST, 'country', FILTER_SANITIZE_STRING);
            $bio = htmlspecialchars(filter_input(INPUT_POST, 'bio', FILTER_SANITIZE_MAGIC_QUOTES));

            if (isset($firstname) && isset($middlename) && isset($lastname) && isset($phone) && isset($mobile) && isset($line_1) && isset($line_2) && isset($city) && isset($state) && isset($country)) {

                $values['firstname'] = $firstname;
                $values['middlename'] = $middlename;
                $values['lastname'] = $lastname;
                $values['phone'] = $phone;
                $values['mobile'] = $mobile;
                $values['line_1'] = $line_1;
                $values['line_2'] = $line_2;
                $values['city'] = $city;
                $values['state'] = $state;
                $values['country'] = $country;
                $values['bio'] = $bio;

                $values = array_map('trim', $values);

                $doctor_result = DoctorService::UpdateDoctor($values);

                $REQUEST_ATTRIBUTES['profile'] = $doctor_result['profile'];
                $REQUEST_ATTRIBUTES['address'] = $doctor_result['address'];
                $REQUEST_ATTRIBUTES['status'] = $doctor_result['status'];
                $REQUEST_ATTRIBUTES['error-msg'] = $doctor_result['error-msg'];
                $REQUEST_ATTRIBUTES['view'] = "edit.php";
                $REQUEST_ATTRIBUTES['title'] = "Edit Doctor";
            } else {

                $doctor_result = DoctorService::GetDoctor($values);

                if ($doctor_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['profile'] = $doctor_result['profile'];
                    $REQUEST_ATTRIBUTES['address'] = $doctor_result['address'];
                    $REQUEST_ATTRIBUTES['view'] = "edit.php";
                    $REQUEST_ATTRIBUTES['title'] = "Edit Doctor";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        case "resend-verification":
            if (!isset($REQUEST_PATHS[4])) {
                header('location: ' . CONTEXT_PATH . '/user-generated-error');
                exit;
            }

            $values['id'] = $REQUEST_PATHS[4];

            $confirmed = filter_input(INPUT_POST, 'confirmed', FILTER_SANITIZE_STRING);
            if (isset($confirmed)) {

                $doctor_result = DoctorService::ResendVerifcation($values);

                $REQUEST_ATTRIBUTES['status'] = $doctor_result['status'];
                $REQUEST_ATTRIBUTES['error-msg'] = $doctor_result['error-msg'];
                $REQUEST_ATTRIBUTES['view'] = "resend-verification.php";
                $REQUEST_ATTRIBUTES['title'] = "Resend Verification";
            } else {

                $doctor_result = DoctorService::GetDoctor($values);
                if ($doctor_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['doctor'] = $doctor_result['doctor'];
                    $REQUEST_ATTRIBUTES['view'] = "confirm-resend-verification.php";
                    $REQUEST_ATTRIBUTES['title'] = "Confirm Resending of Verification";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        case "accept":
            if (!isset($REQUEST_PATHS[4])) {
                header('location: ' . CONTEXT_PATH . '/user-generated-error');
                exit;
            }

            $values['id'] = $REQUEST_PATHS[4];

            $confirmed = filter_input(INPUT_POST, 'confirmed', FILTER_SANITIZE_STRING);

            if (isset($confirmed)) {

                $doctor_result = DoctorService::AcceptDoctor($values);

                $REQUEST_ATTRIBUTES['doctor'] = $doctor_result['doctor'];
                $REQUEST_ATTRIBUTES['status'] = $doctor_result['status'];
                $REQUEST_ATTRIBUTES['error-msg'] = $doctor_result['error-msg'];
                $REQUEST_ATTRIBUTES['view'] = "accepted.php";
                $REQUEST_ATTRIBUTES['title'] = "Doctor Acceptance";
            } else {
                $doctor_result = DoctorService::GetDoctor($values);

                if ($doctor_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['doctor'] = $doctor_result['doctor'];
                    $REQUEST_ATTRIBUTES['view'] = "confirm-acceptance.php";
                    $REQUEST_ATTRIBUTES['title'] = "Confirm Doctor Acceptance ";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        case "delete":
            if (!isset($REQUEST_PATHS[4])) {
                header('location: ' . CONTEXT_PATH . '/user-generated-error');
                exit;
            }

            $values['id'] = $REQUEST_PATHS[4];

            $confirmed = filter_input(INPUT_POST, 'confirmed', FILTER_SANITIZE_STRING);
            if (isset($confirmed)) {

                $doctor_result = MemberService::DeleteMember($values);

                $REQUEST_ATTRIBUTES['status'] = $doctor_result['status'];
                $REQUEST_ATTRIBUTES['error-msg'] = $doctor_result['error-msg'];
                $REQUEST_ATTRIBUTES['view'] = "delete.php";
                $REQUEST_ATTRIBUTES['title'] = "Delete Member";
            } else {

                $doctor_result = MemberService::GetMember($values);
                if ($doctor_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['doctor'] = $doctor_result['doctor'];
                    $REQUEST_ATTRIBUTES['profile'] = $doctor_result['profile'];
                    $REQUEST_ATTRIBUTES['view'] = "confirm-deletion.php";
                    $REQUEST_ATTRIBUTES['title'] = "Confirm Member Deletion";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        default :
            header('location: ' . CONTEXT_PATH . '/404');
            exit;
            break;
    }
} else {
    $doctor_result = DoctorService::GetDoctors();
    if ($doctor_result['status'] == 1) {
        $REQUEST_ATTRIBUTES['doctors'] = $doctor_result['doctors'];
        $REQUEST_ATTRIBUTES['pagination'] = $doctor_result['pagination'];
        $REQUEST_ATTRIBUTES['columns'] = $doctor_result['columns'];
        $REQUEST_ATTRIBUTES['view'] = "get.php";
        $REQUEST_ATTRIBUTES['title'] = "Doctors";
    } else {
        header('location: ' . CONTEXT_PATH . '/user-generated-error');
        exit;
    }
}

require BASE_PATH . '/views/backend/site-management/doctor.php';
