<?php

use HealthCare\Site\Impl\ImageService;
use HealthCare\User\Impl\UserService;
use HealthCare\Widgets\Services\GalleryWidgetTemplateService;

require 'require-login.php';

global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;

UserService::AssertPermission("Manage Image|View Image", null, CONTEXT_PATH . "/access-denied");

if (isset($REQUEST_PATHS[3])) {
    UserService::AssertPermission("Manage Image", null, CONTEXT_PATH . "/access-denied");
    switch ($REQUEST_PATHS[3]) {
        case "create-gallery-widget":
            $image_result = ImageService::GetFolderNames();

            if (!$image_result['status']) {
                header('location: ' . CONTEXT_PATH . '/user-generated-error');
                exit;
            }

            $REQUEST_ATTRIBUTES['folder-names'] = $image_result['folder-names'];

            $folders = filter_input(INPUT_POST, 'folders', FILTER_SANITIZE_STRING);
            $height = filter_input(INPUT_POST, 'height', FILTER_SANITIZE_STRING);
            $width = filter_input(INPUT_POST, 'width', FILTER_SANITIZE_STRING);
            $index = filter_input(INPUT_POST, 'index', FILTER_SANITIZE_STRING);
            $size = filter_input(INPUT_POST, 'size', FILTER_SANITIZE_STRING);

            if (isset($folders) && isset($height) && isset($width) && isset($index) && isset($size)) {

                $values['folders'] = $folders;
                $values['height'] = $height;
                $values['width'] = $width;
                $values['index'] = $index;
                $values['size'] = $size;

                $values = array_map('trim', $values);

                $gallery_widget_result = GalleryWidgetTemplateService::GetSnippet($values);

                $REQUEST_ATTRIBUTES['gallery-widget'] = $gallery_widget_result['gallery-widget'];
                $REQUEST_ATTRIBUTES['status'] = $gallery_widget_result['status'];
                $REQUEST_ATTRIBUTES['error-msg'] = $gallery_widget_result['error-msg'];
                $REQUEST_ATTRIBUTES['view'] = "create-gallery-widget.php";
                $REQUEST_ATTRIBUTES['title'] = "Create Gallery Widget";
            } else {
                $gallery_widget_result = GalleryWidgetTemplateService::GetNewReservationTemplate();
                $REQUEST_ATTRIBUTES['gallery-widget'] = $gallery_widget_result['gallery-widget'];
                $REQUEST_ATTRIBUTES['view'] = "create-gallery-widget.php";
                $REQUEST_ATTRIBUTES['title'] = "Create Gallery Widget";
            }
            break;
        case "create":
            $image_result = ImageService::GetFolderNames();

            if (!$image_result['status']) {
                header('location: ' . CONTEXT_PATH . '/user-generated-error');
                exit;
            }

            $REQUEST_ATTRIBUTES['folder-names'] = $image_result['folder-names'];

            $folder = filter_input(INPUT_POST, 'folder', FILTER_SANITIZE_STRING);
            $folders = filter_input(INPUT_POST, 'folders', FILTER_SANITIZE_STRING);
            $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);

            if (isset($folder) && isset($folders) && isset($title)) {

                $values['folder'] = $folder;
                $values['folders'] = $folders;
                $values['title'] = $title;

                $values = array_map('trim', $values);

                $image_result = ImageService::CreateImage($values);

                $REQUEST_ATTRIBUTES['image'] = $image_result['image'];
                $REQUEST_ATTRIBUTES['status'] = $image_result['status'];
                $REQUEST_ATTRIBUTES['error-msg'] = $image_result['error-msg'];
                $REQUEST_ATTRIBUTES['view'] = "create.php";
                $REQUEST_ATTRIBUTES['title'] = "Create Image";
            } else {
                $image_result = ImageService::GetCreateImageTemplate();
                if ($image_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['image'] = $image_result['image'];
                    $REQUEST_ATTRIBUTES['view'] = "create.php";
                    $REQUEST_ATTRIBUTES['title'] = "Create Image";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        case "edit":
            if (!isset($REQUEST_PATHS[4])) {
                header('location: ' . CONTEXT_PATH . '/user-generated-error');
                exit;
            }

            $image_result = ImageService::GetFolderNames();

            if (!$image_result['status']) {
                header('location: ' . CONTEXT_PATH . '/user-generated-error');
                exit;
            }

            $values['id'] = $REQUEST_PATHS[4];

            $REQUEST_ATTRIBUTES['folder-names'] = $image_result['folder-names'];

            $folder = filter_input(INPUT_POST, 'folder', FILTER_SANITIZE_STRING);
            $folders = filter_input(INPUT_POST, 'folders', FILTER_SANITIZE_STRING);
            $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);

            if (isset($folder) && isset($folders) && isset($title)) {

                $values['folder'] = $folder;
                $values['folders'] = $folders;
                $values['title'] = $title;

                $values = array_map('trim', $values);

                $image_result = ImageService::UpdateImage($values);

                $REQUEST_ATTRIBUTES['image'] = $image_result['image'];
                $REQUEST_ATTRIBUTES['status'] = $image_result['status'];
                $REQUEST_ATTRIBUTES['error-msg'] = $image_result['error-msg'];
                $REQUEST_ATTRIBUTES['view'] = "edit.php";
                $REQUEST_ATTRIBUTES['title'] = "Edit Image";
            } else {

                $image_result = ImageService::GetImage($values);
                if ($image_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['image'] = $image_result['image'];
                    $REQUEST_ATTRIBUTES['view'] = "edit.php";
                    $REQUEST_ATTRIBUTES['title'] = "Edit Image";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        case "delete":
            if (!isset($REQUEST_PATHS[4])) {
                header('location: ' . CONTEXT_PATH . '/user-generated-error');
                exit;
            }

            $values['id'] = $REQUEST_PATHS[4];

            $confirmed = filter_input(INPUT_POST, 'confirmed', FILTER_SANITIZE_STRING);
            if (isset($confirmed)) {
                $image_result = ImageService::DeleteImage($values);

                $REQUEST_ATTRIBUTES['status'] = $image_result['status'];
                $REQUEST_ATTRIBUTES['error-msg'] = $image_result['error-msg'];
                $REQUEST_ATTRIBUTES['view'] = "delete.php";
                $REQUEST_ATTRIBUTES['title'] = "Delete Image";
            } else {

                $image_result = ImageService::GetImage($values);
                if ($image_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['image'] = $image_result['image'];
                    $REQUEST_ATTRIBUTES['view'] = "confirm-deletion.php";
                    $REQUEST_ATTRIBUTES['title'] = "Confirm Image Deletion";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        default :
            header('location: ' . CONTEXT_PATH . '/404');
            exit;
            break;
    }
} else {
    $image_result = ImageService::GetImages();
    if ($image_result['status'] == 1) {
        $REQUEST_ATTRIBUTES['images'] = $image_result['images'];
        $REQUEST_ATTRIBUTES['pagination'] = $image_result['pagination'];
        $REQUEST_ATTRIBUTES['columns'] = $image_result['columns'];
        $REQUEST_ATTRIBUTES['view'] = "get.php";
        $REQUEST_ATTRIBUTES['title'] = "Images";
    } else {
        header('location: ' . CONTEXT_PATH . '/user-generated-error');
        exit;
    }
}

require BASE_PATH . '/views/backend/site-management/image.php';
