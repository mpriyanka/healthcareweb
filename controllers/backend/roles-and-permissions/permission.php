<?php

use HealthCare\User\Impl\PermissionService;
use HealthCare\User\Impl\UserService;

require 'require-login.php';

global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;

UserService::AssertPermission("Manage Permission", null, CONTEXT_PATH."/access-denied");

if (isset($REQUEST_PATHS[3])) {
    switch ($REQUEST_PATHS[3]) {
        case "create":
            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
            if (isset($name)) {

                $values['name'] = $name;

                $values = array_map('trim', $values);

                $permission_result = PermissionService::CreatePermission($values);
                if ($permission_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['permission'] = $permission_result['permission'];
                    $REQUEST_ATTRIBUTES['status'] = $permission_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $permission_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "create.php";
                    $REQUEST_ATTRIBUTES['title'] = "Create Permission";
                } else {
                    $REQUEST_ATTRIBUTES['permission'] = $permission_result['permission'];
                    $REQUEST_ATTRIBUTES['status'] = $permission_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $permission_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "create.php";
                    $REQUEST_ATTRIBUTES['title'] = "Create Permission";
                }
            } else {
                $permission_result = PermissionService::GetCreatePermissionTemplate();
                if ($permission_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['permission'] = $permission_result['permission'];
                    $REQUEST_ATTRIBUTES['view'] = "create.php";
                    $REQUEST_ATTRIBUTES['title'] = "Create Permission";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        case "edit":
            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
            if (isset($name)) {

                $values['id'] = $REQUEST_PATHS[4];
                $values['name'] = $name;

                $values = array_map('trim', $values);

                $permission_result = PermissionService::UpdatePermission($values);

                if ($permission_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['permission'] = $permission_result['permission'];
                    $REQUEST_ATTRIBUTES['status'] = $permission_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $permission_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "edit.php";
                    $REQUEST_ATTRIBUTES['title'] = "Edit Permission";
                } else {
                    $REQUEST_ATTRIBUTES['permission'] = $permission_result['permission'];
                    $REQUEST_ATTRIBUTES['status'] = $permission_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $permission_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "edit.php";
                    $REQUEST_ATTRIBUTES['title'] = "Edit Permission";
                }
            } else {
                $values['id'] = $REQUEST_PATHS[4];

                $permission_result = PermissionService::GetPermission($values);
                if ($permission_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['permission'] = $permission_result['permission'];
                    $REQUEST_ATTRIBUTES['view'] = "edit.php";
                    $REQUEST_ATTRIBUTES['title'] = "Edit Permission";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        case "delete":
            $confirmed = filter_input(INPUT_POST, 'confirmed', FILTER_SANITIZE_STRING);
            if (isset($confirmed)) {

                $values['id'] = $REQUEST_PATHS[4];

                $permission_result = PermissionService::DeletePermission($values);

                if ($permission_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['status'] = $permission_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $permission_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "delete.php";
                    $REQUEST_ATTRIBUTES['title'] = "Delete Permission";
                } else {
                    $REQUEST_ATTRIBUTES['status'] = $permission_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $permission_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "delete.php";
                    $REQUEST_ATTRIBUTES['title'] = "Delete Permission";
                }
            } else {
                $values['id'] = $REQUEST_PATHS[4];

                $permission_result = PermissionService::GetPermission($values);
                if ($permission_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['permission'] = $permission_result['permission'];
                    $REQUEST_ATTRIBUTES['view'] = "confirm-deletion.php";
                    $REQUEST_ATTRIBUTES['title'] = "Confirm Permission Deletion";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        default :
            header('location: ' . CONTEXT_PATH . '/404');
            exit;
            break;
    }
} else {
    $permission_result = PermissionService::GetPermissions();
    if ($permission_result['status'] == 1) {
        $REQUEST_ATTRIBUTES['permissions'] = $permission_result['permissions'];
        $REQUEST_ATTRIBUTES['pagination'] = $permission_result['pagination'];
        $REQUEST_ATTRIBUTES['columns'] = $permission_result['columns'];
        $REQUEST_ATTRIBUTES['view'] = "get.php";
        $REQUEST_ATTRIBUTES['title'] = "Permissions";
    } else {
        header('location: ' . CONTEXT_PATH . '/user-generated-error');
        exit;
    }
}

require BASE_PATH . '/views/backend/roles-and-permissions/permission.php';
