<?php

use HealthCare\User\Impl\PermissionService;
use HealthCare\User\Impl\RolePermissionService;
use HealthCare\User\Impl\RoleService;
use HealthCare\User\Impl\UserService;

require 'require-login.php';

global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;

UserService::AssertPermission("Manage Role Permission", null, CONTEXT_PATH . "/access-denied");

if (isset($REQUEST_PATHS[3])) {
    switch ($REQUEST_PATHS[3]) {
        case "create":
            $role_result = RoleService::GetRoleNames();
            $permission_result = PermissionService::GetPermissionNames();

            if (!$role_result['status'] || !$permission_result['status']) {
                header('location: ' . CONTEXT_PATH . '/user-generated-error');
                exit;
            }

            $REQUEST_ATTRIBUTES['role-names'] = $role_result['role-names'];
            $REQUEST_ATTRIBUTES['permission-names'] = $permission_result['permission-names'];

            $role_id = filter_input(INPUT_POST, 'role_id', FILTER_SANITIZE_STRING);
            $permission_id = filter_input(INPUT_POST, 'permission_id', FILTER_SANITIZE_STRING);
            if (isset($role_id) && isset($permission_id)) {

                $values['role_id'] = $role_id;
                $values['permission_id'] = $permission_id;

                $values = array_map('trim', $values);

                $role_permission_result = RolePermissionService::CreateRolePermission($values);
                $REQUEST_ATTRIBUTES['role-permission'] = $role_permission_result['role-permission'];
                $REQUEST_ATTRIBUTES['status'] = $role_permission_result['status'];
                $REQUEST_ATTRIBUTES['error-msg'] = $role_permission_result['error-msg'];
                $REQUEST_ATTRIBUTES['view'] = "create.php";
                $REQUEST_ATTRIBUTES['title'] = "Create Role Permission";
            } else {
                $role_permission_result = RolePermissionService::GetCreateRolePermissionTemplate();
                if ($role_permission_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['role-permission'] = $role_permission_result['role-permission'];
                    $REQUEST_ATTRIBUTES['view'] = "create.php";
                    $REQUEST_ATTRIBUTES['title'] = "Create Role Permission";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        case "edit":
            $role_result = RoleService::GetRoleNames();
            $permission_result = PermissionService::GetPermissionNames();

            if (!$role_result['status'] || !$permission_result['status'] || !$REQUEST_PATHS[4]) {
                header('location: ' . CONTEXT_PATH . '/user-generated-error');
                exit;
            }
            
            $values['id'] = $REQUEST_PATHS[4];

            $REQUEST_ATTRIBUTES['role-names'] = $role_result['role-names'];
            $REQUEST_ATTRIBUTES['permission-names'] = $permission_result['permission-names'];

            $role_id = filter_input(INPUT_POST, 'role_id', FILTER_SANITIZE_STRING);
            $permission_id = filter_input(INPUT_POST, 'permission_id', FILTER_SANITIZE_STRING);

            if (isset($role_id) && isset($permission_id)) {

                $values['role_id'] = $role_id;
                $values['permission_id'] = $permission_id;

                $values = array_map('trim', $values);

                $role_permission_result = RolePermissionService::UpdateRolePermission($values);

                $REQUEST_ATTRIBUTES['role-permission'] = $role_permission_result['role-permission'];
                $REQUEST_ATTRIBUTES['status'] = $role_permission_result['status'];
                $REQUEST_ATTRIBUTES['error-msg'] = $role_permission_result['error-msg'];
                $REQUEST_ATTRIBUTES['view'] = "edit.php";
                $REQUEST_ATTRIBUTES['title'] = "Edit RolePermission";
            } else {
                $values['id'] = $REQUEST_PATHS[4];

                $role_permission_result = RolePermissionService::GetRolePermission($values);
                if ($role_permission_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['role-permission'] = $role_permission_result['role-permission'];
                    $REQUEST_ATTRIBUTES['view'] = "edit.php";
                    $REQUEST_ATTRIBUTES['title'] = "Edit Role Permission";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        case "delete":
            $confirmed = filter_input(INPUT_POST, 'confirmed', FILTER_SANITIZE_STRING);
            if (isset($confirmed)) {

                $values['id'] = $REQUEST_PATHS[4];

                $role_permission_result = RolePermissionService::DeleteRolePermission($values);

                if ($role_permission_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['status'] = $role_permission_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $role_permission_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "delete.php";
                    $REQUEST_ATTRIBUTES['title'] = "Delete Role Permission";
                } else {
                    $REQUEST_ATTRIBUTES['status'] = $role_permission_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $role_permission_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "delete.php";
                    $REQUEST_ATTRIBUTES['title'] = "Delete Role Permission";
                }
            } else {
                $values['id'] = $REQUEST_PATHS[4];

                $role_permission_result = RolePermissionService::GetRolePermission($values);
                if ($role_permission_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['role-permission'] = $role_permission_result['role-permission'];
                    $REQUEST_ATTRIBUTES['view'] = "confirm-deletion.php";
                    $REQUEST_ATTRIBUTES['title'] = "Confirm Role Permission Deletion";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        default :
            header('location: ' . CONTEXT_PATH . '/404');
            exit;
            break;
    }
} else {
    $role_permission_result = RolePermissionService::GetRolePermissions();
    if ($role_permission_result['status'] == 1) {
        $REQUEST_ATTRIBUTES['role-permissions'] = $role_permission_result['role-permissions'];
        $REQUEST_ATTRIBUTES['pagination'] = $role_permission_result['pagination'];
        $REQUEST_ATTRIBUTES['columns'] = $role_permission_result['columns'];
        $REQUEST_ATTRIBUTES['view'] = "get.php";
        $REQUEST_ATTRIBUTES['title'] = "Role Permissions";
    } else {
        header('location: ' . CONTEXT_PATH . '/user-generated-error');
        exit;
    }
}

require BASE_PATH . '/views/backend/roles-and-permissions/role-permission.php';
