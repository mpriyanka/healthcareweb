<?php

use HealthCare\User\Impl\RoleService;
use HealthCare\User\Impl\UserService;

require 'require-login.php';

global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;

UserService::AssertPermission("Manage Role", null, CONTEXT_PATH."/access-denied");

if (isset($REQUEST_PATHS[3])) {
    switch ($REQUEST_PATHS[3]) {
        case "create":
            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
            if (isset($name)) {

                $values['name'] = $name;

                $values = array_map('trim', $values);

                $role_result = RoleService::CreateRole($values);
                if ($role_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['role'] = $role_result['role'];
                    $REQUEST_ATTRIBUTES['status'] = $role_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $role_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "create.php";
                    $REQUEST_ATTRIBUTES['title'] = "Create Role";
                } else {
                    $REQUEST_ATTRIBUTES['role'] = $role_result['role'];
                    $REQUEST_ATTRIBUTES['status'] = $role_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $role_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "create.php";
                    $REQUEST_ATTRIBUTES['title'] = "Create Role";
                }
            } else {
                $role_result = RoleService::GetCreateRoleTemplate();
                if ($role_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['role'] = $role_result['role'];
                    $REQUEST_ATTRIBUTES['view'] = "create.php";
                    $REQUEST_ATTRIBUTES['title'] = "Create Role";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        case "edit":
            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
            if (isset($name)) {

                $values['id'] = $REQUEST_PATHS[4];
                $values['name'] = $name;

                $values = array_map('trim', $values);

                $role_result = RoleService::UpdateRole($values);

                if ($role_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['role'] = $role_result['role'];
                    $REQUEST_ATTRIBUTES['status'] = $role_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $role_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "edit.php";
                    $REQUEST_ATTRIBUTES['title'] = "Edit Role";
                } else {
                    $REQUEST_ATTRIBUTES['role'] = $role_result['role'];
                    $REQUEST_ATTRIBUTES['status'] = $role_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $role_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "edit.php";
                    $REQUEST_ATTRIBUTES['title'] = "Edit Role";
                }
            } else {
                $values['id'] = $REQUEST_PATHS[4];

                $role_result = RoleService::GetRole($values);
                if ($role_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['role'] = $role_result['role'];
                    $REQUEST_ATTRIBUTES['view'] = "edit.php";
                    $REQUEST_ATTRIBUTES['title'] = "Edit Role";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        case "delete":
            $confirmed = filter_input(INPUT_POST, 'confirmed', FILTER_SANITIZE_STRING);
            if (isset($confirmed)) {

                $values['id'] = $REQUEST_PATHS[4];

                $role_result = RoleService::DeleteRole($values);

                if ($role_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['status'] = $role_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $role_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "delete.php";
                    $REQUEST_ATTRIBUTES['title'] = "Delete Role";
                } else {
                    $REQUEST_ATTRIBUTES['status'] = $role_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $role_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "delete.php";
                    $REQUEST_ATTRIBUTES['title'] = "Delete Role";
                }
            } else {
                $values['id'] = $REQUEST_PATHS[4];

                $role_result = RoleService::GetRole($values);
                if ($role_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['role'] = $role_result['role'];
                    $REQUEST_ATTRIBUTES['view'] = "confirm-deletion.php";
                    $REQUEST_ATTRIBUTES['title'] = "Confirm Role Deletion";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        default :
            header('location: ' . CONTEXT_PATH . '/404');
            exit;
            break;
    }
} else {
    $role_result = RoleService::GetRoles();
    if ($role_result['status'] == 1) {
        $REQUEST_ATTRIBUTES['roles'] = $role_result['roles'];
        $REQUEST_ATTRIBUTES['pagination'] = $role_result['pagination'];
        $REQUEST_ATTRIBUTES['columns'] = $role_result['columns'];
        $REQUEST_ATTRIBUTES['view'] = "get.php";
        $REQUEST_ATTRIBUTES['title'] = "Roles";
    } else {
        header('location: ' . CONTEXT_PATH . '/user-generated-error');
        exit;
    }
}

require BASE_PATH . '/views/backend/roles-and-permissions/role.php';
