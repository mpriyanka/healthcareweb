<?php

use Anytimestream\User\Impl\UserService;

require 'require-login.php';


global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;

UserService::RequireLogin(CONTEXT_PATH . '/login');

$password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
$password2 = filter_input(INPUT_POST, 'password2', FILTER_SANITIZE_STRING);

if (isset($password) && isset($password)) {
    $values['password'] = $password;
    $values['password2'] = $password2;
    $values = array_map('trim', $values);

    $user_result = UserService::ChangePassword($values);
    $REQUEST_ATTRIBUTES['status'] = $user_result['status'];
    if(isset($user_result['user'])){
        $REQUEST_ATTRIBUTES['user'] = $user_result['user'];
    }
    $REQUEST_ATTRIBUTES['error-msg'] = $user_result['error-msg'];
}

require BASE_PATH . '/views/backend/change-password.php';
