<?php

use HealthCare\User\Impl\UserService;

require 'require-login.php';

global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;

UserService::AssertPermission("Manage User", null, CONTEXT_PATH."/access-denied");

if (isset($REQUEST_PATHS[3])) {
    switch ($REQUEST_PATHS[3]) {
        case "create":
            $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
            $confirm_username = filter_input(INPUT_POST, 'confirm_username', FILTER_SANITIZE_STRING);
            $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
            if(isset($username) && isset($confirm_username) && isset($password)) {

                $values['username'] = $username;
                $values['confirm_username'] = $confirm_username;
                $values['password'] = $password;

                $values = array_map('trim', $values);

                $user_result = UserService::CreateUser($values);
                if ($user_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['user'] = $user_result['user'];
                    $REQUEST_ATTRIBUTES['status'] = $user_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $user_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "create.php";
                    $REQUEST_ATTRIBUTES['title'] = "Create User";
                } else {
                    $REQUEST_ATTRIBUTES['user'] = $user_result['user'];
                    $REQUEST_ATTRIBUTES['status'] = $user_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $user_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "create.php";
                    $REQUEST_ATTRIBUTES['title'] = "Create User";
                }
            } else {
                $user_result = UserService::GetCreateUserTemplate();
                if ($user_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['user'] = $user_result['user'];
                    $REQUEST_ATTRIBUTES['view'] = "create.php";
                    $REQUEST_ATTRIBUTES['title'] = "Create User";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        case "change-password":
            $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
            if (isset($password)) {

                $values['id'] = $REQUEST_PATHS[4];
                $values['password'] = $password;

                $values = array_map('trim', $values);

                $user_result = UserService::UpdatePassword($values);

                if ($user_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['user'] = $user_result['user'];
                    $REQUEST_ATTRIBUTES['status'] = $user_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $user_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "change-password.php";
                    $REQUEST_ATTRIBUTES['title'] = "Change Password";
                } else {
                    $REQUEST_ATTRIBUTES['user'] = $user_result['user'];
                    $REQUEST_ATTRIBUTES['status'] = $user_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $user_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "change-password.php";
                    $REQUEST_ATTRIBUTES['title'] = "Change Password";
                }
            } else {
                $values['id'] = $REQUEST_PATHS[4];

                $user_result = UserService::GetUser($values);
                if ($user_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['user'] = $user_result['user'];
                    $REQUEST_ATTRIBUTES['view'] = "change-password.php";
                    $REQUEST_ATTRIBUTES['title'] = "Change Password";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        case "disable":
            $confirmed = filter_input(INPUT_POST, 'confirmed', FILTER_SANITIZE_STRING);
            if (isset($confirmed)) {

                $values['id'] = $REQUEST_PATHS[4];
                $values['enabled'] = 0;

                $values = array_map('trim', $values);

                $user_result = UserService::UpdateEnabled($values);

                if ($user_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['user'] = $user_result['user'];
                    $REQUEST_ATTRIBUTES['status'] = $user_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $user_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "disable.php";
                    $REQUEST_ATTRIBUTES['title'] = "Disable User";
                } else {
                    $REQUEST_ATTRIBUTES['user'] = $user_result['user'];
                    $REQUEST_ATTRIBUTES['status'] = $user_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $user_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "disable.php";
                    $REQUEST_ATTRIBUTES['title'] = "Disable User";
                }
            } else {
                $values['id'] = $REQUEST_PATHS[4];

                $user_result = UserService::GetUser($values);
                if ($user_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['user'] = $user_result['user'];
                    $REQUEST_ATTRIBUTES['view'] = "confirm-disable.php";
                    $REQUEST_ATTRIBUTES['title'] = "Disable User";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        case "enable":
            $confirmed = filter_input(INPUT_POST, 'confirmed', FILTER_SANITIZE_STRING);
            if (isset($confirmed)) {

                $values['id'] = $REQUEST_PATHS[4];
                $values['enabled'] = 1;

                $values = array_map('trim', $values);

                $user_result = UserService::UpdateEnabled($values);

                if ($user_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['user'] = $user_result['user'];
                    $REQUEST_ATTRIBUTES['status'] = $user_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $user_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "enable.php";
                    $REQUEST_ATTRIBUTES['title'] = "Enable User";
                } else {
                    $REQUEST_ATTRIBUTES['user'] = $user_result['user'];
                    $REQUEST_ATTRIBUTES['status'] = $user_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $user_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "enable.php";
                    $REQUEST_ATTRIBUTES['title'] = "Enable User";
                }
            } else {
                $values['id'] = $REQUEST_PATHS[4];

                $user_result = UserService::GetUser($values);
                if ($user_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['user'] = $user_result['user'];
                    $REQUEST_ATTRIBUTES['view'] = "confirm-enable.php";
                    $REQUEST_ATTRIBUTES['title'] = "Enable User";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        case "delete":
            $confirmed = filter_input(INPUT_POST, 'confirmed', FILTER_SANITIZE_STRING);
            if (isset($confirmed)) {

                $values['id'] = $REQUEST_PATHS[4];

                $user_result = UserService::DeleteUser($values);

                if ($user_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['status'] = $user_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $user_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "delete.php";
                    $REQUEST_ATTRIBUTES['title'] = "Delete User";
                } else {
                    $REQUEST_ATTRIBUTES['status'] = $user_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $user_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "delete.php";
                    $REQUEST_ATTRIBUTES['title'] = "Delete User";
                }
            } else {
                $values['id'] = $REQUEST_PATHS[4];

                $user_result = UserService::GetUser($values);
                if ($user_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['user'] = $user_result['user'];
                    $REQUEST_ATTRIBUTES['view'] = "confirm-deletion.php";
                    $REQUEST_ATTRIBUTES['title'] = "Confirm User Deletion";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        default :
            header('location: ' . CONTEXT_PATH . '/404');
            exit;
            break;
    }
} else {
    $user_result = UserService::GetUsers();
    if ($user_result['status'] == 1) {
        $REQUEST_ATTRIBUTES['users'] = $user_result['users'];
        $REQUEST_ATTRIBUTES['pagination'] = $user_result['pagination'];
        $REQUEST_ATTRIBUTES['columns'] = $user_result['columns'];
        $REQUEST_ATTRIBUTES['view'] = "get.php";
        $REQUEST_ATTRIBUTES['title'] = "Users";
    } else {
        header('location: ' . CONTEXT_PATH . '/user-generated-error');
        exit;
    }
}

require BASE_PATH . '/views/backend/user-management/user.php';
