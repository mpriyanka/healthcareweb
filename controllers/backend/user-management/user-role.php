<?php

use HealthCare\User\Impl\RoleService;
use HealthCare\User\Impl\UserRoleService;
use HealthCare\User\Impl\UserService;

require 'require-login.php';

global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;

UserService::AssertPermission("Manage User Role", null, CONTEXT_PATH."/access-denied");

if (isset($REQUEST_PATHS[3])) {
    switch ($REQUEST_PATHS[3]) {
        case "create":
            $role_result = RoleService::GetRoleNames();
            $user_result = UserService::GetUserNames();

            if (!$role_result['status'] || !$user_result['status']) {
                header('location: ' . CONTEXT_PATH . '/user-generated-error');
                exit;
            }

            $REQUEST_ATTRIBUTES['role-names'] = $role_result['role-names'];
            $REQUEST_ATTRIBUTES['user-names'] = $user_result['user-names'];

            $role_id = filter_input(INPUT_POST, 'role_id', FILTER_SANITIZE_STRING);
            $user_id = filter_input(INPUT_POST, 'user_id', FILTER_SANITIZE_STRING);
            
            if (isset($role_id) && isset($user_id)) {

                $values['role_id'] = $role_id;
                $values['user_id'] = $user_id;

                $values = array_map('trim', $values);

                $user_role_result = UserRoleService::CreateUserRole($values);
                if ($user_role_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['user-role'] = $user_role_result['user-role'];
                    $REQUEST_ATTRIBUTES['status'] = $user_role_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $user_role_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "create.php";
                    $REQUEST_ATTRIBUTES['title'] = "Create User Role";
                } else {
                    $REQUEST_ATTRIBUTES['user-role'] = $user_role_result['user-role'];
                    $REQUEST_ATTRIBUTES['status'] = $user_role_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $user_role_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "create.php";
                    $REQUEST_ATTRIBUTES['title'] = "Create User Role";
                }
            } else {
                $user_role_result = UserRoleService::GetCreateUserRoleTemplate();
                if ($user_role_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['user-role'] = $user_role_result['user-role'];
                    $REQUEST_ATTRIBUTES['view'] = "create.php";
                    $REQUEST_ATTRIBUTES['title'] = "Create User Role";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        case "edit":
            $role_result = RoleService::GetRoleNames();
            $user_result = UserService::GetUserNames();

            if (!$role_result['status'] || !$user_result['status']) {
                header('location: ' . CONTEXT_PATH . '/user-generated-error');
                exit;
            }

            $REQUEST_ATTRIBUTES['role-names'] = $role_result['role-names'];
            $REQUEST_ATTRIBUTES['user-names'] = $user_result['user-names'];

            $role_id = filter_input(INPUT_POST, 'role_id', FILTER_SANITIZE_STRING);
            $user_id = filter_input(INPUT_POST, 'user_id', FILTER_SANITIZE_STRING);

            if (isset($role_id) && isset($user_id)) {

                $values['id'] = $REQUEST_PATHS[4];
                $values['role_id'] = $role_id;
                $values['user_id'] = $user_id;

                $values = array_map('trim', $values);

                $user_role_result = UserRoleService::UpdateUserRole($values);

                if ($user_role_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['user-role'] = $user_role_result['user-role'];
                    $REQUEST_ATTRIBUTES['status'] = $user_role_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $user_role_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "edit.php";
                    $REQUEST_ATTRIBUTES['title'] = "Edit UserRole";
                } else {
                    $REQUEST_ATTRIBUTES['user-role'] = $user_role_result['user-role'];
                    $REQUEST_ATTRIBUTES['status'] = $user_role_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $user_role_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "edit.php";
                    $REQUEST_ATTRIBUTES['title'] = "Edit User Role";
                }
            } else {
                $values['id'] = $REQUEST_PATHS[4];

                $user_role_result = UserRoleService::GetUserRole($values);
                if ($user_role_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['user-role'] = $user_role_result['user-role'];
                    $REQUEST_ATTRIBUTES['view'] = "edit.php";
                    $REQUEST_ATTRIBUTES['title'] = "Edit User Role";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        case "delete":
            $confirmed = filter_input(INPUT_POST, 'confirmed', FILTER_SANITIZE_STRING);
            if (isset($confirmed)) {

                $values['id'] = $REQUEST_PATHS[4];

                $user_role_result = UserRoleService::DeleteUserRole($values);

                if ($user_role_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['status'] = $user_role_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $user_role_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "delete.php";
                    $REQUEST_ATTRIBUTES['title'] = "Delete User Role";
                } else {
                    $REQUEST_ATTRIBUTES['status'] = $user_role_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $user_role_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "delete.php";
                    $REQUEST_ATTRIBUTES['title'] = "Delete User Role";
                }
            } else {
                $values['id'] = $REQUEST_PATHS[4];

                $user_role_result = UserRoleService::GetUserRole($values);
                if ($user_role_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['user-role'] = $user_role_result['user-role'];
                    $REQUEST_ATTRIBUTES['view'] = "confirm-deletion.php";
                    $REQUEST_ATTRIBUTES['title'] = "Confirm User Role Deletion";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        default :
            header('location: ' . CONTEXT_PATH . '/404');
            exit;
            break;
    }
} else {
    $user_role_result = UserRoleService::GetUserRoles();
    if ($user_role_result['status'] == 1) {
        $REQUEST_ATTRIBUTES['user-roles'] = $user_role_result['user-roles'];
        $REQUEST_ATTRIBUTES['pagination'] = $user_role_result['pagination'];
        $REQUEST_ATTRIBUTES['columns'] = $user_role_result['columns'];
        $REQUEST_ATTRIBUTES['view'] = "get.php";
        $REQUEST_ATTRIBUTES['title'] = "User Roles";
    } else {
        header('location: ' . CONTEXT_PATH . '/user-generated-error');
        exit;
    }
}

require BASE_PATH . '/views/backend/user-management/user-role.php';
