<?php

use HealthCare\User\Impl\UserProfileService;
use HealthCare\User\Impl\UserService;
use HealthCare\Util\Impl\CountryService;

require 'require-login.php';

global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;

UserService::AssertPermission("Manage User Profile", null, CONTEXT_PATH."/access-denied");

if (isset($REQUEST_PATHS[3])) {
    switch ($REQUEST_PATHS[3]) {
        case "create":
            $country_result = CountryService::GetCountryNames();
            $user_result = UserService::GetUserNames();

            if (!$country_result['status'] || !$user_result['status']) {
                header('location: ' . CONTEXT_PATH . '/user-generated-error');
                exit;
            }

            $REQUEST_ATTRIBUTES['country-names'] = $country_result['country-names'];
            $REQUEST_ATTRIBUTES['user-names'] = $user_result['user-names'];

            $user_id = filter_input(INPUT_POST, 'user_id', FILTER_SANITIZE_STRING);
            $firstname = filter_input(INPUT_POST, 'firstname', FILTER_SANITIZE_STRING);
            $lastname = filter_input(INPUT_POST, 'lastname', FILTER_SANITIZE_STRING);
            $phone1 = filter_input(INPUT_POST, 'phone1', FILTER_SANITIZE_STRING);
            $phone2 = filter_input(INPUT_POST, 'phone2', FILTER_SANITIZE_STRING);
            $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
            $line_1 = filter_input(INPUT_POST, 'line_1', FILTER_SANITIZE_STRING);
            $line_2 = filter_input(INPUT_POST, 'line_2', FILTER_SANITIZE_STRING);
            $city = filter_input(INPUT_POST, 'city', FILTER_SANITIZE_STRING);
            $state = filter_input(INPUT_POST, 'state', FILTER_SANITIZE_STRING);
            $country = filter_input(INPUT_POST, 'country', FILTER_SANITIZE_STRING);
            if (isset($user_id) && isset($firstname) && isset($lastname) && isset($phone1) && isset($phone2) && isset($email) && isset($line_1) && isset($line_2) && isset($city) && isset($state) && isset($country)) {

                $values['user_id'] = $user_id;
                $values['firstname'] = $firstname;
                $values['lastname'] = $lastname;
                $values['phone1'] = $phone1;
                $values['phone2'] = $phone2;
                $values['email'] = $email;
                $values['line_1'] = $line_1;
                $values['line_2'] = $line_2;
                $values['city'] = $city;
                $values['state'] = $state;
                $values['country'] = $country;

                $values = array_map('trim', $values);

                $user_profile_result = UserProfileService::CreateUserProfile($values);
                if ($user_profile_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['user-profile'] = $user_profile_result['user-profile'];
                    $REQUEST_ATTRIBUTES['address'] = $user_profile_result['address'];
                    $REQUEST_ATTRIBUTES['status'] = $user_profile_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $user_profile_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "create.php";
                    $REQUEST_ATTRIBUTES['title'] = "Create User Profile";
                } else {
                    $REQUEST_ATTRIBUTES['user-profile'] = $user_profile_result['user-profile'];
                    $REQUEST_ATTRIBUTES['address'] = $user_profile_result['address'];
                    $REQUEST_ATTRIBUTES['status'] = $user_profile_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $user_profile_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "create.php";
                    $REQUEST_ATTRIBUTES['title'] = "Create User Profile";
                }
            } else {
                $user_profile_result = UserProfileService::GetCreateUserProfileTemplate();
                if ($user_profile_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['user-profile'] = $user_profile_result['user-profile'];
                    $REQUEST_ATTRIBUTES['address'] = $user_profile_result['address'];
                    $REQUEST_ATTRIBUTES['view'] = "create.php";
                    $REQUEST_ATTRIBUTES['title'] = "Create User Profile";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        case "edit":
            $country_result = CountryService::GetCountryNames();

            if (!$country_result['status']) {
                header('location: ' . CONTEXT_PATH . '/user-generated-error');
                exit;
            }

            $REQUEST_ATTRIBUTES['country-names'] = $country_result['country-names'];

            $firstname = filter_input(INPUT_POST, 'firstname', FILTER_SANITIZE_STRING);
            $lastname = filter_input(INPUT_POST, 'lastname', FILTER_SANITIZE_STRING);
            $phone1 = filter_input(INPUT_POST, 'phone1', FILTER_SANITIZE_STRING);
            $phone2 = filter_input(INPUT_POST, 'phone2', FILTER_SANITIZE_STRING);
            $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
            $line_1 = filter_input(INPUT_POST, 'line_1', FILTER_SANITIZE_STRING);
            $line_2 = filter_input(INPUT_POST, 'line_2', FILTER_SANITIZE_STRING);
            $city = filter_input(INPUT_POST, 'city', FILTER_SANITIZE_STRING);
            $state = filter_input(INPUT_POST, 'state', FILTER_SANITIZE_STRING);
            $country = filter_input(INPUT_POST, 'country', FILTER_SANITIZE_STRING);
            if (isset($firstname) && isset($lastname) && isset($phone1) && isset($phone2) && isset($email) && isset($line_1) && isset($line_2) && isset($city) && isset($state) && isset($country)) {


                $values['id'] = $REQUEST_PATHS[4];
                $values['firstname'] = $firstname;
                $values['lastname'] = $lastname;
                $values['phone1'] = $phone1;
                $values['phone2'] = $phone2;
                $values['email'] = $email;
                $values['line_1'] = $line_1;
                $values['line_2'] = $line_2;
                $values['city'] = $city;
                $values['state'] = $state;
                $values['country'] = $country;

                $values = array_map('trim', $values);

                $user_profile_result = UserProfileService::UpdateUserProfile($values);

                if ($user_profile_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['user-profile'] = $user_profile_result['user-profile'];
                    $REQUEST_ATTRIBUTES['address'] = $user_profile_result['address'];
                    $REQUEST_ATTRIBUTES['status'] = $user_profile_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $user_profile_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "edit.php";
                    $REQUEST_ATTRIBUTES['title'] = "Edit User Profile";
                } else {
                    $REQUEST_ATTRIBUTES['user-profile'] = $user_profile_result['user-profile'];
                    $REQUEST_ATTRIBUTES['address'] = $user_profile_result['address'];
                    $REQUEST_ATTRIBUTES['status'] = $user_profile_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $user_profile_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "edit.php";
                    $REQUEST_ATTRIBUTES['title'] = "Edit User Profile";
                }
            } else {
                $values['id'] = $REQUEST_PATHS[4];

                $user_profile_result = UserProfileService::GetUserProfile($values);
                if ($user_profile_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['user-profile'] = $user_profile_result['user-profile'];
                    $REQUEST_ATTRIBUTES['address'] = $user_profile_result['address'];
                    $REQUEST_ATTRIBUTES['view'] = "edit.php";
                    $REQUEST_ATTRIBUTES['title'] = "Edit User Profile";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        case "delete":
            $confirmed = filter_input(INPUT_POST, 'confirmed', FILTER_SANITIZE_STRING);
            if (isset($confirmed)) {

                $values['id'] = $REQUEST_PATHS[4];

                $user_profile_result = UserProfileService::DeleteUserProfile($values);

                if ($user_profile_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['status'] = $user_profile_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $user_profile_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "delete.php";
                    $REQUEST_ATTRIBUTES['title'] = "Delete UserProfile";
                } else {
                    $REQUEST_ATTRIBUTES['status'] = $user_profile_result['status'];
                    $REQUEST_ATTRIBUTES['error-msg'] = $user_profile_result['error-msg'];
                    $REQUEST_ATTRIBUTES['view'] = "delete.php";
                    $REQUEST_ATTRIBUTES['title'] = "Delete UserProfile";
                }
            } else {
                $values['id'] = $REQUEST_PATHS[4];

                $user_profile_result = UserProfileService::GetUserProfile($values);
                if ($user_profile_result['status'] == 1) {
                    $REQUEST_ATTRIBUTES['user-profile'] = $user_profile_result['user-profile'];
                    $REQUEST_ATTRIBUTES['view'] = "confirm-deletion.php";
                    $REQUEST_ATTRIBUTES['title'] = "Confirm UserProfile Deletion";
                } else {
                    header('location: ' . CONTEXT_PATH . '/user-generated-error');
                    exit;
                }
            }
            break;
        default :
            header('location: ' . CONTEXT_PATH . '/404');
            exit;
            break;
    }
} else {
    $user_profile_result = UserProfileService::GetUserProfiles();
    if ($user_profile_result['status'] == 1) {
        $REQUEST_ATTRIBUTES['user-profiles'] = $user_profile_result['user-profiles'];
        $REQUEST_ATTRIBUTES['pagination'] = $user_profile_result['pagination'];
        $REQUEST_ATTRIBUTES['columns'] = $user_profile_result['columns'];
        $REQUEST_ATTRIBUTES['view'] = "get.php";
        $REQUEST_ATTRIBUTES['title'] = "User Profiles";
    } else {
        header('location: ' . CONTEXT_PATH . '/user-generated-error');
        exit;
    }
}

require BASE_PATH . '/views/backend/user-management/user-profile.php';
