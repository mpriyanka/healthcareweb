<?php

use HealthCare\Site\Impl\DoctorService;

DoctorService::Logout();
DoctorService::Redirect(CONTEXT_PATH.'/doctor-login');