<?php

use HealthCare\Site\Impl\DoctorService;

require 'require-doctor-login.php';


global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;

$password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
$password2 = filter_input(INPUT_POST, 'password2', FILTER_SANITIZE_STRING);

if (isset($password) && isset($password)) {
    $values['password'] = $password;
    $values['password2'] = $password2;
    $values = array_map('trim', $values);

    $doctor_result = DoctorService::ChangePassword($values);
    $REQUEST_ATTRIBUTES['status'] = $doctor_result['status'];
    if(isset($doctor_result['doctor'])){
        $REQUEST_ATTRIBUTES['doctor'] = $doctor_result['doctor'];
    }
    $REQUEST_ATTRIBUTES['error-msg'] = $doctor_result['error-msg'];
}

require BASE_PATH . '/views/doctors/change-password.php';
