<?php

use HealthCare\Site\Impl\DoctorService;


global $REQUEST_PATHS, $REQUEST_ATTRIBUTES;

$username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);

if (DoctorService::IsAuthenticated()) {
    DoctorService::Logout();
} else if (isset($username)) {
    $login_result = DoctorService::ForgotPassword($username);
    $REQUEST_ATTRIBUTES = $login_result;
}

require BASE_PATH . '/views/doc-forgot-password.php';
