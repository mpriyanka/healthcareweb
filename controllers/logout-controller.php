<?php
use HealthCare\User\Impl\UserService;

UserService::Logout();
UserService::Redirect(CONTEXT_PATH.'/login');