<?php

use HealthCare\User\Impl\UserService;

$username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
$password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);

if (UserService::IsAuthenticated()) {
    UserService::Logout();
} else if (isset($username) && isset($password)) {
    UserService::Login($username, $password, CONTEXT_PATH.'/backend');
}

require BASE_PATH . '/views/login.php';
