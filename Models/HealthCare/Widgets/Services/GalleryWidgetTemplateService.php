<?php

namespace HealthCare\Widgets\Services;

use HealthCare\Dao\Exception\ExceptionManager;
use HealthCare\Util\AssertProperties;
use HealthCare\Widgets\GalleryWidgetTemplate;
use Exception;

class GalleryWidgetTemplateService {

    public static function GetNewReservationTemplate() {
        $result['gallery-widget'] = new GalleryWidgetTemplate();

        return $result;
    }

    public static function GetSnippet($values) {
        $pm = null;
        $result['status'] = 0;
        $result['error-msg'] = "";
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'folders');
            $assertProperties->addProperty($values, 'height');
            $assertProperties->addProperty($values, 'width');
            $assertProperties->addProperty($values, 'index');
            $assertProperties->addProperty($values, 'size');
            $assertProperties->assert();

            $gallery_widget = new GalleryWidgetTemplate();

            $result['gallery-widget'] = $gallery_widget;

            $gallery_widget->setValue('folder', $values['folders']);
            $gallery_widget->setValue('height', $values['height']);
            $gallery_widget->setValue('width', $values['width']);
            $gallery_widget->setValue('index', $values['index']);
            $gallery_widget->setValue('size', $values['size']);

            $gallery_widget->CheckValidationRules($pm);

            $result['gallery-widget'] = $gallery_widget;

            if (count($gallery_widget->ValidationErrors) == 0) {
                $snippet = "\${widget-gallery-- folder:".$gallery_widget->getValue('folder');
                if($gallery_widget->getValue('index') > 0){
                    $snippet .= ",index:".$gallery_widget->getValue('index');
                }
                if($gallery_widget->getValue('size') > 0){
                    $snippet .= ",size:".$gallery_widget->getValue('size');
                }
                $snippet .= "}";
                $gallery_widget->setValue('snippet', $snippet);
                $result['gallery-widget'] = $gallery_widget;
                $result['status'] = 1;
                $result['error-msg'] = 'Copy snippet code';
            } else {
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }
        return $result;
    }

}
