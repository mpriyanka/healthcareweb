<?php


namespace HealthCare\Widgets;

use HealthCare\Dao\Exception\ExceptionManager;
use HealthCare\Dao\PersistenceManager;
use HealthCare\Publishing\Site\Image;
use Exception;

class GalleryWidget extends Widget implements IWidget {

    private $folder;
    private $size;
    private $index;
    private $height;
    private $width;

    private function __construct($config) {
        $this->folder = (isset($config['folder'])) ? $config['folder'] : '';
        $this->index = (isset($config['index'])) ? $config['index'] : 0;
        $this->size = (isset($config['size'])) ? $config['size'] : 8;
        $this->height = (isset($config['height'])) ? $config['height'] : 180;
        $this->width = (isset($config['width'])) ? $config['width'] : 240;
    }

    public static function GetWidget($config) {
        return new GalleryWidget(parent::GetWidget($config));
    }

    public static function LoadWidgets(&$data) {

        $widgets = array();

        preg_match_all('/\${widget-gallery--([\w\W]*?)}/', $data, $widgets);

        for ($i = 0; $i < count($widgets[1]); $i++) {

            $gallery_widget = GalleryWidget::GetWidget(trim($widgets[1][$i]));

            if ($gallery_widget != null && $gallery_widget->processRequest()) {
                $data = str_replace($widgets[0][$i], $gallery_widget->getViewAsString(), $data);
            }
        }
    }

    public function getViewAsString() {
        ob_start();
        include BASE_PATH . '/views/includes/widget-images.php';
        return ob_get_clean();
    }

    public function processRequest() {
        global $REQUEST_ATTRIBUTES;
        try {

            $pm = PersistenceManager::NewPersistenceManager();
            $query = $pm->getQueryBuilder('HealthCare\Publishing\Site\Image');
            $sql = "select folder,title,path from " . Image::GetDSN() . "_active where folder = ?";
            $REQUEST_ATTRIBUTES['widget-images'] = $query->executeQuery($sql, array($this->folder), $this->index, $this->size);
            $REQUEST_ATTRIBUTES['widget-image-height'] = $this->height;
            $REQUEST_ATTRIBUTES['widget-image-width'] = $this->width;
            return true;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }
        return false;
    }

}
