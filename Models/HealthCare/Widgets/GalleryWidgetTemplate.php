<?php

namespace HealthCare\Widgets;

use HealthCare\Dao\IndexType;
use HealthCare\Dao\PersistableObject;
use HealthCare\Dao\Property;
use HealthCare\Dao\Validation\IllegalValueValidationRule;
use HealthCare\Dao\Validation\NumberValidationRule;
use HealthCare\Dao\Validation\ValidationRules;

class GalleryWidgetTemplate extends PersistableObject {

    function __construct() {
        //Persistable Data Members
        $this->properties['folder'] = new Property('folder', '', true, IndexType::Normal);
        $this->properties['height'] = new Property('height', 180, true, IndexType::Normal);
        $this->properties['width'] = new Property('width', 240, true, IndexType::Normal);
        $this->properties['index'] = new Property('index', 0, true, IndexType::Normal);
        $this->properties['size'] = new Property('size', 0, true, IndexType::Normal);
        $this->properties['snippet'] = new Property('snippet', '', true, IndexType::Normal);
        $this->properties['creation_date'] = new Property('creation_date', date('Y-m-d H:i:s'), true, IndexType::Normal);

        //Non-Persistable Data Members
        $this->properties['last_changed'] = new Property('last_changed', '', false, IndexType::Timestamp);

        //ValidationRules
        $this->ValidationRules = new ValidationRules();
        $this->ValidationRules->add(new NumberValidationRule('height', 1, -1, 'invalid'));
        $this->ValidationRules->add(new NumberValidationRule('width', 1, -1, 'invalid'));
        $this->ValidationRules->add(new NumberValidationRule('index', 0, -1, 'invalid'));
        $this->ValidationRules->add(new NumberValidationRule('size', 0, -1, 'invalid'));
        $this->ValidationRules->add(new IllegalValueValidationRule('folder', '-', 'invalid'));

    }

}
