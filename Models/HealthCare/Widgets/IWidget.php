<?php

namespace HealthCare\Widgets;

interface IWidget {
    
    public static function GetWidget($values);
    
    public function processRequest();
    
    public function getViewAsString();
    
}
