<?php

namespace HealthCare\Widgets;


class Widget {
    
    public static function GetWidget($config) {
        $new_config = array();
        $array = explode(',', $config);
        for($i = 0; $i < count($array); $i++){
            $name_value = explode(':', $array[$i]);
            if(count($name_value) == 2){
                $new_config[$name_value[0]] = $name_value[1];
            }
        }
        return $new_config;
    }
}
