<?php

namespace HealthCare\Publishing\Site;

use HealthCare\Dao\IndexType;
use HealthCare\Dao\PersistableObject;
use HealthCare\Dao\Property;
use HealthCare\Dao\Validation\CanonicalPathValidationRule;
use HealthCare\Dao\Validation\NumberValidationRule;
use HealthCare\Dao\Validation\StringValidationRule;
use HealthCare\Dao\Validation\ValidationRules;

class Page extends PersistableObject {

    function __construct() {
        //Persistable Data Members
        $this->properties['id'] = new Property('id', str_replace(".", "", uniqid(rand(), true)), true, IndexType::PrimaryKey);
        $this->properties['title'] = new Property('title', '', true, IndexType::Normal);
        $this->properties['url'] = new Property('url', '', true, IndexType::Normal);
        $this->properties['body'] = new Property('body', '', true, IndexType::Normal);
        $this->properties['order_index'] = new Property('order_index', '', true, IndexType::Normal);
        $this->properties['tags'] = new Property('tags', '', true, IndexType::Normal);
        $this->properties['creation_date'] = new Property('creation_date', date('Y-m-d H:i:s'), true, IndexType::Normal);

        //Non-Persistable Data Members
        $this->properties['last_changed'] = new Property('last_changed', '', false, IndexType::Timestamp);

        //ValidationRules
        $this->ValidationRules = new ValidationRules();
        $this->ValidationRules->add(new StringValidationRule('title', 2, 650, 'title'));
        $this->ValidationRules->add(new CanonicalPathValidationRule('url', 2, 650, 'url'));
        $this->ValidationRules->add(new NumberValidationRule('order_index', 1, -1, 'order_index'));
        $this->ValidationRules->add(new StringValidationRule('tags', 0, 200, 'tags'));

        //TrackedPropertyChanges
        $this->TrackedPropertyChanges[] = 'title';
        $this->TrackedPropertyChanges[] = 'url';
        $this->TrackedPropertyChanges[] = 'body';
        $this->TrackedPropertyChanges[] = 'order_index';
        $this->TrackedPropertyChanges[] = 'tags';
    }

    public static function GetDSN() {
        return DB_NAME . '.pages';
    }

}
