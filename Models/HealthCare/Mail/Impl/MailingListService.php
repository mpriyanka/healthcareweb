<?php

namespace HealthCare\Mail\Impl;

use HealthCare\Dao\Exception\ExceptionManager;
use HealthCare\Dao\PersistenceManager;
use HealthCare\Mail\MailingList;
use Exception;
use PHPMailer;

class MailingListService {

    public static function Run($values) {

        try {
            $pm = PersistenceManager::getConnection();
            $query = $pm->getQueryBuilder('HealthCare\Mail\MailingList');
            $sql = "select * from " . MailingList::GetDSN() . " where sent = ? order by priority";
            $mailing_list = $query->executeQuery($sql, array(0), 0, $values['size']);
            for ($i = 0; $i < $mailing_list->count(); $i++) {
                if(self::SendMail($mailing_list[$i])){
                    $sql_update = 'update '.MailingList::GetDSN()." set sent = ? where id = ?";
                    $query->execute($sql_update, array(1, $mailing_list[$i]->getValue('id')));
                }
            }
            
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }
    }

    private static function SendMail($mailing_list) {
        try {
            $mail = new PHPMailer(true);

            $mail->IsSMTP();

            $mail->SMTPAuth = true;                  // enable SMTP authentication

            $mail->Host = SMTP_HOST;

            $mail->SMTPSecure = "ssl"; // sets the SMTP server

            $mail->Port = SMTP_PORT;                    // set the SMTP port for the GMAIL server

            $mail->Username = SMTP_USERNAME; // SMTP account username

            $mail->Password = SMTP_PASSWORD;

            $mail->From = $mailing_list->getValue('sender');
            $mail->FromName = $mailing_list->getValue('name');  // Add a recipient
            $mail->AddAddress($mailing_list->getValue('recipient'));               // Name is optional

            $mail->IsHTML(true);                                  // Set email format to HTML
            
            $attachments = json_decode($mailing_list->getValue('attachments'), true);
            
            foreach($attachments as $attachment){
                $array = explode('.', $attachment['file_path']);
                $mail->AddStringAttachment(file_get_contents(BASE_PATH.'/'.$attachment['file_path']), $attachment['type']. '.' . $array[count($array) - 1]);
            }

            $mail->Subject = $mailing_list->getValue('subject');
            $mail->MsgHTML("<html><head></head><body>" . html_entity_decode($mailing_list->getValue('message')) . "</body></html>");

            $mail->Send();
            return true;
        } catch (Exception $ex) {
            ExceptionManager::RaiseException($ex);
        }
        return false;
    }

}
