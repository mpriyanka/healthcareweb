<?php

namespace HealthCare\Mail;

use HealthCare\Dao\IndexType;
use HealthCare\Dao\PersistableObject;
use HealthCare\Dao\Property;
use HealthCare\Dao\Validation\EmailValidationRule;
use HealthCare\Dao\Validation\NumberValidationRule;
use HealthCare\Dao\Validation\StringValidationRule;
use HealthCare\Dao\Validation\ValidationRules;

class MailingList extends PersistableObject {

    function __construct() {
        //Persistable Data Members
        $this->properties['id'] = new Property('id', str_replace(".", "", uniqid(rand(), true)), true, IndexType::PrimaryKey);
        $this->properties['sender'] = new Property('sender', '', true, IndexType::Normal);
        $this->properties['name'] = new Property('name', '', true, IndexType::Normal);
        $this->properties['recipient'] = new Property('recipient', '', true, IndexType::Normal);
        $this->properties['read'] = new Property('read', 0, true, IndexType::Normal);
        $this->properties['sent'] = new Property('sent', 0, true, IndexType::Normal);
        $this->properties['attachments'] = new Property('attachments', '', true, IndexType::Normal);
        $this->properties['subject'] = new Property('subject', '', true, IndexType::Normal);
        $this->properties['message'] = new Property('message', '', true, IndexType::Normal);
        $this->properties['priority'] = new Property('priority', 0, true, IndexType::Normal);
        $this->properties['creation_date'] = new Property('creation_date', date('Y-m-d H:i:s'), true, IndexType::Normal);

        //Non-Persistable Data Members
        $this->properties['last_changed'] = new Property('last_changed', '', false, IndexType::Timestamp);

        //ValidationRules
        $this->ValidationRules = new ValidationRules();
        $this->ValidationRules->add(new EmailValidationRule('sender', false, 'sender'));
        $this->ValidationRules->add(new EmailValidationRule('recipient', false, 'recipient'));
        $this->ValidationRules->add(new NumberValidationRule('priority', 0, -1, 'priority'));
        $this->ValidationRules->add(new NumberValidationRule('read', 0, 1, 'read'));
        $this->ValidationRules->add(new NumberValidationRule('sent', 0, 1, 'sent'));
        $this->ValidationRules->add(new StringValidationRule('subject', 4, 300, 'subject'));
        $this->ValidationRules->add(new StringValidationRule('name', 4, 300, 'name'));

        //TrackedPropertyChanges
    }

    public static function GetDSN() {
        return DB_NAME . '.mailing_list';
    }

}
