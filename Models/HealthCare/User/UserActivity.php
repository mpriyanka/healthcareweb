<?php
namespace HealthCare\User;

use HealthCare\Dao\IndexType;
use HealthCare\Dao\PersistableObject;
use HealthCare\Dao\Property;
use HealthCare\Dao\Validation\IllegalValueValidationRule;
use HealthCare\Dao\Validation\StringValidationRule;
use HealthCare\Dao\Validation\ValidationRules;

class UserActivity extends PersistableObject {

    function  __construct() {
        //Persistable Data Members
        $this->properties['id'] = new Property('id', str_replace(".", "", uniqid(rand(), true)), true, IndexType::PrimaryKey);
        $this->properties['user_id'] = new Property('user_id', '', true, IndexType::Normal);
        $this->properties['activity'] = new Property('activity', '', true, IndexType::Normal);
        $this->properties['url'] = new Property('url', '', true, IndexType::Normal);
        $this->properties['ip_address'] = new Property('ip_address', '', true, IndexType::Normal);
        $this->properties['referer'] = new Property('referer', '', true, IndexType::Normal);
        $this->properties['user_agent'] = new Property('user_agent', '', true, IndexType::Normal);
        $this->properties['creation_date'] = new Property('creation_date', date('Y-m-d H:i:s'), true, IndexType::Normal);

        //Non-Persistable Data Members
        $this->properties['last_changed'] = new Property('last_changed','',false, IndexType::Timestamp);

        //ValidationRules
        $this->ValidationRules = new ValidationRules();
        $this->ValidationRules->add(new IllegalValueValidationRule('user_id', '-', 'user_id'));
        $this->ValidationRules->add(new StringValidationRule('activity', 2, 30, 'activity'));
        
        //TrackedPropertyChanges
        $this->TrackedPropertyChanges[] = 'user_id';
        $this->TrackedPropertyChanges[] = 'activity';
        $this->TrackedPropertyChanges[] = 'url';
        $this->TrackedPropertyChanges[] = 'ip_address';
        $this->TrackedPropertyChanges[] = 'referer';
        $this->TrackedPropertyChanges[] = 'user_agent';
    }

    public static function GetDSN() {
        return DB_NAME.'.user_activities';
    }

}
