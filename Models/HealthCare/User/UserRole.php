<?php
namespace HealthCare\User;

use HealthCare\Dao\IndexType;
use HealthCare\Dao\PersistableObject;
use HealthCare\Dao\Property;
use HealthCare\Dao\Validation\IllegalValueValidationRule;
use HealthCare\Dao\Validation\ValidationRules;

class UserRole extends PersistableObject {

    function  __construct() {
        //Persistable Data Members
        $this->properties['id'] = new Property('id', str_replace(".", "", uniqid(rand(), true)), true, IndexType::PrimaryKey);
        $this->properties['user_id'] = new Property('user_id', '', true, IndexType::Normal);
        $this->properties['role_id'] = new Property('role_id', '', true, IndexType::Normal);
        $this->properties['creation_date'] = new Property('creation_date', date('Y-m-d H:i:s'), true, IndexType::Normal);
        $this->properties['deleted'] = new Property('deleted', 0, true, IndexType::Normal);
        $this->properties['delete_timestamp'] = new Property('delete_timestamp', '0000-00-00 00:00:00', true, IndexType::Nullable);

        //Non-Persistable Data Members
        $this->properties['last_changed'] = new Property('last_changed','',false, IndexType::Timestamp);

        //ValidationRules
        $this->ValidationRules = new ValidationRules();
        $this->ValidationRules->add(new IllegalValueValidationRule('user_id', '-', 'user_id'));
        $this->ValidationRules->add(new IllegalValueValidationRule('role_id', '-','role_id'));
        
        //TrackedPropertyChanges
        $this->TrackedPropertyChanges[] = 'user_id';
        $this->TrackedPropertyChanges[] = 'role_id';
        $this->TrackedPropertyChanges[] = 'deleted';
        $this->TrackedPropertyChanges[] = 'delete_timestamp';
    }

    public static function GetDSN() {
        return DB_NAME.'.user_roles';
    }

}
