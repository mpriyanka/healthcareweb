<?php
namespace HealthCare\User;

use HealthCare\Dao\IndexType;
use HealthCare\Dao\PersistableObject;
use HealthCare\Dao\Property;
use HealthCare\Dao\Validation\EmailValidationRule;
use HealthCare\Dao\Validation\NumberValidationRule;
use HealthCare\Dao\Validation\SameValueValidationRule;
use HealthCare\Dao\Validation\StringValidationRule;
use HealthCare\Dao\Validation\ValidationRules;

class User extends PersistableObject {

    function  __construct() {
        //Persistable Data Members
        $this->properties['id'] = new Property('id', str_replace(".", "", uniqid(rand(), true)), true, IndexType::PrimaryKey);
        $this->properties['username'] = new Property('username', '', true, IndexType::Normal);
        $this->properties['password'] = new Property('password', '', true, IndexType::Normal);
        $this->properties['enabled'] = new Property('enabled', '', true, IndexType::Normal);
        $this->properties['creation_date'] = new Property('creation_date', date('Y-m-d H:i:s'), true, IndexType::Normal);
        $this->properties['deleted'] = new Property('deleted', 0, true, IndexType::Normal);
        $this->properties['delete_timestamp'] = new Property('delete_timestamp', '0000-00-00 00:00:00', true, IndexType::Nullable);

        //Non-Persistable Data Members
        $this->properties['confirm_username'] = new Property('confirm_username', '', false, IndexType::Normal);
        $this->properties['last_changed'] = new Property('last_changed','',false, IndexType::Timestamp);

        //ValidationRules
        $this->ValidationRules = new ValidationRules();
        $this->ValidationRules->add(new EmailValidationRule('username', false, 'username'));
        $this->ValidationRules->add(new SameValueValidationRule('confirm_username', 'username', 'confirm_username'));
        $this->ValidationRules->add(new StringValidationRule('password', 6, 40, 'password'));
        $this->ValidationRules->add(new NumberValidationRule('enabled', 0, 1, 'enabled'));
        
        //TrackedPropertyChanges
        $this->TrackedPropertyChanges[] = 'username';
        $this->TrackedPropertyChanges[] = 'password';
        $this->TrackedPropertyChanges[] = 'enabled';
        $this->TrackedPropertyChanges[] = 'deleted';
        $this->TrackedPropertyChanges[] = 'delete_timestamp';
    }

    public static function GetDSN() {
        return DB_NAME.'.users';
    }

}
