<?php
namespace HealthCare\User;

use HealthCare\Dao\IndexType;
use HealthCare\Dao\PersistableObject;
use HealthCare\Dao\Property;
use HealthCare\Dao\Validation\EmailValidationRule;
use HealthCare\Dao\Validation\IllegalValueValidationRule;
use HealthCare\Dao\Validation\InternationalPhoneValidationRule;
use HealthCare\Dao\Validation\StringValidationRule;
use HealthCare\Dao\Validation\ValidationRules;

class UserProfile extends PersistableObject {

    function  __construct() {
        //Persistable Data Members
        $this->properties['id'] = new Property('id', str_replace(".", "", uniqid(rand(), true)), true, IndexType::PrimaryKey);
        $this->properties['user_id'] = new Property('user_id', '', true, IndexType::Normal);
        $this->properties['firstname'] = new Property('firstname', '', true, IndexType::Normal);
        $this->properties['lastname'] = new Property('lastname', '', true, IndexType::Normal);
        $this->properties['phone1'] = new Property('phone1', '', true, IndexType::Normal);
        $this->properties['phone2'] = new Property('phone2', '', true, IndexType::Normal);
        $this->properties['email'] = new Property('email', '', true, IndexType::Normal);
        $this->properties['address_id'] = new Property('address_id', '', true, IndexType::Normal);
        $this->properties['creation_date'] = new Property('creation_date', date('Y-m-d H:i:s'), true, IndexType::Normal);
        $this->properties['deleted'] = new Property('deleted', 0, true, IndexType::Normal);
        $this->properties['delete_timestamp'] = new Property('delete_timestamp', '0000-00-00 00:00:00', true, IndexType::Nullable);

        //Non-Persistable Data Members
        $this->properties['last_changed'] = new Property('last_changed','',false, IndexType::Timestamp);

        //ValidationRules
        $this->ValidationRules = new ValidationRules();
        $this->ValidationRules->add(new IllegalValueValidationRule('user_id', '-', 'user_id'));
        $this->ValidationRules->add(new StringValidationRule('firstname', 2, 25, 'name'));
        $this->ValidationRules->add(new StringValidationRule('lastname', 2, 25, 'name'));
        $this->ValidationRules->add(new InternationalPhoneValidationRule('phone1', false, 'phone'));
        $this->ValidationRules->add(new InternationalPhoneValidationRule('phone2', true, 'phone'));
        $this->ValidationRules->add(new EmailValidationRule('email', true, 'email'));
        $this->ValidationRules->add(new IllegalValueValidationRule('address_id', '-','address_id'));
        
        //TrackedPropertyChanges
        $this->TrackedPropertyChanges[] = 'user_id';
        $this->TrackedPropertyChanges[] = 'firstname';
        $this->TrackedPropertyChanges[] = 'lastname';
        $this->TrackedPropertyChanges[] = 'phone1';
        $this->TrackedPropertyChanges[] = 'phone2';
        $this->TrackedPropertyChanges[] = 'email';
        $this->TrackedPropertyChanges[] = 'address_id';
        $this->TrackedPropertyChanges[] = 'deleted';
        $this->TrackedPropertyChanges[] = 'delete_timestamp';
    }

    public static function GetDSN() {
        return DB_NAME.'.user_profiles';
    }

}
