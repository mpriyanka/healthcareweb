<?php

namespace HealthCare\User\Impl;

use HealthCare\Dao\Exception\ExceptionManager;
use HealthCare\Dao\PersistenceManager;
use HealthCare\User\Role;
use HealthCare\Util\AssertProperties;
use HealthCare\Util\DataPagination;
use HealthCare\Util\DataTable;
use Exception;

class RoleService {

    public static function GetRoles() {
        $result['status'] = 0;
        try {
            $columns['name'] = array("sql" => "name", 'display_name' => 'Name', 'data_type' => 'String');
            $columns['creation_date'] = array("sql" => "creation_date", 'display_name' => 'Creation Date', 'data_type' => 'Date');
            $columns['last_changed'] = array("sql" => "last_changed", 'display_name' => 'Last Changed', 'data_type' => 'Date');

            $query_prpoperties_result = DataTable::GetQueryProperties($columns, "name", "asc");

            $pm = PersistenceManager::NewPersistenceManager();
            $query = $pm->getQueryBuilder('HealthCare\User\Role');
            $sql = "select * from " . Role::GetDSN() . "_active " . $query_prpoperties_result['query'] . " order by " . $query_prpoperties_result['order_by'] . " " . $query_prpoperties_result['order_dir'];
            $row = $query->execute(str_replace("select *", "select count(*) as _row_count", $sql), $query_prpoperties_result['values'])->fetch();
            $total = $row['_row_count'];
            $pagination = new DataPagination();
            $pagination->setIndex($query_prpoperties_result['page_index']);
            $pagination->setSize(DATA_PAGE_SIZE);
            $pagination->setTotal($total);
            $pagination->setPages();
            $result['roles'] = $query->executeQuery($sql, $query_prpoperties_result['values'], (($query_prpoperties_result['page_index'] - 1) * DATA_PAGE_SIZE), DATA_PAGE_SIZE);
            $pagination->setPageCount($result['roles']->count());
            $result['pagination'] = $pagination;
            $result['columns'] = $columns;
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
            $result['error-msg'] = $e;
        }
        return $result;
    }

    public static function GetRoleNames() {
        $result['status'] = 0;
        try {
            $pm = PersistenceManager::NewPersistenceManager();
            $query = $pm->getQueryBuilder('HealthCare\User\Role');
            $sql = "select id,name from " . Role::GetDSN() . "_active order by name";
            $result['role-names'] = $query->executeQuery($sql, array(), 0, -1);
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
            $result['error-msg'] = $e;
        }
        return $result;
    }

    public static function GetCreateRoleTemplate() {
        $result['status'] = 0;
        $result['role'] = new Role();
        $result['status'] = 1;
        return $result;
    }

    public static function CreateRole($values) {
        $pm = null;
        $result['role'] = new Role();
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'name');

            $assertProperties->assert();

            $role = $result['role'];

            $role->setValue('name', $values['name']);

            $role->CheckValidationRules($pm);

            $result['role'] = $role;

            if (count($role->ValidationErrors) == 0) {
                $pm = PersistenceManager::getConnection();
                $pm->beginTransaction();
                $pm->saveWithAudit($role, $_SESSION['IPrincipal']['id']);
                $pm->commit();
                $result['status'] = 1;
                $result['role'] = new Role();
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            if (strpos($e, '1062 Duplicate entry') != false) {
                $result['error-msg'] = "Role already exists";
            } else {
                ExceptionManager::RaiseException($e);
            }
        }

        return $result;
    }

    public static function GetRole($values) {
        $result['status'] = 0;
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'id');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();
            $result['role'] = $pm->getObjectById('HealthCare\User\Role', $values['id']);
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }

        return $result;
    }

    public static function UpdateRole($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'name');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();
            $role = $pm->getObjectById('HealthCare\User\Role', $values['id']);

            $role->setValue('name', $values['name']);

            $role->CheckValidationRules($pm);

            $result['role'] = $role;

            if (count($role->ValidationErrors) == 0) {
                $pm->beginTransaction();
                $pm->saveWithAudit($role, $_SESSION['IPrincipal']['id']);
                $pm->commit();
                $result['status'] = 1;
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            if (strpos($e, '1062 Duplicate entry') != false) {
                $result['error-msg'] = "Role already exists";
            } else {
                ExceptionManager::RaiseException($e);
            }
        }

        return $result;
    }

    public static function DeleteRole($values) {
        $result['status'] = 0;
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'id');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $role = $pm->getObjectById('HealthCare\User\Role', $values['id']);
            if ($role != null) {
                $pm->beginTransaction();
                $role->setValue('deleted', 1);
                $role->setValue('delete_timestamp', date('Y-m-d H:i:s'));
                $pm->saveWithAudit($role, $_SESSION['IPrincipal']['id']);
                if (UserService::IsLastFullAdministrator($pm)) {
                    $result['error-msg'] = 'An active User must exist with Full Administrative Permission';
                    $pm->rollBack();
                } else {
                    $result['status'] = 1;
                    $pm->commit();
                }
            } else {
                $result['error-msg'] = 'Role not found';
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }
        return $result;
    }

}
