<?php

namespace HealthCare\User\Impl;

use HealthCare\Dao\Exception\ExceptionManager;
use HealthCare\Dao\PersistenceManager;
use HealthCare\User\Role;
use HealthCare\User\User;
use HealthCare\User\UserRole;
use HealthCare\Util\AssertProperties;
use HealthCare\Util\DataPagination;
use HealthCare\Util\DataTable;
use Exception;

class UserRoleService {

    public static function GetUserRoles() {
        $result['status'] = 0;
        try {
            $columns['username'] = array("sql" => "u.username", 'display_name' => 'Username', 'data_type' => 'String');
            $columns['role'] = array("sql" => "r.name", 'display_name' => 'Role', 'data_type' => 'String');
            $columns['creation_date'] = array("sql" => "ur.creation_date", 'display_name' => 'Creation Date', 'data_type' => 'Date');
            $columns['last_changed'] = array("sql" => "ur.last_changed", 'display_name' => 'Last Changed', 'data_type' => 'Date');

            $query_prpoperties_result = DataTable::GetQueryProperties($columns, "username", "asc");

            $pm = PersistenceManager::NewPersistenceManager();
            $query = $pm->getQueryBuilder('HealthCare\User\UserRole');
            $sql = "select ur.id,u.username as user,r.name as role,ur.creation_date,ur.last_changed from " . UserRole::GetDSN() . "_active as ur inner join " . Role::GetDSN() . " as r on ur.role_id = r.id inner join " . User::GetDSN() . "_active as u on ur.user_id = u.id " . $query_prpoperties_result['query'] . " order by " . $query_prpoperties_result['order_by'] . " " . $query_prpoperties_result['order_dir'];
            $row = $query->execute(str_replace("select", "select count(*) as _row_count,", $sql), $query_prpoperties_result['values'])->fetch();
            $total = $row['_row_count'];
            $pagination = new DataPagination();
            $pagination->setIndex($query_prpoperties_result['page_index']);
            $pagination->setSize(DATA_PAGE_SIZE);
            $pagination->setTotal($total);
            $pagination->setPages();
            $result['user-roles'] = $query->executeQuery($sql, $query_prpoperties_result['values'], (($query_prpoperties_result['page_index'] - 1) * DATA_PAGE_SIZE), DATA_PAGE_SIZE);
            $pagination->setPageCount($result['user-roles']->count());
            $result['pagination'] = $pagination;
            $result['columns'] = $columns;
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
            $result['error-msg'] = $e;
        }
        return $result;
    }

    public static function GetCreateUserRoleTemplate() {
        $result['status'] = 0;
        $result['user-role'] = new UserRole();
        $result['status'] = 1;
        return $result;
    }

    public static function CreateUserRole($values) {
        $result['user-role'] = new UserRole();
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'role_id');
            $assertProperties->addProperty($values, 'user_id');

            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $user_role = $result['user-role'];

            $user_role->setValue('role_id', $values['role_id']);
            $user_role->setValue('user_id', $values['user_id']);

            $user_role->CheckValidationRules($pm);

            $result['user-role'] = $user_role;

            if (count($user_role->ValidationErrors) == 0) {
                $pm->beginTransaction();
                $pm->saveWithAudit($user_role, $_SESSION['IPrincipal']['id']);
                $pm->commit();
                $result['status'] = 1;
                $result['user-role'] = new UserRole();
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            if (strpos($e, '1062 Duplicate entry') != false) {
                $result['error-msg'] = "User Role already exists";
            } else {
                ExceptionManager::RaiseException($e);
            }
        }

        return $result;
    }

    public static function GetUserRole($values) {
        $result['status'] = 0;
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'id');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();
            $result['user-role'] = $pm->getObjectById('HealthCare\User\UserRole', $values['id']);
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }

        return $result;
    }

    public static function UpdateUserRole($values) {
        $result['status'] = 0;
        $result['error-msg'] = "";
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'id');
            $assertProperties->addProperty($values, 'role_id');
            $assertProperties->addProperty($values, 'user_id');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();
            $user_role = $pm->getObjectById('HealthCare\User\UserRole', $values['id']);

            $user_role->setValue('role_id', $values['role_id']);
            $user_role->setValue('user_id', $values['user_id']);

            $user_role->CheckValidationRules($pm);

            $result['user-role'] = $user_role;

            if (count($user_role->ValidationErrors) == 0) {
                $pm->beginTransaction();
                $pm->saveWithAudit($user_role, $_SESSION['IPrincipal']['id']);
                if (UserService::IsLastFullAdministrator($pm)) {
                    throw new Exception("1100");
                }
                $pm->commit();
                $result['status'] = 1;
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            if (strpos($e, '1100') != false) {
                $result['error-msg'] = 'An active User must exist with Full Administrative Permission';
            } else if (strpos($e, '1062 Duplicate entry') != false) {
                $result['error-msg'] = "User Role already exists";
            } else {
                ExceptionManager::RaiseException($e);
            }
        }

        return $result;
    }

    public static function DeleteUserRole($values) {
        $result['status'] = 0;
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'id');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $user_role = $pm->getObjectById('HealthCare\User\UserRole', $values['id']);
            if ($user_role != null) {
                $pm->beginTransaction();
                $user_role->setValue('deleted', 1);
                $user_role->setValue('delete_timestamp', date('Y-m-d H:i:s'));
                $pm->saveWithAudit($user_role, $_SESSION['IPrincipal']['id']);
                if (UserService::IsLastFullAdministrator($pm)) {
                    $result['error-msg'] = 'An active User must exist with Full Administrative Permission';
                    $pm->rollBack();
                } else {
                    $result['status'] = 1;
                    $pm->commit();
                }
            } else {
                $result['error-msg'] = 'User Role not found';
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }
        return $result;
    }

}
