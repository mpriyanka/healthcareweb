<?php

namespace HealthCare\User\Impl;

use HealthCare\Dao\Exception\ExceptionManager;
use HealthCare\Dao\PersistenceManager;
use HealthCare\User\Permission;
use HealthCare\User\RolePermission;
use HealthCare\User\User;
use HealthCare\User\UserRole;
use HealthCare\Util\AssertProperties;
use HealthCare\Util\DataPagination;
use HealthCare\Util\DataTable;
use HealthCare\Util\Impl\EmailNotificationService;
use Exception;

class UserService {

    public static function GetUsers() {
        $result['status'] = 0;
        try {
            $columns['username'] = array("sql" => "username", 'display_name' => 'Username', 'data_type' => 'String');
            $columns['enabled'] = array("sql" => "enabled", 'display_name' => 'Enabled', 'data_type' => 'Dropdown', 'dropdown_list' => array('No', 'Yes'));
            $columns['creation_date'] = array("sql" => "creation_date", 'display_name' => 'Creation Date', 'data_type' => 'Date');
            $columns['last_changed'] = array("sql" => "last_changed", 'display_name' => 'Last Changed', 'data_type' => 'Date');


            $query_prpoperties_result = DataTable::GetQueryProperties($columns, "username", "asc");

            $pm = PersistenceManager::NewPersistenceManager();
            $query = $pm->getQueryBuilder('HealthCare\User\User');
            $sql = "select * from " . User::GetDSN() . "_active " . $query_prpoperties_result['query'] . " order by " . $query_prpoperties_result['order_by'] . " " . $query_prpoperties_result['order_dir'];
            $row = $query->execute(str_replace("select *", "select count(*) as _row_count", $sql), $query_prpoperties_result['values'])->fetch();
            $total = $row['_row_count'];
            $pagination = new DataPagination();
            $pagination->setIndex($query_prpoperties_result['page_index']);
            $pagination->setSize(DATA_PAGE_SIZE);
            $pagination->setTotal($total);
            $pagination->setPages();
            $result['users'] = $query->executeQuery($sql, $query_prpoperties_result['values'], (($query_prpoperties_result['page_index'] - 1) * DATA_PAGE_SIZE), DATA_PAGE_SIZE);
            $pagination->setPageCount($result['users']->count());
            $result['pagination'] = $pagination;
            $result['columns'] = $columns;
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
            $result['error-msg'] = $e;
        }
        return $result;
    }

    public static function GetUserNames() {
        $result['status'] = 0;
        try {
            $pm = PersistenceManager::NewPersistenceManager();
            $query = $pm->getQueryBuilder('HealthCare\User\User');
            $sql = "select id,username from " . User::GetDSN() . "_active order by username";
            $result['user-names'] = $query->executeQuery($sql, array(), 0, -1);
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
            $result['error-msg'] = $e;
        }
        return $result;
    }

    public static function GetCreateUserTemplate() {
        $result['status'] = 0;
        $result['user'] = new User();
        $result['status'] = 1;
        return $result;
    }

    public static function CreateUser($values) {
        $pm = null;
        $result['user'] = new User();
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'username');
            $assertProperties->addProperty($values, 'confirm_username');
            $assertProperties->addProperty($values, 'password');

            $assertProperties->assert();

            $user = $result['user'];

            $user->setValue('username', $values['username']);
            $user->setValue('confirm_username', $values['confirm_username']);
            $user->setValue('password', md5($user->getValue('id') . $values['password'] . strtolower($user->getValue('username'))));
            $user->setValue('enabled', 1);
            
            $password = $values['password'];

            $user->CheckValidationRules($pm);

            $result['user'] = $user;

            if (count($user->ValidationErrors) == 0) {
                $pm = PersistenceManager::getConnection();
                $pm->beginTransaction();
                $pm->saveWithAudit($user, $_SESSION['IPrincipal']['id']);
                $pm->commit();
                $values = array();
                $values['subject'] = "New Account Info";
                $values['url'] = "backend-account-creation-by-admin";
                $values['email'] = $user->getValue('username');
                $values['placeholder']['${backend_link}'] = "<a href=\"" . CONTEXT_PATH . "/backend\">" . CONTEXT_PATH . "/backend</a>";
                $values['placeholder']['${site_name}'] = SITE_NAME;
                $values['placeholder']['${billing_email}'] = BILLING_EMAIL;
                $values['placeholder']['${support_email}'] = SUPPORT_EMAIL;
                $values['placeholder']['${password}'] = $password;
                $values['placeholder']['${username}'] = $user->getValue('username');
                EmailNotificationService::Sendmail($values);
                $result['status'] = 1;
                $result['user'] = new User();
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
                $user->setValue('password', $values['password']);
                $result['user'] = $user;
            }
        } catch (Exception $e) {
            if (strpos($e, '1062 Duplicate entry') != false) {
                $result['error-msg'] = "User already exists";
            } else {
                ExceptionManager::RaiseException($e);
            }
            $user = $result['user'];
            $user->setValue('password', $values['password']);
            $result['user'] = $user;
        }

        return $result;
    }

    public static function GetUser($values) {
        $result['status'] = 0;
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'id');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();
            $user = $pm->getObjectById('HealthCare\User\User', $values['id']);
            $user->setValue('password', '');
            $result['user'] = $user;
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }

        return $result;
    }

    public static function UpdatePassword($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'password');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();
            $user = $pm->getObjectById('HealthCare\User\User', $values['id']);

            $user->setValue('confirm_username', $user->getValue('username'));
            $user->setValue('password', md5($user->getValue('id') . $values['password'] . strtolower($user->getValue('username'))));

            $user->CheckValidationRules($pm);

            $result['user'] = $user;

            if (count($user->ValidationErrors) == 0) {
                $pm->beginTransaction();
                $pm->saveWithAudit($user, $_SESSION['IPrincipal']['id']);
                $pm->commit();
                $result['status'] = 1;
                $user->setValue('password', $values['password']);
                $result['user'] = $user;
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
                $user->setValue('password', $values['password']);
                $result['user'] = $user;
            }
        } catch (Exception $e) {
            if (strpos($e, '1062 Duplicate entry') != false) {
                $result['error-msg'] = "User already exists";
            } else {
                ExceptionManager::RaiseException($e);
            }
            $user = $result['user'];
            $user->setValue('password', $values['password']);
            $result['user'] = $user;
        }

        return $result;
    }

    public static function UpdateEnabled($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'enabled');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();
            $user = $pm->getObjectById('HealthCare\User\User', $values['id']);

            $user->setValue('confirm_username', $user->getValue('username'));
            $user->setValue('enabled', $values['enabled']);

            $user->CheckValidationRules($pm);

            $result['user'] = $user;

            if (count($user->ValidationErrors) == 0) {
                $pm->beginTransaction();
                $pm->saveWithAudit($user, $_SESSION['IPrincipal']['id']);
                if (!$user->getValue('enabled')) {
                    if (UserService::IsLastFullAdministrator($pm)) {
                        $result['error-msg'] = 'An active User must exist with Full Administrative Permission';
                        $pm->rollBack();
                    } else {
                        $result['status'] = 1;
                        $pm->commit();
                    }
                } else {
                    $result['status'] = 1;
                    $pm->commit();
                }
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }

        return $result;
    }
    
    public static function ChangePassword($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'password');
            $assertProperties->addProperty($values, 'password2');
            $assertProperties->assert();
            
            if(strcmp($values['password'], $values['password2']) != 0){
                throw new Exception('1013');
            }

            $pm = PersistenceManager::getConnection();
            $user = $pm->getObjectById('HealthCare\User\User', $_SESSION['IPrincipal']['id']);

            $user->setValue('confirm_username', $user->getValue('username'));
            $user->setValue('password', $values['password']);

            $user->CheckValidationRules($pm);

            $result['user'] = $user;

            if (count($user->ValidationErrors) == 0) {
                $pm->beginTransaction();
                $user->setValue('password', md5($user->getValue('id') . $values['password'] . strtolower($user->getValue('username'))));
                $pm->saveWithAudit($user, $_SESSION['IPrincipal']['id']);
                $pm->commit();
                $result['status'] = 1;
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            if (strpos($e, '1013') != false) {
                $result['error-msg'] = "New Password and Re-Password are not the same";
            } else {
                ExceptionManager::RaiseException($e);
            }
        }

        return $result;
    }

    public static function IsLastFullAdministrator($pm) {
        $query = $pm->getQueryBuilder('HealthCare\User\User');
        $sql = "select count(*) from " . User::GetDSN() . "_active as u inner join " . UserRole::GetDSN() . "_active as ur on u.id = ur.user_id inner join " . RolePermission::GetDSN() . "_active as rp on ur.role_id = rp.role_id inner join " . Permission::GetDSN() . " as p on rp.permission_id = p.id where p.name = ? and u.enabled = ?";
        $row = $query->execute($sql, array('All', 1))->fetch();
        if ($row[0] == 0) {
            return true;
        }
        return false;
    }

    public static function DeleteUser($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'id');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $user = $pm->getObjectById('HealthCare\User\User', $values['id']);
            if ($user != null) {
                $pm->beginTransaction();
                $user->setValue('deleted', 1);
                $user->setValue('delete_timestamp', date('Y-m-d H:i:s'));
                $pm->saveWithAudit($user, $_SESSION['IPrincipal']['id']);
                if (UserService::IsLastFullAdministrator($pm)) {
                    $result['error-msg'] = 'An active User must exist with Full Administrative Permission';
                    $pm->rollBack();
                } else {
                    $result['status'] = 1;
                    $pm->commit();
                }
            } else {
                $result['error-msg'] = 'User not found';
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }
        return $result;
    }

    public static function IsAuthenticated() {
        try {
            if (isset($_SESSION['IPrincipal'])) {
                return true;
            }
            return false;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }
    }

    public static function Logout() {
        try {
            if (isset($_SESSION['IPrincipal'])) {
                unset($_SESSION['IPrincipal']);
                unset($_SESSION['UserPermissions']);
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }
    }

    public static function Redirect($redirect) {
        $str = '';
        $query_string = filter_input(INPUT_SERVER, 'QUERY_STRING', FILTER_SANITIZE_STRING);
        if (!empty($query_string)) {
            $str .= '?' . $query_string;
        }
        $pos = strpos($str, 'return=');
        if ($pos) {
            header('Location: ' . substr($str, $pos + 7));
            exit;
        } else {
            header('Location: ' . $redirect);
            exit;
        }
    }

    public static function RequireLogin($url) {
        if (!self::IsAuthenticated()) {
            self::RedirectBack($url);
        }
    }

    private static function RedirectBack($url) {
        $redirect = 'http';
        $query_string = filter_input(INPUT_SERVER, 'QUERY_STRING', FILTER_SANITIZE_STRING);
        $https = filter_input(INPUT_SERVER, 'HTTPS', FILTER_SANITIZE_STRING);
        $http_host = filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING);
        if (isset($https) && $https == 'on') {
            $redirect . 's';
        }
        $redirect .= '://' . $http_host . REQUEST_URI;
        if (!empty($query_string)) {
            $redirect .= '?' . $query_string;
        }
        header('Location: ' . $url . '?return=' . $redirect);
        exit;
    }

    public static function Login($userName, $password, $redirect) {
        try {
            if (self::IsAuthenticated()) {
                self::Logout();
            }
            $pm = PersistenceManager::getConnection();
            $query = $pm->getQueryBuilder('HealthCare\User\User');
            $sql = 'select id,username,password,enabled from ' . User::GetDSN() . '_active where username = ?';
            $values = array($userName);
            $logins = $query->executeQuery($sql, $values, 0, 1);
            if ($logins->count() > 0 && strcmp($logins[0]->getValue('password'), md5($logins[0]->getValue('id') . $password . strtolower($logins[0]->getValue('username')))) == 0) {
                $_SESSION['IPrincipal'] = array('id' => $logins[0]->getValue('id'), 'username' => $logins[0]->getValue('username'));
                $_SESSION['UserPermissions'] = array();
                $sql = 'select p.name from ' . Permission::GetDSN() . ' as p inner join ' . RolePermission::GetDSN() . ' as rp on p.id = rp.permission_id inner join ' . UserRole::GetDSN() . ' as ur on rp.role_id = ur.role_id where ur.user_id = ?';
                $values = array($logins[0]->getValue('id'));
                $user_permissions = $query->executeQuery($sql, $values, 0, -1);
                for ($i = 0; $i < $user_permissions->count(); $i++) {
                    $_SESSION['UserPermissions'][] = array('permission' => $user_permissions[$i]->getValue('name'));
                }
                self::Redirect($redirect);
                return true;
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }
    }

    public static function HasPermission($permission, $scope_id) {
        if (isset($_SESSION['UserPermissions'])) {
            $user_permissions = $_SESSION['UserPermissions'];
            for ($i = 0; $i < count($user_permissions); $i++) {
                $user_permission = $user_permissions[$i];
                if (in_array($user_permission['permission'], explode('|', $permission)) || strcasecmp($user_permission['permission'], "all") == 0) {
                    if (isset($scope_id)) {
                        if (strlen($user_permission['scope_id']) == 0 || strcmp($scope_id, $user_permission['scope_id']) == 0) {
                            return true;
                        }
                    } else {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static function AssertPermission($permission, $scope_id, $redirect) {
        if (!self::HasPermission($permission, $scope_id)) {
            header("location: " . $redirect);
            exit;
        }
    }

    public static function GetIPrincipal() {
        if (isset($_SESSION['IPrincipal'])) {
            return $_SESSION['IPrincipal'];
        }
        return false;
    }

}
