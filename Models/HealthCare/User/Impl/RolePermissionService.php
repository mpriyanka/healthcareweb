<?php

namespace HealthCare\User\Impl;

use HealthCare\Dao\Exception\ExceptionManager;
use HealthCare\Dao\PersistenceManager;
use HealthCare\User\Permission;
use HealthCare\User\Role;
use HealthCare\User\RolePermission;
use HealthCare\Util\AssertProperties;
use HealthCare\Util\DataPagination;
use HealthCare\Util\DataTable;
use Exception;

class RolePermissionService {

    public static function GetRolePermissions() {
        $result['status'] = 0;
        try {
            $columns['role'] = array("sql" => "r.name", 'display_name' => 'Role', 'data_type' => 'String');
            $columns['permission'] = array("sql" => "p.name", 'display_name' => 'Permission', 'data_type' => 'String');
            $columns['creation_date'] = array("sql" => "creation_date", 'display_name' => 'Creation Date', 'data_type' => 'Date');
            $columns['last_changed'] = array("sql" => "last_changed", 'display_name' => 'Last Changed', 'data_type' => 'Date');

            $query_prpoperties_result = DataTable::GetQueryProperties($columns, "role", "asc");

            $pm = PersistenceManager::NewPersistenceManager();
            $query = $pm->getQueryBuilder('HealthCare\User\RolePermission');
            $sql = "select rp.id,r.name as role,p.name as permission,rp.creation_date,rp.last_changed from " . RolePermission::GetDSN() . "_active as rp inner join " . Role::GetDSN() . " as r on rp.role_id = r.id inner join " . Permission::GetDSN() . " as p on rp.permission_id = p.id " . $query_prpoperties_result['query'] . " order by " . $query_prpoperties_result['order_by'] . " " . $query_prpoperties_result['order_dir'];
            $row = $query->execute(str_replace("select", "select count(*) as _row_count,", $sql), $query_prpoperties_result['values'])->fetch();
            $total = $row['_row_count'];
            $pagination = new DataPagination();
            $pagination->setIndex($query_prpoperties_result['page_index']);
            $pagination->setSize(DATA_PAGE_SIZE);
            $pagination->setTotal($total);
            $pagination->setPages();
            $result['role-permissions'] = $query->executeQuery($sql, $query_prpoperties_result['values'], (($query_prpoperties_result['page_index'] - 1) * DATA_PAGE_SIZE), DATA_PAGE_SIZE);
            $pagination->setPageCount($result['role-permissions']->count());
            $result['pagination'] = $pagination;
            $result['columns'] = $columns;
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
            $result['error-msg'] = $e;
        }
        return $result;
    }

    public static function GetCreateRolePermissionTemplate() {
        $result['status'] = 0;
        $result['role-permission'] = new RolePermission();
        $result['status'] = 1;
        return $result;
    }

    public static function CreateRolePermission($values) {
        $pm;
        $result['role-permission'] = new RolePermission();
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'role_id');
            $assertProperties->addProperty($values, 'permission_id');

            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $role_permission = $result['role-permission'];

            $role_permission->setValue('role_id', $values['role_id']);
            $role_permission->setValue('permission_id', $values['permission_id']);

            $role_permission->CheckValidationRules($pm);

            $result['role-permission'] = $role_permission;

            if (count($role_permission->ValidationErrors) == 0) {
                $pm->beginTransaction();
                $pm->saveWithAudit($role_permission, $_SESSION['IPrincipal']['id']);
                $pm->commit();
                $result['status'] = 1;
                $result['role-permission'] = new RolePermission();
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            if (strpos($e, '1062 Duplicate entry') != false) {
                $result['error-msg'] = "Role Permission already exists";
            } else {
                ExceptionManager::RaiseException($e);
            }
        }

        return $result;
    }

    public static function GetRolePermission($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'id');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();
            $result['role-permission'] = $pm->getObjectById('HealthCare\User\RolePermission', $values['id']);
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }

        return $result;
    }

    public static function UpdateRolePermission($values) {
        $result['status'] = 0;
        $result['error-msg'] = "";
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'id');
            $assertProperties->addProperty($values, 'role_id');
            $assertProperties->addProperty($values, 'permission_id');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();
            $role_permission = $pm->getObjectById('HealthCare\User\RolePermission', $values['id']);

            $role_permission->setValue('role_id', $values['role_id']);
            $role_permission->setValue('permission_id', $values['permission_id']);

            $role_permission->CheckValidationRules($pm);

            $result['role-permission'] = $role_permission;

            if (count($role_permission->ValidationErrors) == 0) {
                $pm->beginTransaction();
                $pm->saveWithAudit($role_permission, $_SESSION['IPrincipal']['id']);
                if (UserService::IsLastFullAdministrator($pm)) {
                    throw new Exception("1100");
                }
                $result['status'] = 1;
                $pm->commit();
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            if (strpos($e, '1100') != false) {
                $result['error-msg'] = 'An active User must exist with Full Administrative Permission';
            } else if (strpos($e, '1062 Duplicate entry') != false) {
                $result['error-msg'] = "Role Permission already exists";
            } else {
                ExceptionManager::RaiseException($e);
            }
        }

        return $result;
    }

    public static function DeleteRolePermission($values) {
        $result['status'] = 0;
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'id');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $role_permission = $pm->getObjectById('HealthCare\User\RolePermission', $values['id']);
            if ($role_permission != null) {
                $pm->beginTransaction();
                $role_permission->setValue('deleted', 1);
                $role_permission->setValue('delete_timestamp', date('Y-m-d H:i:s'));
                $pm->saveWithAudit($role_permission, $_SESSION['IPrincipal']['id']);
                if (UserService::IsLastFullAdministrator($pm)) {
                    $result['error-msg'] = 'An active User must exist with Full Administrative Permission';
                    $pm->rollBack();
                } else {
                    $result['status'] = 1;
                    $pm->commit();
                }
            } else {
                $result['error-msg'] = 'Role Permission not found';
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }
        return $result;
    }

}
