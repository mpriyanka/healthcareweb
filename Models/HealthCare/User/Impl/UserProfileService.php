<?php

namespace HealthCare\User\Impl;

use HealthCare\Audit\Audit;
use HealthCare\Audit\AuditLine;
use HealthCare\Dao\Exception\ExceptionManager;
use HealthCare\Dao\PersistenceManager;
use HealthCare\User\User;
use HealthCare\User\UserProfile;
use HealthCare\Util\Address;
use HealthCare\Util\AssertProperties;
use HealthCare\Util\DataPagination;
use HealthCare\Util\DataTable;
use Exception;

class UserProfileService {

    public static function GetUserProfiles() {
        $result['status'] = 0;
        try {
            $columns['username'] = array("sql" => "u.username", 'display_name' => 'Username', 'data_type' => 'String');
            $columns['name'] = array("sql" => "concat(up.firstname,' ',up.lastname)", 'display_name' => 'Name', 'data_type' => 'String');
            $columns['phone'] = array("sql" => "concat(up.phone1,' ',up.phone2)", 'display_name' => 'Phone', 'data_type' => 'String');
            $columns['email'] = array("sql" => "up.email", 'display_name' => 'Email', 'data_type' => 'String');
            $columns['address'] = array("sql" => "concat(a.line_1,' ',a.line_2)", 'display_name' => 'Address', 'data_type' => 'String');
            $columns['city'] = array("sql" => "a.city", 'display_name' => 'City', 'data_type' => 'String');
            $columns['state'] = array("sql" => "a.state", 'display_name' => 'State', 'data_type' => 'String');
            $columns['country'] = array("sql" => "a.country", 'display_name' => 'Country', 'data_type' => 'String');
            $columns['creation_date'] = array("sql" => "up.creation_date", 'display_name' => 'Creation Date', 'data_type' => 'Date');
            $columns['last_changed'] = array("sql" => "up.last_changed", 'display_name' => 'Last Changed', 'data_type' => 'Date');
            
            $query_prpoperties_result = DataTable::GetQueryProperties($columns, "name", "asc");
            
            $pm = PersistenceManager::NewPersistenceManager();
            $query = $pm->getQueryBuilder('HealthCare\User\UserProfile');
            $sql = "select up.id,u.username,concat(up.firstname,' ',up.lastname) as name,up.email,up.phone1,up.phone2,a.line_1,a.line_2,a.city,a.state,a.country,up.creation_date,up.last_changed from " . UserProfile::GetDSN() . "_active as up inner join " . User::GetDSN() . " as u on up.user_id = u.id inner join " . Address::GetDSN() . " as a on up.address_id = a.id " . $query_prpoperties_result['query'] . " order by " . $query_prpoperties_result['order_by'] . " " . $query_prpoperties_result['order_dir'];
            $row = $query->execute(str_replace("select", "select count(*) as _row_count,", $sql), $query_prpoperties_result['values'])->fetch();
            $total = $row['_row_count'];
            $pagination = new DataPagination();
            $pagination->setIndex($query_prpoperties_result['page_index']);
            $pagination->setSize(DATA_PAGE_SIZE);
            $pagination->setTotal($total);
            $pagination->setPages();
            $result['user-profiles'] = $query->executeQuery($sql, $query_prpoperties_result['values'], (($query_prpoperties_result['page_index'] - 1) * DATA_PAGE_SIZE), DATA_PAGE_SIZE);
            $pagination->setPageCount($result['user-profiles']->count());
            $result['pagination'] = $pagination;
            $result['columns'] = $columns;
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
            $result['error-msg'] = $e;
        }
        return $result;
    }

    public static function GetCreateUserProfileTemplate() {
        $result['status'] = 0;
        $result['user-profile'] = new UserProfile();
        $result['address'] = new Address();
        $result['status'] = 1;
        return $result;
    }

    public static function CreateUserProfile($values) {
        $result['user-profile'] = new UserProfile();
        $result['address'] = new Address();
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'user_id');
            $assertProperties->addProperty($values, 'firstname');
            $assertProperties->addProperty($values, 'lastname');
            $assertProperties->addProperty($values, 'phone1');
            $assertProperties->addProperty($values, 'phone2');
            $assertProperties->addProperty($values, 'email');
            $assertProperties->addProperty($values, 'line_1');
            $assertProperties->addProperty($values, 'line_2');
            $assertProperties->addProperty($values, 'city');
            $assertProperties->addProperty($values, 'state');
            $assertProperties->addProperty($values, 'country');

            $assertProperties->assert();

            $user_profile = $result['user-profile'];
            $address = $result['address'];

            $user_profile->setValue('user_id', $values['user_id']);
            $user_profile->setValue('address_id', $address->getValue('id'));
            $user_profile->setValue('firstname', $values['firstname']);
            $user_profile->setValue('lastname', $values['lastname']);
            $user_profile->setValue('phone1', $values['phone1']);
            $user_profile->setValue('phone2', $values['phone2']);
            $user_profile->setValue('email', $values['email']);

            $address->setValue('line_1', $values['line_1']);
            $address->setValue('line_2', $values['line_2']);
            $address->setValue('city', $values['city']);
            $address->setValue('state', $values['state']);
            $address->setValue('country', $values['country']);

            $user_profile->CheckValidationRules(null);
            $address->CheckValidationRules(null);

            $result['user-profile'] = $user_profile;
            $result['address'] = $address;

            if (count($user_profile->ValidationErrors) == 0 && count($address->ValidationErrors) == 0) {
                $pm = PersistenceManager::getConnection();
                $pm->beginTransaction();
                $pm->saveWithAudit($address, $_SESSION['IPrincipal']['id']);
                $pm->saveWithAudit($user_profile, $_SESSION['IPrincipal']['id']);
                $pm->commit();
                $result['status'] = 1;
                $result['user-profile'] = new UserProfile();
                $result['address'] = new Address();
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            if (strpos($e, '1062 Duplicate entry') != false) {
                $result['error-msg'] = "User Profile already exists";
            } else {
                ExceptionManager::RaiseException($e);
            }
        }

        return $result;
    }

    public static function GetUserProfile($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'id');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();
            $result['user-profile'] = $pm->getObjectById('HealthCare\User\UserProfile', $values['id']);
            $result['address'] = $pm->getObjectById('HealthCare\Util\Address', $result['user-profile']->getValue('address_id'));
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }

        return $result;
    }

    public static function UpdateUserProfile($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'id');
            $assertProperties->addProperty($values, 'firstname');
            $assertProperties->addProperty($values, 'lastname');
            $assertProperties->addProperty($values, 'phone1');
            $assertProperties->addProperty($values, 'phone2');
            $assertProperties->addProperty($values, 'email');
            $assertProperties->addProperty($values, 'line_1');
            $assertProperties->addProperty($values, 'line_2');
            $assertProperties->addProperty($values, 'city');
            $assertProperties->addProperty($values, 'state');
            $assertProperties->addProperty($values, 'country');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();
            $user_profile = $pm->getObjectById('HealthCare\User\UserProfile', $values['id']);
            $address = $pm->getObjectById('HealthCare\Util\Address', $user_profile->getValue('address_id'));

            $user_profile->setValue('firstname', $values['firstname']);
            $user_profile->setValue('lastname', $values['lastname']);
            $user_profile->setValue('phone1', $values['phone1']);
            $user_profile->setValue('phone2', $values['phone2']);
            $user_profile->setValue('email', $values['email']);

            $address->setValue('line_1', $values['line_1']);
            $address->setValue('line_2', $values['line_2']);
            $address->setValue('city', $values['city']);
            $address->setValue('state', $values['state']);
            $address->setValue('country', $values['country']);

            $user_profile->CheckValidationRules($pm);
            $address->CheckValidationRules($pm);

            $result['user-profile'] = $user_profile;
            $result['address'] = $address;


            if (count($user_profile->ValidationErrors) == 0 && count($address->ValidationErrors) == 0) {
                $pm = PersistenceManager::getConnection();
                $pm->beginTransaction();
                $pm->saveWithAudit($address, $_SESSION['IPrincipal']['id']);
                $pm->saveWithAudit($user_profile, $_SESSION['IPrincipal']['id']);
                $pm->commit();
                $result['status'] = 1;
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }

        return $result;
    }

    public static function DeleteUserProfile($values) {
        $result['status'] = 0;
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'id');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $user_profile = $pm->getObjectById('HealthCare\User\UserProfile', $values['id']);
            if ($user_profile != null) {
                $pm->beginTransaction();
                $user_profile->setValue('deleted', 1);
                $user_profile->setValue('delete_timestamp', date('Y-m-d H:i:s'));
                $pm->saveWithAudit($user_profile, $_SESSION['IPrincipal']['id']);
                $pm->commit();
                $result['status'] = 1;
            } else {
                $result['error-msg'] = 'User Profile not found';
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }
        return $result;
    }

}
