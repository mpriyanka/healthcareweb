<?php

namespace HealthCare\User\Impl;

use HealthCare\Dao\Exception\ExceptionManager;
use HealthCare\Dao\PersistenceManager;
use HealthCare\User\Permission;
use HealthCare\Util\AssertProperties;
use HealthCare\Util\DataPagination;
use HealthCare\Util\DataTable;
use Exception;

class PermissionService {

    public static function GetPermissions() {
        $result['status'] = 0;
        try {
            $columns['name'] = array("sql" => "name", 'display_name' => 'Name', 'data_type' => 'String');
            $columns['creation_date'] = array("sql" => "creation_date", 'display_name' => 'Creation Date', 'data_type' => 'Date');
            $columns['last_changed'] = array("sql" => "last_changed", 'display_name' => 'Last Changed', 'data_type' => 'Date');
            
            $query_prpoperties_result = DataTable::GetQueryProperties($columns, "name", "asc");
            
            $pm = PersistenceManager::NewPersistenceManager();
            $query = $pm->getQueryBuilder('HealthCare\User\Permission');
            $sql = "select * from " . Permission::GetDSN() . " " . $query_prpoperties_result['query'] . " order by " . $query_prpoperties_result['order_by'] . " " . $query_prpoperties_result['order_dir'];
            $row = $query->execute(str_replace("select *", "select count(*) as _row_count", $sql), $query_prpoperties_result['values'])->fetch();
            $total = $row['_row_count'];
            $pagination = new DataPagination();
            $pagination->setIndex($query_prpoperties_result['page_index']);
            $pagination->setSize(DATA_PAGE_SIZE);
            $pagination->setTotal($total);
            $pagination->setPages();
            $result['permissions'] = $query->executeQuery($sql, $query_prpoperties_result['values'], (($query_prpoperties_result['page_index'] - 1) * DATA_PAGE_SIZE), DATA_PAGE_SIZE);
            $pagination->setPageCount($result['permissions']->count());
            $result['pagination'] = $pagination;
            $result['columns'] = $columns;
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
            $result['error-msg'] = $e;
        }
        return $result;
    }
    
    public static function GetPermissionNames() {
        $result['status'] = 0;
        try {
            $pm = PersistenceManager::NewPersistenceManager();
            $query = $pm->getQueryBuilder('HealthCare\User\Permission');
            $sql = "select id,name from " . Permission::GetDSN() . " order by name";
            $result['permission-names'] = $query->executeQuery($sql, array(), 0, -1);
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
            $result['error-msg'] = $e;
        }
        return $result;
    }

    public static function GetCreatePermissionTemplate() {
        $result['status'] = 0;
        $result['permission'] = new Permission();
        $result['status'] = 1;
        return $result;
    }

    public static function CreatePermission($values) {
        $pm = null;
        $result['permission'] = new Permission();
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'name');

            $assertProperties->assert();

            $permission = $result['permission'];

            $permission->setValue('name', $values['name']);

            $permission->CheckValidationRules($pm);

            $result['permission'] = $permission;

            if (count($permission->ValidationErrors) == 0) {
                $pm = PersistenceManager::getConnection();
                $pm->save($permission);
                $result['status'] = 1;
                $result['permission'] = new Permission();
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            if (strpos($e, '1062 Duplicate entry') != false) {
                $result['error-msg'] = "Permission already exists";
            } else {
                ExceptionManager::RaiseException($e);
            }
        }

        return $result;
    }

    public static function GetPermission($values) {
        $result['status'] = 0;
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'id');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();
            $result['permission'] = $pm->getObjectById('HealthCare\User\Permission', $values['id']);
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }

        return $result;
    }

    public static function UpdatePermission($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'name');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();
            $permission = $pm->getObjectById('HealthCare\User\Permission', $values['id']);

            $permission->setValue('name', $values['name']);

            $permission->CheckValidationRules($pm);

            $result['permission'] = $permission;

            if (count($permission->ValidationErrors) == 0) {
                $pm->save($permission);
                $result['status'] = 1;
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            if (strpos($e, '1062 Duplicate entry') != false) {
                $result['error-msg'] = "Permission already exists";
            } else {
                ExceptionManager::RaiseException($e);
            }
        }

        return $result;
    }

    public static function DeletePermission($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'id');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $permission = $pm->getObjectById('HealthCare\User\Permission', $values['id']);
            if ($permission != null) {
                $pm->deleteObjectById('HealthCare\User\Permission', $values['id']);
                $result['status'] = 1;
            } else {
                $result['error-msg'] = 'Permission not found';
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }
        return $result;
    }

}
