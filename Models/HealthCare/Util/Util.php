<?php

namespace HealthCare\Util;

use Exception;
use HealthCare\Dao\Exception\ExceptionManager;
use HealthCare\Dao\PersistenceManager;

class Util {

    public static function AddLeadingZeros($value, $len) {
        $pad = '';
        for ($i = strlen($value); $i < $len; $i++) {
            $pad .= '0';
        }
        return $pad . $value;
    }

    public static function VerifyRequestHashToken($values) {
        $hash_token = $values['hash_token'];
        $hash_str = "";
        foreach ($values as $key => $value) {
            if (strcmp($key, "hash_token") != 0) {
                $hash_str .= $key . $value;
            }
        }
        if (strcmp($hash_token, md5($hash_str . 'hCarePPM')) == 0) {
            return true;
        }
        return false;
    }

    public static function UpdateDoctorGCMTokenWS($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'username');
            $assertProperties->addProperty($values, 'gcm_token');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $doctor = $pm->getObjectByColumn('HealthCare\Site\Doctor', 'username', $values['username']);
            if ($doctor == null) {
                throw new Exception("404");
            }

            $doctor->setValue('gcm_token', $values['gcm_token']);

            $pm->beginTransaction();
            $pm->saveWithAudit($doctor, '');
            $pm->commit();
            $result['status'] = 1;
        } catch (Exception $e) {
            if (strpos($e, '404') != false) {
                $result['error-msg'] = "Doctor not found";
            } else {
                ExceptionManager::RaiseException($e);
            }
        }
        return $result;
    }

    public static function UpdateUserGCMTokenWS($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'phone');
            $assertProperties->addProperty($values, 'gcm_token');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $app_user = $pm->getObjectByColumn('HealthCare\App\AppUser', 'phone', $values['phone']);
            if ($app_user == null) {
                throw new Exception("404");
            }

            $app_user->setValue('gcm_token', $values['gcm_token']);

            $pm->beginTransaction();
            $pm->saveWithAudit($app_user, '');
            $pm->commit();
            $result['status'] = 1;
        } catch (Exception $e) {
            if (strpos($e, '404') != false) {
                $result['error-msg'] = "App User not found";
            } else {
                ExceptionManager::RaiseException($e);
            }
        }
        return $result;
    }

    public static function SendGCM($values) {
        $result['success'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'message');
            $assertProperties->addProperty($values, 'gcm_tokens');
            $assertProperties->assert();

            $fields = array(
                'registration_ids' => $values['gcm_tokens'],
                'data' => array("message" => json_encode($values['message'])),
            );
            $headers = array(
                'Authorization: key=' . API_KEY,
                'Content-Type: application/json'
            );

            // Open connection
            $ch = curl_init();

            // Set the URL, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, GCM_URL);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields));

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            // curl_setopt($ch, CURLOPT_POST, true);
            // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            // Execute post
            $result_gcm = curl_exec($ch);

            // Close connection
            curl_close($ch);
            
            $json_result = json_decode($result_gcm, true);

            $result['success'] = $json_result['success'];
        } catch (Exception $e) {
            
        }
        return $result;
    }

}
