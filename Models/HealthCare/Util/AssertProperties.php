<?php

namespace HealthCare\Util;

use Exception;

class AssertProperties {

    private $error = "";

    public function addProperty($type, $property) {
        if (!isset($type[$property])) {
            $this->error .= 'Undefined Input: ' . $property . '<br/>';
        }
    }

    public function assert() {
        if (strlen($this->error) > 0) {
            throw new Exception($this->error);
        }
    }

}

