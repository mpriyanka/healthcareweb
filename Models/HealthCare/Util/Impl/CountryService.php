<?php

namespace HealthCare\Util\Impl;

use Exception;
use HealthCare\Dao\Exception\ExceptionManager;
use HealthCare\Dao\PersistenceManager;
use HealthCare\Util\Country;

class CountryService {

    public static function GetCountries() {
        $result['status'] = 0;
        try {
            $pm = PersistenceManager::NewPersistenceManager();
            $query = $pm->getQueryBuilder('HealthCare\Util\Country');
            $sql = "select * from " . Country::GetDSN() . " order by name";
            $result['countries'] = $query->executeQuery($sql, array(), 0, 500);
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
            $result['error-msg'] = $e;
        }
        return $result;
    }
    
    public static function GetCountryNames() {
        $result['status'] = 0;
        try {
            $pm = PersistenceManager::NewPersistenceManager();
            $query = $pm->getQueryBuilder('HealthCare\Util\Country');
            $sql = "select name,phone_code from " . Country::GetDSN() . " order by name";
            $result['country-names'] = $query->executeQuery($sql, array(), 0, -1);
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
            $result['error-msg'] = $e;
        }
        return $result;
    }
}
