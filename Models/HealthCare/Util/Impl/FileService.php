<?php

namespace HealthCare\Util\Impl;

use HealthCare\Dao\Exception\ExceptionManager;
use Exception;

class FileService {

    public static function GetSize($path) {
        if (file_exists($path)) {
            $info = stat($path);
            return $info['size'];
        }
        return 0;
    }

    public static function Remove($path) {
        if (file_exists($path)) {
            unlink($path);
        }
    }

    public static function GetMimeInfo($path) {
        if (file_exists($path)) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            return  finfo_file($finfo, $path);
        }
        return false;
    }

    public static function Upload($file, $path, $mimes) {
        try {
            if ($_FILES[$file]['error'] === UPLOAD_ERR_OK) {
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                $mime = finfo_file($finfo, $_FILES[$file]['tmp_name']);
                if (in_array($mime, $mimes) && move_uploaded_file($_FILES[$file]['tmp_name'], $path)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }
    }

}
