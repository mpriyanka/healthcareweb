<?php

namespace HealthCare\Util\Impl;

use HealthCare\Site\Impl\PageService;
use Exception;
use PHPMailer;

class EmailNotificationService {

    public static function Sendmail($values) {

        try {
            $page_result = PageService::GetPageByURL($values);
            if ($page_result['status']) {

                $message = str_replace(array_keys($values['placeholder']), array_values($values['placeholder']), html_entity_decode(stripcslashes($page_result['page']->getValue('body'))));

                $mail = new PHPMailer;

                $mail->IsSMTP();

                $mail->SMTPAuth = true;                  // enable SMTP authentication

                $mail->Host = SMTP_HOST;

                $mail->SMTPSecure = "ssl"; // sets the SMTP server

                $mail->Port = SMTP_PORT;                    // set the SMTP port for the GMAIL server

                $mail->Username = SMTP_USERNAME; // SMTP account username

                $mail->Password = SMTP_PASSWORD;

                $mail->From = SMTP_USERNAME;
                $mail->FromName = SITE_NAME;  // Add a recipient
                $mail->AddAddress($values['email']);               // Name is optional

                $mail->IsHTML(true);                                  // Set email format to HTML

                $mail->Subject = $values['subject'];
                $mail->MsgHTML("<html><head></head><body>" . $message . "</body></html>");

                $mail->Send();
            }
        } catch (Exception $e) {
            
        }
    }

}
