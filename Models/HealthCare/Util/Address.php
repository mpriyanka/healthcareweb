<?php

namespace HealthCare\Util;

use HealthCare\Dao\IndexType;
use HealthCare\Dao\PersistableObject;
use HealthCare\Dao\Property;
use HealthCare\Dao\Validation\IllegalValueValidationRule;
use HealthCare\Dao\Validation\StringValidationRule;
use HealthCare\Dao\Validation\ValidationRules;

class Address extends PersistableObject {

    function  __construct() {
        //Persistable Data Members
        $this->properties['id'] = new Property('id', str_replace(".", "", uniqid(rand(), true)), true, IndexType::PrimaryKey);
        $this->properties['line_1'] = new Property('line_1', '', true, IndexType::Normal);
        $this->properties['line_2'] = new Property('line_2', '', true, IndexType::Normal);
        $this->properties['city'] = new Property('city', '', true, IndexType::Normal);
        $this->properties['state'] = new Property('state', '', true, IndexType::Normal);
        $this->properties['country'] = new Property('country', '', true, IndexType::Normal);
        $this->properties['creation_date'] = new Property('creation_date', date('Y-m-d H:i:s'), true, IndexType::Normal);

        //Non-Persistable Data Members
        $this->properties['last_changed'] = new Property('last_changed','',false, IndexType::Timestamp);

        //ValidationRules
        $this->ValidationRules = new ValidationRules();
        $this->ValidationRules->add(new StringValidationRule('line_1', 0, 100, 'line'));
        $this->ValidationRules->add(new StringValidationRule('line_2', 0, 100, 'line'));
        $this->ValidationRules->add(new StringValidationRule('city', 0, 25, 'city'));
        $this->ValidationRules->add(new StringValidationRule('state', 0, 30, 'state'));
        $this->ValidationRules->add(new IllegalValueValidationRule('country', '-', 'country'));
        
        //TrackedPropertyChanges
        $this->TrackedPropertyChanges[] = 'line_1';
        $this->TrackedPropertyChanges[] = 'line_2';
        $this->TrackedPropertyChanges[] = 'city';
        $this->TrackedPropertyChanges[] = 'state';
        $this->TrackedPropertyChanges[] = 'country';
    }

    public static function GetDSN() {
        return DB_NAME.'.addresses';
    }

}
