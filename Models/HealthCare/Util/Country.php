<?php

namespace HealthCare\Util;

use HealthCare\Dao\PersistableObject;
use HealthCare\Dao\Property;
use HealthCare\Dao\IndexType;

class Country extends PersistableObject {

    function __construct() {
        //Persistable Data Members
        $this->properties['id'] = new Property('id', '', true, IndexType::PrimaryKey);
        $this->properties['iso'] = new Property('iso', '', true, IndexType::Normal);
        $this->properties['name'] = new Property('name', '', true, IndexType::Normal);
        $this->properties['nice_name'] = new Property('nice_name', '', true, IndexType::Normal);
        $this->properties['iso3'] = new Property('iso3', '', true, IndexType::Normal);
        $this->properties['num_code'] = new Property('num_code', '', true, IndexType::Normal);
        $this->properties['phone_code'] = new Property('phone_code', '', true, IndexType::Normal);

        //ValidationRules
    }

    public static function GetDSN() {
        return DB_NAME . '.countries';
    }

}
