<?php

namespace HealthCare\Util;

class DataTable {

    private $request_uri;
    private $query_string;
    private $default_column;
    private $default_order_dir;
    private $columns;

    public function __construct() {
        $this->request_uri = REQUEST_URI;
        $this->query_string = QUERY_STRING;
        $this->default_column = "";
        $this->default_order_dir = "asc";
        $this->columns = array();
    }

    public function SetColumns($columns) {
        $this->columns = $columns;
    }

    public function SetDefaultOrderByColumn($column) {
        $this->default_column = $column;
    }
       
    public function getColumnHeader($column) {
        $query_string = $this->getQueryString($column);
        $request_query_string = null;
        parse_str($this->query_string, $request_query_string);
        ?><a class="<?php
        if (isset($request_query_string['order_by']) && strcmp($request_query_string['order_by'], $column) == 0 || (!isset($request_query_string['order_by']) && strcmp($this->default_column, $column) == 0)) {
            echo 'active';
        }
        ?>" href="<?php echo CONTEXT_PATH . $this->request_uri . '?' . http_build_query($query_string) ?>"><?php echo $this->columns[$column]['display_name'] ?></a><?php
       }

    private function getQueryString($column) {
           $query_string = null;
           parse_str($this->query_string, $query_string);
           if (isset($query_string['order_by']) && strcmp($query_string['order_by'], $column) == 0) {
               if (isset($query_string['order_dir'])) {
                   if (strcmp($query_string['order_dir'], 'asc') == 0) {
                       $query_string['order_dir'] = 'desc';
                   } else {
                       $query_string['order_dir'] = 'asc';
                   }
               } else {
                   $query_string['order_dir'] = 'asc';
               }
           } else {
               $query_string['order_by'] = $column;
               $query_string['order_dir'] = 'asc';
           }
           return $query_string;
       }

    public function SetFilter() {
           $query_string = null;
           parse_str($this->query_string, $query_string);
           ?>
        <form class="ats-ui-filter-form" action="<?php echo CONTEXT_PATH . $this->request_uri ?>" method="get">
            <label>Filter</label>
            <select id="filter_by" name="filter_by" class="filter_by">
                <option value="">Select Filter</option>
                <?php
                foreach ($this->columns as $column => $column_properties) {
                    ?><option value="<?php echo $column ?>" <?php if (isset($query_string['filter_by']) && strcmp($query_string['filter_by'], $column) == 0) { ?> selected <?php } ?>><?php echo $column_properties['display_name'] ?></option><?php
                }
                ?>
            </select>
            <span class="filters">
                <?php $this->setFilterValue($query_string) ?>
            </span>
            <button class="btn">Apply</button>
            <?php if (isset($query_string['order_by'])) { ?><input type="hidden" name="order_by" value="<?php echo urldecode($query_string['order_by']) ?>" /><?php } ?>
            <?php if (isset($query_string['order_dir'])) { ?><input type="hidden" name="order_dir" value="<?php echo urldecode($query_string['order_dir']) ?>" /><?php } ?>
        </form>
        <?php
    }

    private function setFilterValue($query_string) {
        if (isset($query_string['filter_by']) && isset($this->columns[$query_string['filter_by']])) {
            $column = $this->columns[$query_string['filter_by']];
            switch ($column['data_type']) {
                case 'Number':
                    ?><input id="filter_value" name="filter_value" value="<?php
                    if (isset($query_string['filter_value'])) {
                        echo urldecode($query_string['filter_value']);
                    }
                    ?>"/><?php
                           break;
                       case 'String':
                           ?><input id="filter_value" name="filter_value" value="<?php
                    if (isset($query_string['filter_value'])) {
                        echo urldecode($query_string['filter_value']);
                    }
                    ?>"/><?php
                           break;
                       case 'Dropdown':
                           ?>
                    <select id="filter_value" name="filter_value">
                        <?php
                        $dropdown_list = array_values($column['dropdown_list']);
                        for ($i = 0; $i < count($dropdown_list); $i++) {
                            ?><option <?php if (isset($query_string['filter_value']) && strcmp(urldecode($query_string['filter_value']), $dropdown_list[$i]) == 0) { ?> selected <?php } ?>><?php echo $dropdown_list[$i]; ?></option><?php
                        }
                        ?>
                    </select>
                    <?php
                    break;
                case 'Date':
                    ?>
                    <input style="width:95px;" id="filter_value1" name="filter_value1" value="<?php
                    if (isset($query_string['filter_value1'])) {
                        echo urldecode($query_string['filter_value1']);
                    }
                    ?>"/>
                    <input style="width:94px;" id="filter_value2" name="filter_value2" value="<?php
                    if (isset($query_string['filter_value2'])) {
                        echo urldecode($query_string['filter_value2']);
                    }
                    ?>"/>
                           <?php
                           break;
                   }
               } else {
                   ?><input id="filter_value" name="filter_value"/><?php
        }
    }

    public static function GetQueryProperties($columns, $default_order_by, $default_order_dir) {
        $query_string = null;

        parse_str(QUERY_STRING, $query_string);

        $query_properties_result = array("order_by" => $columns[$default_order_by]["sql"], "order_dir" => $default_order_dir, "page_index" => 1, "query" => "", "values" => array());

        if (isset($query_string['order_by']) && isset($columns[$query_string['order_by']])) {
            $query_properties_result['order_by'] = $columns[$query_string['order_by']]["sql"];
        }

        if (isset($query_string['order_dir']) && strcmp($query_string['order_dir'], "asc") == 0) {
            $query_properties_result['order_dir'] = "asc";
        }

        if (isset($query_string['order_dir']) && strcmp($query_string['order_dir'], "desc") == 0) {
            $query_properties_result['order_dir'] = "desc";
        }

        if (filter_input(INPUT_GET, 'page', FILTER_SANITIZE_NUMBER_INT) != null && filter_input(INPUT_GET, 'page', FILTER_SANITIZE_NUMBER_INT)) {
            $query_properties_result['page_index'] = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_NUMBER_INT);
        }

        if (isset($query_string['filter_by']) && isset($columns[$query_string['filter_by']])) {
            $column = $columns[$query_string['filter_by']];
            switch ($column['data_type']) {
                case 'Number':
                    if (filter_input(INPUT_GET, 'filter_value', FILTER_SANITIZE_NUMBER_FLOAT) != null && filter_input(INPUT_GET, 'filter_value', FILTER_SANITIZE_NUMBER_FLOAT)) {
                        $query_properties_result["values"][] = filter_input(INPUT_GET, 'filter_value', FILTER_SANITIZE_NUMBER_FLOAT);
                        $query_properties_result["query"] = "where " . $columns[$query_string['filter_by']]["sql"] . " = ?";
                    }
                    break;
                case 'String':
                    if (filter_input(INPUT_GET, 'filter_value', FILTER_SANITIZE_STRING) != null && filter_input(INPUT_GET, 'filter_value', FILTER_SANITIZE_STRING)) {
                        $query_properties_result["values"][] = "%" . filter_input(INPUT_GET, 'filter_value', FILTER_SANITIZE_STRING) . "%";
                        $query_properties_result["query"] = "where " . $columns[$query_string['filter_by']]["sql"] . " like ?";
                    }
                    break;
                case 'Dropdown':
                    if (filter_input(INPUT_GET, 'filter_value', FILTER_SANITIZE_STRING) != null && filter_input(INPUT_GET, 'filter_value', FILTER_SANITIZE_STRING)) {
                        $filter_value = array_search(filter_input(INPUT_GET, 'filter_value', FILTER_SANITIZE_STRING), $column['dropdown_list']);
                        if ($filter_value !== FALSE) {
                            $query_properties_result["values"][] = $filter_value;
                            $query_properties_result["query"] = "where " . $columns[$query_string['filter_by']]["sql"] . " = ?";
                        }
                    }
                    break;
                case 'Date':
                    if (filter_input(INPUT_GET, 'filter_value1', FILTER_SANITIZE_STRING) != null && filter_input(INPUT_GET, 'filter_value1', FILTER_SANITIZE_STRING) && filter_input(INPUT_GET, 'filter_value2', FILTER_SANITIZE_STRING) != null && filter_input(INPUT_GET, 'filter_value2', FILTER_SANITIZE_STRING)) {
                        $query_properties_result["values"][] = implode("-", array_reverse(explode("-", str_replace("/", "-", filter_input(INPUT_GET, 'filter_value1', FILTER_SANITIZE_STRING)))))." 00:00:00";
                        $query_properties_result["values"][] = implode("-", array_reverse(explode("-", str_replace("/", "-", filter_input(INPUT_GET, 'filter_value2', FILTER_SANITIZE_STRING)))))." 23:59:59";
                        $query_properties_result["query"] = "where " . $columns[$query_string['filter_by']]["sql"] . " between ? and ?";
                    }
                    break;
            }
        }
        return $query_properties_result;
    }

    public function getJavaScriptColumns(){
        $columns = array();
        foreach($this->columns as $key => $column){
            unset($column["sql"]);
            unset($column["display_name"]);
            if(isset($column['dropdown_list'])){
                $column['dropdown_list'] = array_values($column['dropdown_list']);
            }
            $columns[$key] = $column;
        }
        return $columns;
    }
}
