<?php
namespace HealthCare\Util;

use HealthCare\Dao\IndexType;
use HealthCare\Dao\PersistableObject;
use HealthCare\Dao\Property;
use HealthCare\Dao\Validation\EmailValidationRule;
use HealthCare\Dao\Validation\IllegalValueValidationRule;
use HealthCare\Dao\Validation\InternationalPhoneValidationRule;
use HealthCare\Dao\Validation\StringValidationRule;
use HealthCare\Dao\Validation\ValidationRules;

class Profile extends PersistableObject {

    function  __construct($email_not_compulsory = true) {
        //Persistable Data Members
        $this->properties['id'] = new Property('id', str_replace(".", "", uniqid(rand(), true)), true, IndexType::PrimaryKey);
        $this->properties['title'] = new Property('title', '', true, IndexType::Normal);
        $this->properties['firstname'] = new Property('firstname', '', true, IndexType::Normal);
        $this->properties['middlename'] = new Property('middlename', '', true, IndexType::Normal);
        $this->properties['lastname'] = new Property('lastname', '', true, IndexType::Normal);
        $this->properties['phone'] = new Property('phone', '', true, IndexType::Normal);
        $this->properties['mobile'] = new Property('mobile', '', true, IndexType::Normal);
        $this->properties['email'] = new Property('email', '', true, IndexType::Normal);
        $this->properties['affiliations'] = new Property('affiliations', '', true, IndexType::Normal);
        $this->properties['image_path'] = new Property('image_path', '', true, IndexType::Nullable);
        $this->properties['cv_path'] = new Property('cv_path', '', true, IndexType::Nullable);
        $this->properties['bio'] = new Property('bio', '', true, IndexType::Nullable);
        $this->properties['research_interest'] = new Property('research_interest', '', true, IndexType::Nullable);
        $this->properties['address_id'] = new Property('address_id', '', true, IndexType::Normal);
        $this->properties['creation_date'] = new Property('creation_date', date('Y-m-d H:i:s'), true, IndexType::Normal);

        //Non-Persistable Data Members
        $this->properties['last_changed'] = new Property('last_changed','',false, IndexType::Timestamp);

        //ValidationRules
        $this->ValidationRules = new ValidationRules();
        $this->ValidationRules->add(new IllegalValueValidationRule('title', '-', 'title'));
        $this->ValidationRules->add(new StringValidationRule('firstname', 2, 25, 'name'));
        $this->ValidationRules->add(new StringValidationRule('middlename', 0, 25, 'middlename'));
        $this->ValidationRules->add(new StringValidationRule('lastname', 2, 25, 'name'));
        $this->ValidationRules->add(new InternationalPhoneValidationRule('phone', true, 'phone'));
        $this->ValidationRules->add(new InternationalPhoneValidationRule('mobile', true, 'phone'));
        $this->ValidationRules->add(new EmailValidationRule('email', $email_not_compulsory, 'email'));
        $this->ValidationRules->add(new IllegalValueValidationRule('address_id', '-','address_id'));
        
        //TrackedPropertyChanges
        $this->TrackedPropertyChanges[] = 'title';
        $this->TrackedPropertyChanges[] = 'firstname';
        $this->TrackedPropertyChanges[] = 'middlename';
        $this->TrackedPropertyChanges[] = 'lastname';
        $this->TrackedPropertyChanges[] = 'phone';
        $this->TrackedPropertyChanges[] = 'mobile';
        $this->TrackedPropertyChanges[] = 'email';
        $this->TrackedPropertyChanges[] = 'affiliations';
        $this->TrackedPropertyChanges[] = 'bio';
        $this->TrackedPropertyChanges[] = 'research_interest';
        $this->TrackedPropertyChanges[] = 'address_id';
    }

    public static function GetDSN() {
        return DB_NAME.'.profiles';
    }

}
