<?php
namespace HealthCare\Audit;

use HealthCare\Dao\IndexType;
use HealthCare\Dao\PersistableObject;
use HealthCare\Dao\Property;
use HealthCare\Dao\Validation\StringValidationRule;
use HealthCare\Dao\Validation\ValidationRules;

class Audit extends PersistableObject {

    function  __construct() {
        //Persistable Data Members
        $this->properties['id'] = new Property('id', str_replace(".", "", uniqid(rand(), true)), true, IndexType::PrimaryKey);
        $this->properties['operation'] = new Property('operation', '', true, IndexType::Normal);
        $this->properties['primary_key'] = new Property('primary_key', '', true, IndexType::Normal);
        $this->properties['table_name'] = new Property('table_name', '', true, IndexType::Normal);
        $this->properties['operation_by_user_id'] = new Property('operation_by_user_id', '', true, IndexType::Nullable);
        $this->properties['creation_date'] = new Property('creation_date', date('Y-m-d H:i:s'), true, IndexType::Normal);

        //Non-Persistable Data Members
        $this->properties['last_changed'] = new Property('last_changed','',false, IndexType::Timestamp);

        //ValidationRules
        $this->ValidationRules = new ValidationRules();
        $this->ValidationRules->add(new StringValidationRule('operation', 1, 1, 'operation'));
        $this->ValidationRules->add(new StringValidationRule('primary_key', 20, 30, 'primary_key'));
        $this->ValidationRules->add(new StringValidationRule('table_name', 2, 100, 'table_name'));
        $this->ValidationRules->add(new StringValidationRule('operation_by_user_id', 20, 30, 'primary_key'));
        
    }

    public static function GetDSN() {
        return DB_NAME.'.audits';
    }

}
