<?php
namespace HealthCare\Audit;

use HealthCare\Dao\IndexType;
use HealthCare\Dao\PersistableObject;
use HealthCare\Dao\Property;
use HealthCare\Dao\Validation\IllegalValueValidationRule;
use HealthCare\Dao\Validation\StringValidationRule;
use HealthCare\Dao\Validation\ValidationRules;

class AuditLine extends PersistableObject {

    function  __construct() {
        //Persistable Data Members
        $this->properties['id'] = new Property('id', str_replace(".", "", uniqid(rand(), true)), true, IndexType::PrimaryKey);
        $this->properties['audit_id'] = new Property('audit_id', '', true, IndexType::Normal);
        $this->properties['column_name'] = new Property('column_name', '', true, IndexType::Normal);
        $this->properties['old_value'] = new Property('old_value', '', true, IndexType::Normal);
        $this->properties['new_value'] = new Property('new_value', '', true, IndexType::Normal);
        $this->properties['creation_date'] = new Property('creation_date', date('Y-m-d H:i:s'), true, IndexType::Normal);

        //Non-Persistable Data Members
        $this->properties['last_changed'] = new Property('last_changed','',false, IndexType::Timestamp);

        //ValidationRules
        $this->ValidationRules = new ValidationRules();
        $this->ValidationRules->add(new StringValidationRule('column_name', 2, 100, 'column_name'));
        $this->ValidationRules->add(new IllegalValueValidationRule('audit_id', '-','audit_id'));
    }

    public static function GetDSN() {
        return DB_NAME.'.audit_lines';
    }

}
