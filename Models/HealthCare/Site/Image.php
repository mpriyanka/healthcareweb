<?php

namespace HealthCare\Site;

use HealthCare\Dao\IndexType;
use HealthCare\Dao\PersistableObject;
use HealthCare\Dao\Property;
use HealthCare\Dao\Validation\StringValidationRule;
use HealthCare\Dao\Validation\ValidationRules;

class Image extends PersistableObject {

    function __construct() {
        //Persistable Data Members
        $this->properties['id'] = new Property('id', str_replace(".", "", uniqid(rand(), true)), true, IndexType::PrimaryKey);
        $this->properties['folder'] = new Property('folder', '', true, IndexType::Normal);
        $this->properties['title'] = new Property('title', '', true, IndexType::Normal);
        $this->properties['path'] = new Property('path', '', true, IndexType::Normal);
        $this->properties['deleted'] = new Property('deleted', 0, true, IndexType::Normal);
        $this->properties['delete_timestamp'] = new Property('delete_timestamp', '0000-00-00 00:00:00', true, IndexType::Nullable);
        $this->properties['creation_date'] = new Property('creation_date', date('Y-m-d H:i:s'), true, IndexType::Normal);

        //Non-Persistable Data Members
        $this->properties['last_changed'] = new Property('last_changed', '', false, IndexType::Timestamp);

        //ValidationRules
        $this->ValidationRules = new ValidationRules();
        $this->ValidationRules->add(new StringValidationRule('title', 2, 100, 'title'));
        $this->ValidationRules->add(new StringValidationRule('folder', 2, 100, 'folder'));

        //TrackedPropertyChanges
        $this->TrackedPropertyChanges[] = 'title';
        $this->TrackedPropertyChanges[] = 'folder';
        $this->TrackedPropertyChanges[] = 'path';
        $this->TrackedPropertyChanges[] = 'deleted';
        $this->TrackedPropertyChanges[] = 'delete_timestamp';
    }

    public static function GetDSN() {
        return DB_NAME . '.images';
    }

}
