<?php

namespace HealthCare\Site;

use HealthCare\Dao\IndexType;
use HealthCare\Dao\PersistableObject;
use HealthCare\Dao\Property;
use HealthCare\Dao\Validation\SameValueValidationRule;
use HealthCare\Dao\Validation\StringValidationRule;
use HealthCare\Dao\Validation\ValidationRules;

class Doctor extends PersistableObject {

    function __construct() {
        //Persistable Data Members
        $this->properties['id'] = new Property('id', str_replace(".", "", uniqid(rand(), true)), true, IndexType::PrimaryKey);
        $this->properties['username'] = new Property('username', '', true, IndexType::Normal);
        $this->properties['password'] = new Property('password', '', true, IndexType::Normal);
        $this->properties['profile_id'] = new Property('profile_id', '', true, IndexType::Normal);
        $this->properties['accepted_date'] = new Property('accepted_date', '', true, IndexType::Nullable);
        $this->properties['gcm_token'] = new Property('gcm_token', '', true, IndexType::Nullable);
        $this->properties['status'] = new Property('status', 0, true, IndexType::Normal);
        $this->properties['ratings'] = new Property('ratings', 0, true, IndexType::Normal);
        $this->properties['ratings_count'] = new Property('ratings_count', 0, true, IndexType::Normal);
        $this->properties['deleted'] = new Property('deleted', 0, true, IndexType::Normal);
        $this->properties['delete_timestamp'] = new Property('delete_timestamp', '0000-00-00 00:00:00', true, IndexType::Nullable);
        $this->properties['creation_date'] = new Property('creation_date', date('Y-m-d H:i:s'), true, IndexType::Normal);

        //Non-Persistable Data Members
        $this->properties['last_changed'] = new Property('last_changed', '', false, IndexType::Timestamp);
        $this->properties['confirm_username'] = new Property('confirm_username', '', false, IndexType::Normal);

        //ValidationRules
        $this->ValidationRules = new ValidationRules();
        $this->ValidationRules->add(new StringValidationRule('username', 5, 250, 'username'));
        $this->ValidationRules->add(new SameValueValidationRule('confirm_username', 'username', 'confirm_username'));
        $this->ValidationRules->add(new StringValidationRule('password', 6, 40, 'password'));

        //TrackedPropertyChanges
        $this->TrackedPropertyChanges[] = 'username';
        $this->TrackedPropertyChanges[] = 'password';
        $this->TrackedPropertyChanges[] = 'status';
        $this->TrackedPropertyChanges[] = 'gcm_token';
        $this->TrackedPropertyChanges[] = 'ratings';
        $this->TrackedPropertyChanges[] = 'ratings_count';
        $this->TrackedPropertyChanges[] = 'deleted';
        $this->TrackedPropertyChanges[] = 'delete_timestamp';
    }

    public static function GetDSN() {
        return DB_NAME . '.doctors';
    }

}
