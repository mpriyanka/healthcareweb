<?php

namespace HealthCare\Site;

use HealthCare\Dao\IndexType;
use HealthCare\Dao\PersistableObject;
use HealthCare\Dao\Property;
use HealthCare\Dao\Validation\CanonicalPathValidationRule;
use HealthCare\Dao\Validation\IllegalValueValidationRule;
use HealthCare\Dao\Validation\StringValidationRule;
use HealthCare\Dao\Validation\ValidationRules;

class Page extends PersistableObject {

    function __construct() {
        //Persistable Data Members
        $this->properties['id'] = new Property('id', str_replace(".", "", uniqid(rand(), true)), true, IndexType::PrimaryKey);
        $this->properties['title'] = new Property('title', '', true, IndexType::Normal);
        $this->properties['url'] = new Property('url', '', true, IndexType::Normal);
        $this->properties['body'] = new Property('body', '', true, IndexType::Normal);
        $this->properties['level'] = new Property('level', 'User', true, IndexType::Normal);
        $this->properties['type'] = new Property('type', '', true, IndexType::Normal);
        $this->properties['miscellaneous'] = new Property('miscellaneous', '', true, IndexType::Normal);
        $this->properties['keywords'] = new Property('keywords', '', true, IndexType::Normal);
        $this->properties['tags'] = new Property('tags', '', true, IndexType::Normal);
        $this->properties['published'] = new Property('published', 0, true, IndexType::Normal);
        $this->properties['deleted'] = new Property('deleted', 0, true, IndexType::Normal);
        $this->properties['delete_timestamp'] = new Property('delete_timestamp', '0000-00-00 00:00:00', true, IndexType::Nullable);
        $this->properties['creation_date'] = new Property('creation_date', date('Y-m-d H:i:s'), true, IndexType::Normal);

        //Non-Persistable Data Members
        $this->properties['last_changed'] = new Property('last_changed', '', false, IndexType::Timestamp);

        //ValidationRules
        $this->ValidationRules = new ValidationRules();
        $this->ValidationRules->add(new StringValidationRule('title', 2, 650, 'title'));
        $this->ValidationRules->add(new CanonicalPathValidationRule('url', 2, 650, 'url'));
        $this->ValidationRules->add(new IllegalValueValidationRule('level', '-', 'level'));
        $this->ValidationRules->add(new IllegalValueValidationRule('type', '-', 'type'));
        $this->ValidationRules->add(new StringValidationRule('tags', 0, 200, 'tags'));
        $this->ValidationRules->add(new StringValidationRule('keywords', 0, 600, 'keywords'));

        //TrackedPropertyChanges
        $this->TrackedPropertyChanges[] = 'title';
        $this->TrackedPropertyChanges[] = 'url';
        $this->TrackedPropertyChanges[] = 'body';
        $this->TrackedPropertyChanges[] = 'level';
        $this->TrackedPropertyChanges[] = 'type';
        $this->TrackedPropertyChanges[] = 'miscellaneous';
        $this->TrackedPropertyChanges[] = 'keywords';
        $this->TrackedPropertyChanges[] = 'tags';
        $this->TrackedPropertyChanges[] = 'published';
        $this->TrackedPropertyChanges[] = 'deleted';
        $this->TrackedPropertyChanges[] = 'delete_timestamp';
    }

    public static function GetDSN() {
        return DB_NAME . '.pages';
    }

}
