<?php
namespace HealthCare\Site;

use HealthCare\Dao\IndexType;
use HealthCare\Dao\PersistableObject;
use HealthCare\Dao\Property;


class DoctorPasswordRecovery extends PersistableObject {

    function  __construct() {
        //Persistable Data Members
        $this->properties['id'] = new Property('id', str_replace(".", "", uniqid(rand(), true)), true, IndexType::PrimaryKey);
        $this->properties['doctor_id'] = new Property('doctor_id', '', true, IndexType::Normal);
        $this->properties['hash'] = new Property('hash', '', true, IndexType::Normal);
        $this->properties['status'] = new Property('status', 0, true, IndexType::Normal);
        $this->properties['creation_date'] = new Property('creation_date', date('Y-m-d H:i:s'), true, IndexType::Normal);

        //Non-Persistable Data Members
        $this->properties['last_changed'] = new Property('last_changed','',false, IndexType::Timestamp);

    }

    public static function GetDSN() {
        return DB_NAME.'.doctor_password_recoveries';
    }

}
