<?php

namespace HealthCare\Site\Impl;

use HealthCare\Dao\Exception\ExceptionManager;
use HealthCare\Dao\PersistenceManager;
use HealthCare\Site\Image;
use HealthCare\Util\AssertProperties;
use HealthCare\Util\DataPagination;
use HealthCare\Util\DataTable;
use HealthCare\Util\Impl\FileService;
use Exception;

class ImageService {

    public static function GetImages() {
        $result['status'] = 0;
        try {
            $columns['folder'] = array("sql" => "folder", 'display_name' => 'Folder', 'data_type' => 'String');
            $columns['title'] = array("sql" => "title", 'display_name' => 'Title', 'data_type' => 'String');
            $columns['creation_date'] = array("sql" => "creation_date", 'display_name' => 'Creation Date', 'data_type' => 'Date');
            $columns['last_changed'] = array("sql" => "last_changed", 'display_name' => 'Last Changed', 'data_type' => 'Date');

            $query_prpoperties_result = DataTable::GetQueryProperties($columns, "title", "asc");

            $pm = PersistenceManager::NewPersistenceManager();
            $query = $pm->getQueryBuilder('HealthCare\Site\Image');
            $sql = "select * from " . Image::GetDSN() . "_active " . $query_prpoperties_result['query'] . " order by " . $query_prpoperties_result['order_by'] . " " . $query_prpoperties_result['order_dir'];
            $row = $query->execute(str_replace("select *", "select count(*) as _row_count", $sql), $query_prpoperties_result['values'])->fetch();
            $total = $row['_row_count'];
            $pagination = new DataPagination();
            $pagination->setIndex($query_prpoperties_result['page_index']);
            $pagination->setSize(DATA_PAGE_SIZE);
            $pagination->setTotal($total);
            $pagination->setPages();
            $result['images'] = $query->executeQuery($sql, $query_prpoperties_result['values'], (($query_prpoperties_result['page_index'] - 1) * DATA_PAGE_SIZE), DATA_PAGE_SIZE);
            $pagination->setPageCount($result['images']->count());
            $result['pagination'] = $pagination;
            $result['columns'] = $columns;
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
            $result['error-msg'] = $e;
        }
        return $result;
    }

    public static function GetFolderNames() {
        $result['status'] = 0;
        try {
            $pm = PersistenceManager::NewPersistenceManager();
            $query = $pm->getQueryBuilder('HealthCare\Site\Image');
            $sql = "select distinct folder from " . Image::GetDSN() . "_active order by folder";
            $result['folder-names'] = $query->executeQuery($sql, array(), 0, -1);
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
            $result['error-msg'] = $e;
        }
        return $result;
    }

    public static function GetCreateImageTemplate() {
        $result['status'] = 0;
        $result['image'] = new Image();
        $result['status'] = 1;
        return $result;
    }

    public static function CreateImage($values) {
        $pm = null;
        $result['image'] = new Image();
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'title');
            $assertProperties->addProperty($values, 'folder');
            $assertProperties->addProperty($values, 'folders');

            $assertProperties->assert();

            $image = $result['image'];

            $image->setValue('title', $values['title']);
            if (strlen($values['folders']) > 1) {
                $image->setValue('folder', $values['folders']);
            } else {
                $image->setValue('folder', $values['folder']);
            }

            $image->CheckValidationRules($pm);

            $result['image'] = $image;

            if (count($image->ValidationErrors) == 0) {
                if (file_exists($_FILES['file']['tmp_name'])) {
                    $array = explode('.', $_FILES['file']['name']);
                    $path = 'uploads/image_' . rand(100000, 999999) . rand(100000, 999999) . rand(100000, 999999) . '.' . $array[count($array) - 1];

                    if (!FileService::Upload('file', $path, array('image/jpeg'))) {
                        throw new Exception('10023');
                    }

                    $image->setValue('path', $path);
                }
                $pm = PersistenceManager::getConnection();
                $pm->beginTransaction();
                $pm->saveWithAudit($image, $_SESSION['IPrincipal']['id']);
                $pm->commit();
                $result['status'] = 1;
                $result['image'] = new Image();
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            if (strpos($e, '1062 Duplicate entry') != false) {
                $result['error-msg'] = "Image already exists";
            } else if (strpos($e, '10023') != false) {
                $result['error-msg'] = "Unable to upload Image";
            } else {
                ExceptionManager::RaiseException($e);
            }
        }

        return $result;
    }

    public static function GetImage($values) {
        $result['status'] = 0;
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'id');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();
            $result['image'] = $pm->getObjectById('HealthCare\Site\Image', $values['id']);
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }

        return $result;
    }

    public static function GetImageByUrl($values) {
        $result['status'] = 0;
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'url');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();
            $result['image'] = $pm->getObjectByColumn('HealthCare\Site\Image', 'url', $values['url']);
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }

        return $result;
    }

    public static function GetImagesByTags($values) {
        $result['status'] = 0;
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'tags');
            $assertProperties->assert();

            $pm = PersistenceManager::NewPersistenceManager();
            $query = $pm->getQueryBuilder('HealthCare\Site\Image');
            $sql = "select title,url,path from " . Image::GetDSN() . "_active where tags = ? order by creation_date";
            $csql = "select count(*) from " . Image::GetDSN() . " where tags = ?";
            $row = $query->execute($csql, array($values['tags']))->fetch();
            $total = $row[0];
            $result['images'] = $query->executeQuery($sql, array($values['tags']), (($values['index'] - 1) * $values['size']), $values['size']);
            $pagination = new DataPagination();
            $pagination->setIndex($values['index']);
            $pagination->setSize($values['size']);
            $pagination->setTotal($total);
            $pagination->setUrl('/' . $values['url']);
            $pagination->setImages();
            $pagination->setImageCount($result['images']->count());
            $result['pagination'] = $pagination;
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }

        return $result;
    }

    public static function UpdateImage($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'title');
            $assertProperties->addProperty($values, 'folder');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();
            $image = $pm->getObjectById('HealthCare\Site\Image', $values['id']);

            $image->setValue('title', $values['title']);
            if (strlen($values['folders']) > 1) {
                $image->setValue('folder', $values['folders']);
            } else {
                $image->setValue('folder', $values['folder']);
            }

            $image->CheckValidationRules($pm);

            $result['image'] = $image;

            if (count($image->ValidationErrors) == 0) {
                if (file_exists($_FILES['file']['tmp_name'])) {
                    $array = explode('.', $_FILES['file']['name']);
                    $path = 'uploads/image_' . rand(100000, 999999) . rand(100000, 999999) . rand(100000, 999999) . '.' . $array[count($array) - 1];

                    if (!FileService::Upload('file', $path, array('image/jpeg'))) {
                        throw new Exception('10023');
                    }

                    $image->setValue('path', $path);
                }
                $pm->beginTransaction();
                $pm->saveWithAudit($image, $_SESSION['IPrincipal']['id']);
                $pm->commit();
                $result['status'] = 1;
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            if (strpos($e, '1062 Duplicate entry') != false) {
                $result['error-msg'] = "Image already exists";
            } else if (strpos($e, '10023') != false) {
                $result['error-msg'] = "Unable to upload Image";
            } else {
                ExceptionManager::RaiseException($e);
            }
        }

        return $result;
    }

    public static function DeleteImage($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'id');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $image = $pm->getObjectById('HealthCare\Site\Image', $values['id']);
            if ($image != null) {
                $image->setValue('deleted', 1);
                $image->setValue('delete_timestamp', date('Y-m-d H:i:s'));
                $pm->beginTransaction();
                $pm->saveWithAudit($image, $_SESSION['IPrincipal']['id']);
                $pm->commit();
                $result['status'] = 1;
            } else {
                $result['error-msg'] = 'Image not found';
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }
        return $result;
    }

}
