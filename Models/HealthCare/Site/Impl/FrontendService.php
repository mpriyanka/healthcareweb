<?php

namespace HealthCare\Site\Impl;

use Exception;
use HealthCare\Dao\Exception\ExceptionManager;
use HealthCare\Dao\PersistenceManager;
use HealthCare\Util\AssertProperties;

class FrontendService {

    public static function GetPageByUrl($values) {
        $result['status'] = 0;
        $result['error-msg'] = "";
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'url');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();
            $result['page'] = $pm->getObjectByColumns('HealthCare\Site\Page', array('url', 'published'), array($values['url'], 1));
            if ($result['page'] != null) {
                $result['status'] = 1;
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }
        return $result;
    }

}
