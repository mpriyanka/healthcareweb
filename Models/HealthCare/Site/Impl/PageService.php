<?php

namespace HealthCare\Site\Impl;

use HealthCare\Dao\Exception\ExceptionManager;
use HealthCare\Dao\PersistenceManager;
use HealthCare\Site\Page;
use HealthCare\Util\AssertProperties;
use HealthCare\Util\DataPagination;
use HealthCare\Util\DataTable;
use Exception;

class PageService {

    public static function GetPages() {
        $result['status'] = 0;
        try {
            $columns['title'] = array("sql" => "title", 'display_name' => 'Title', 'data_type' => 'String');
            $columns['url'] = array("sql" => "url", 'display_name' => 'URL Path', 'data_type' => 'String');
            $columns['tags'] = array("sql" => "tags", 'display_name' => 'Tags', 'data_type' => 'String');
            $columns['keywords'] = array("sql" => "keywords", 'display_name' => 'Keywords', 'data_type' => 'String');
            $columns['type'] = array("sql" => "type", 'display_name' => 'Type', 'data_type' => 'Dropdown', 'dropdown_list' => json_decode(MODEL_PAGE_TYPES, true));
            $columns['level'] = array("sql" => "level", 'display_name' => 'Level', 'data_type' => 'Dropdown', 'dropdown_list' => array('System' => 'System', 'User' => 'User'));
            $columns['published'] = array("sql" => "published", 'display_name' => 'Published', 'data_type' => 'Dropdown', 'dropdown_list' => array(0 => 'No', 1 => 'Yes'));
            $columns['creation_date'] = array("sql" => "creation_date", 'display_name' => 'Creation Date', 'data_type' => 'Date');
            $columns['last_changed'] = array("sql" => "last_changed", 'display_name' => 'Last Changed', 'data_type' => 'Date');

            $query_prpoperties_result = DataTable::GetQueryProperties($columns, "title", "asc");

            $pm = PersistenceManager::NewPersistenceManager();
            $query = $pm->getQueryBuilder('HealthCare\Site\Page');
            $sql = "select * from " . Page::GetDSN() . "_active " . $query_prpoperties_result['query'] . " order by " . $query_prpoperties_result['order_by'] . " " . $query_prpoperties_result['order_dir'];
            $row = $query->execute(str_replace("select *", "select count(*) as _row_count", $sql), $query_prpoperties_result['values'])->fetch();
            $total = $row['_row_count'];
            $pagination = new DataPagination();
            $pagination->setIndex($query_prpoperties_result['page_index']);
            $pagination->setSize(DATA_PAGE_SIZE);
            $pagination->setTotal($total);
            $pagination->setPages();
            $result['pages'] = $query->executeQuery($sql, $query_prpoperties_result['values'], (($query_prpoperties_result['page_index'] - 1) * DATA_PAGE_SIZE), DATA_PAGE_SIZE);
            $pagination->setPageCount($result['pages']->count());
            $result['pagination'] = $pagination;
            $result['columns'] = $columns;
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
            $result['error-msg'] = $e;
        }
        return $result;
    }

    public static function GetPageNames() {
        $result['status'] = 0;
        try {
            $pm = PersistenceManager::NewPersistenceManager();
            $query = $pm->getQueryBuilder('HealthCare\Site\Page');
            $sql = "select id,title,url,tags from " . Page::GetDSN() . "_active order by title";
            $result['page-names'] = $query->executeQuery($sql, array(), 0, -1);
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
            $result['error-msg'] = $e;
        }
        return $result;
    }

    public static function GetCreatePageTemplate() {
        $result['status'] = 0;
        $result['page'] = new Page();
        $result['status'] = 1;
        return $result;
    }

    public static function CreatePage($values) {
        $pm = null;
        $result['page'] = new Page();
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $page = $result['page'];

            $page->setValue('title', $values['title']);
            $page->setValue('url', $values['url']);
            $page->setValue('type', $values['type']);
            $page->setValue('keywords', $values['keywords']);
            $page->setValue('tags', $values['tags']);

            $page->CheckValidationRules();

            $result['page'] = $page;

            if (count($page->ValidationErrors) == 0) {
                $pm = PersistenceManager::getConnection();
                $pm->beginTransaction();
                $pm->saveWithAudit($page, $_SESSION['IPrincipal']['id']);
                $pm->commit();
                $result['status'] = 1;
                $result['page'] = new Page();
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            if (strpos($e, '1062 Duplicate entry') != false) {
                $result['error-msg'] = "Page already exists";
            } else if (strpos($e, '10023') != false) {
                $result['error-msg'] = "Unable to upload Image";
            } else {
                ExceptionManager::RaiseException($e);
            }
        }

        return $result;
    }

    public static function GetPage($values) {
        $result['status'] = 0;
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'id');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();
            $result['page'] = $pm->getObjectById('HealthCare\Site\Page', $values['id']);
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }

        return $result;
    }

    public static function GetPageByUrl($values) {
        $result['status'] = 0;
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'url');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();
            $result['page'] = $pm->getObjectByColumn('HealthCare\Site\Page', 'url', $values['url']);
            if ($result['page'] != null && $result['page']->getValue('published') == 1) {
                $result['status'] = 1;
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }

        return $result;
    }

    public static function GetPagesByType($values) {
        $result['status'] = 0;
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'type');
            $assertProperties->assert();

            $pm = PersistenceManager::NewPersistenceManager();
            $query = $pm->getQueryBuilder('HealthCare\Site\Page');
            $sql = "select title,url,miscellaneous,body,creation_date from " . Page::GetDSN() . "_active_published where type = ? order by creation_date desc";
            $csql = "select count(*) from " . Page::GetDSN() . "_active_published where type = ?";
            $row = $query->execute($csql, array($values['type']))->fetch();
            $total = $row[0];
            $result['pages'] = $query->executeQuery($sql, array($values['type']), (($values['index'] - 1) * $values['size']), $values['size']);
            $pagination = new DataPagination();
            $pagination->setIndex($values['index']);
            $pagination->setSize($values['size']);
            $pagination->setTotal($total);
            //$pagination->setUrl('/' . $values['url']);
            $pagination->setPages();
            $pagination->setPageCount($result['pages']->count());
            $result['pagination'] = $pagination;
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }

        return $result;
    }

    public static function UpdatePage($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {

            $result = self::GetPage($values);

            $result['status'] = 0;
            $result['error-msg'] = '';

            $page = $result['page'];

            if (strcasecmp($page->getValue('level'), "System") != 0) {
                $page->setValue('title', $values['title']);
                $page->setValue('url', $values['url']);
            }

            $page->setValue('tags', $values['tags']);
            $page->setValue('type', $values['type']);
            $page->setValue('keywords', $values['keywords']);

            $page->CheckValidationRules();

            $result['page'] = $page;

            if (count($page->ValidationErrors) == 0) {
                $pm = PersistenceManager::getConnection();
                $pm->beginTransaction();
                $pm->saveWithAudit($page, $_SESSION['IPrincipal']['id']);
                $pm->commit();
                $result['status'] = 1;
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            if (strpos($e, '1062 Duplicate entry') != false) {
                $result['error-msg'] = "Page already exists";
            } else if (strpos($e, '10023') != false) {
                $result['error-msg'] = "Unable to upload Image";
            } else {
                ExceptionManager::RaiseException($e);
            }
        }

        return $result;
    }

    public static function UpdatePageBody($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'id');
            $assertProperties->addProperty($values, 'body');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();
            $page = $pm->getObjectById('HealthCare\Site\Page', $values['id']);

            if (get_magic_quotes_gpc()) {
                $page->setValue('body', stripslashes($values['body']));
            } else {
                $page->setValue('body', $values['body']);
            }

            $page->CheckValidationRules($pm);

            $result['page'] = $page;

            if (count($page->ValidationErrors) == 0) {
                $pm->beginTransaction();
                $pm->saveWithAudit($page, $_SESSION['IPrincipal']['id']);
                $pm->commit();
                $result['status'] = 1;
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }

        return $result;
    }

    public static function UpdatePublishStatus($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'id');
            $assertProperties->addProperty($values, 'published');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();
            $page = $pm->getObjectById('HealthCare\Site\Page', $values['id']);

            if (strcasecmp($page->getValue('level'), "System") != 0) {
                $page->setValue('published', $values['published']);
            }

            $page->CheckValidationRules();

            $result['page'] = $page;

            if (count($page->ValidationErrors) == 0) {
                $pm->beginTransaction();
                $pm->saveWithAudit($page, $_SESSION['IPrincipal']['id']);
                $pm->commit();
                $result['status'] = 1;
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }

        return $result;
    }

    public static function DeletePage($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'id');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $page = $pm->getObjectById('HealthCare\Site\Page', $values['id']);
            if ($page != null) {
                if (strcmp($page->getValue('level'), "System") == 0) {
                    $result['error-msg'] = 'Cannot Delete System Page';
                } else {
                    $page->setValue('deleted', 1);
                    $page->setValue('delete_timestamp', date('Y-m-d H:i:s'));
                    $pm->beginTransaction();
                    $pm->saveWithAudit($page, $_SESSION['IPrincipal']['id']);
                    $pm->commit();
                    $result['status'] = 1;
                }
            } else {
                $result['error-msg'] = 'Page not found';
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }
        return $result;
    }

}
