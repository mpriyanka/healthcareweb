<?php

namespace HealthCare\Site\Impl;

use Exception;
use HealthCare\Dao\Exception\ExceptionManager;
use HealthCare\Dao\PersistenceManager;
use HealthCare\Site\Doctor;
use HealthCare\Site\DoctorPasswordRecovery;
use HealthCare\Util\Address;
use HealthCare\Util\AssertProperties;
use HealthCare\Util\DataPagination;
use HealthCare\Util\DataTable;
use HealthCare\Util\Impl\EmailNotificationService;
use HealthCare\Util\Impl\FileService;
use HealthCare\Util\Profile;

class DoctorService {

    public static function GetDoctors() {
        $result['status'] = 0;
        try {
            $columns["name"] = array("sql" => "concat('Dr. ',p.firstname,' ',p.middlename,' ',p.lastname)", 'display_name' => 'Name', 'data_type' => 'String');
            $columns['username'] = array("sql" => "mem.username", 'display_name' => 'Username', 'data_type' => 'String');
            $columns['status'] = array("sql" => "doc.status", 'display_name' => 'Status', 'data_type' => 'Dropdown', 'dropdown_list' => array('Pending Verification', 'Pending Review', 'Approved', 'Rejected', 'NA', 'Pending Password Reset', 'Disabled'));
            $columns['accepted_date'] = array("sql" => "doc.accepted_date", 'display_name' => 'Accepted Date', 'data_type' => 'Date');
            $columns["phone"] = array("sql" => "p.phone", 'display_name' => 'Phone', 'data_type' => 'String');
            $columns["mobile"] = array("sql" => "p.mobile", 'display_name' => 'Mobile', 'data_type' => 'String');
            $columns["address"] = array("sql" => "concat(a.line_1,' ',a.line_2)", 'display_name' => 'Address', 'data_type' => 'String');
            $columns["city"] = array("sql" => "a.city", 'display_name' => 'City', 'data_type' => 'String');
            $columns["state"] = array("sql" => "a.state", 'display_name' => 'State', 'data_type' => 'String');
            $columns["country"] = array("sql" => "a.country", 'display_name' => 'Country', 'data_type' => 'String');
            $columns['creation_date'] = array("sql" => "doc.creation_date", 'display_name' => 'Creation Date', 'data_type' => 'Date');
            $columns['last_changed'] = array("sql" => "doc.last_changed", 'display_name' => 'Last Changed', 'data_type' => 'Date');


            $query_prpoperties_result = DataTable::GetQueryProperties($columns, "name", "asc");

            $pm = PersistenceManager::NewPersistenceManager();
            $query = $pm->getQueryBuilder('HealthCare\Site\Doctor');
            $sql = "select doc.id,doc.username,doc.status,IFNULL(DATE_FORMAT(doc.accepted_date, '%d/%m/%Y'), '') as accepted_date,p.firstname,p.middlename,p.lastname,p.email,p.phone,p.mobile,p.image_path,a.line_1,a.line_2,a.city,a.state,a.country,doc.creation_date,doc.last_changed from " . Doctor::GetDSN() . "_active as doc inner join " . Profile::GetDSN() . " as p on doc.profile_id = p.id inner join " . Address::GetDSN() . " as a on p.address_id = a.id " . $query_prpoperties_result['query'] . " order by " . $query_prpoperties_result['order_by'] . " " . $query_prpoperties_result['order_dir'];
            $row = $query->execute(str_replace("select", "select count(*) as _row_count,", $sql), $query_prpoperties_result['values'])->fetch();
            $total = $row['_row_count'];
            $pagination = new DataPagination();
            $pagination->setIndex($query_prpoperties_result['page_index']);
            $pagination->setSize(DATA_PAGE_SIZE);
            $pagination->setTotal($total);
            $pagination->setPages();
            $result['doctors'] = $query->executeQuery($sql, $query_prpoperties_result['values'], (($query_prpoperties_result['page_index'] - 1) * DATA_PAGE_SIZE), DATA_PAGE_SIZE);
            $pagination->setPageCount($result['doctors']->count());
            $result['pagination'] = $pagination;
            $result['columns'] = $columns;
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
            $result['error-msg'] = $e;
        }
        return $result;
    }
    
    public static function GetDoctorsWS($values) {
        $result['status'] = 0;
        try {
            $columns["name"] = array("sql" => "concat('Dr. ',p.firstname,' ',p.middlename,' ',p.lastname)", 'display_name' => 'Name', 'data_type' => 'String');
            $columns['username'] = array("sql" => "mem.username", 'display_name' => 'Username', 'data_type' => 'String');
            $columns['status'] = array("sql" => "doc.status", 'display_name' => 'Status', 'data_type' => 'Dropdown', 'dropdown_list' => array('Pending Verification', 'Pending Review', 'Approved', 'Rejected', 'NA', 'Pending Password Reset', 'Disabled'));
            $columns['accepted_date'] = array("sql" => "doc.accepted_date", 'display_name' => 'Accepted Date', 'data_type' => 'Date');
            $columns["phone"] = array("sql" => "p.phone", 'display_name' => 'Phone', 'data_type' => 'String');
            $columns["mobile"] = array("sql" => "p.mobile", 'display_name' => 'Mobile', 'data_type' => 'String');
            $columns["address"] = array("sql" => "concat(a.line_1,' ',a.line_2)", 'display_name' => 'Address', 'data_type' => 'String');
            $columns["city"] = array("sql" => "a.city", 'display_name' => 'City', 'data_type' => 'String');
            $columns["state"] = array("sql" => "a.state", 'display_name' => 'State', 'data_type' => 'String');
            $columns["country"] = array("sql" => "a.country", 'display_name' => 'Country', 'data_type' => 'String');
            $columns['creation_date'] = array("sql" => "doc.creation_date", 'display_name' => 'Creation Date', 'data_type' => 'Date');
            $columns['last_changed'] = array("sql" => "doc.last_changed", 'display_name' => 'Last Changed', 'data_type' => 'Date');


            $query_prpoperties_result = DataTable::GetQueryProperties($columns, "name", "asc");
            
            if (strlen($query_prpoperties_result['query']) > 0) {
                $query_prpoperties_result['query'] = str_replace("where", "where doc.status = '2' and", $query_prpoperties_result['query']);
            } else {
                $query_prpoperties_result['query'] = "where doc.status = '2'";
            }

            $pm = PersistenceManager::NewPersistenceManager();
            $query = $pm->getQueryBuilder('HealthCare\Site\Doctor');
            $sql = "select doc.id,doc.username,doc.status,IFNULL(DATE_FORMAT(doc.accepted_date, '%d/%m/%Y'), '') as accepted_date,p.firstname,p.middlename,p.lastname,p.email,p.phone,p.mobile,p.image_path,a.line_1,a.line_2,a.city,a.state,a.country,doc.creation_date,doc.last_changed from " . Doctor::GetDSN() . "_active as doc inner join " . Profile::GetDSN() . " as p on doc.profile_id = p.id inner join " . Address::GetDSN() . " as a on p.address_id = a.id " . $query_prpoperties_result['query'] . " order by " . $query_prpoperties_result['order_by'] . " " . $query_prpoperties_result['order_dir'];
            $row = $query->execute(str_replace("select", "select count(*) as _row_count,", $sql), $query_prpoperties_result['values'])->fetch();
            $total = $row['_row_count'];
            $pagination = new DataPagination();
            $pagination->setIndex($values['index']);
            $pagination->setSize($values['size']);
            $pagination->setTotal($total);
            $pagination->setPages();
            $result['doctors'] = $query->executeQuery($sql, $query_prpoperties_result['values'], (($values['index'] - 1) * $values['size']), $values['size']);
            $pagination->setPageCount($result['doctors']->count());
            $result['pagination'] = $pagination;
            $result['columns'] = $columns;
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
            $result['error-msg'] = $e;
        }
        return $result;
    }

    public static function GetDoctorNames() {
        $result['status'] = 0;
        try {
            $pm = PersistenceManager::NewPersistenceManager();
            $query = $pm->getQueryBuilder('HealthCare\Site\Doctor');
            $sql = "select id,title,url,tags from " . Doctor::GetDSN() . "_active order by title";
            $result['page-names'] = $query->executeQuery($sql, array(), 0, -1);
            $result['status'] = 1;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
            $result['error-msg'] = $e;
        }
        return $result;
    }

    public static function GetCreateDoctorTemplate() {
        $result['status'] = 0;
        $result['address'] = new Address();
        $result['profile'] = new Profile(false);
        $result['status'] = 1;
        return $result;
    }

    public static function CreateDoctor($values) {
        $pm = null;
        $result['address'] = new Address();
        $result['profile'] = new Profile(false);
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $profile = $result['profile'];
            $address = $result['address'];

            $profile->setValue('address_id', $address->getValue('id'));
            $profile->setValue('title', "Dr");
            $profile->setValue('firstname', $values['firstname']);
            $profile->setValue('middlename', $values['middlename']);
            $profile->setValue('lastname', $values['lastname']);
            $profile->setValue('phone', $values['phone']);
            $profile->setValue('mobile', $values['mobile']);
            $profile->setValue('email', $values['email']);

            $address->setValue('line_1', $values['line_1']);
            $address->setValue('line_2', $values['line_2']);
            $address->setValue('city', $values['city']);
            $address->setValue('state', $values['state']);
            $address->setValue('country', $values['country']);

            $profile->CheckValidationRules();
            $address->CheckValidationRules();

            $result['profiel'] = $profile;

            if (count($profile->ValidationErrors) == 0 && count($address->ValidationErrors) == 0) {
                if (file_exists($_FILES['file']['tmp_name'])) {
                    $array = explode('.', $_FILES['file']['name']);
                    $path = 'uploads/medical_profile_' . rand(100000, 999999) . rand(100000, 999999) . rand(100000, 999999) . '.' . $array[count($array) - 1];

                    if (!FileService::Upload('file', $path, array('application/pdf', 'application/msword', 'application/zip', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'))) {
                        throw new Exception('10023');
                    }

                    $profile->setValue('cv_path', $path);
                } else {
                    throw new Exception('10023');
                }
                $array = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
                    "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
                    "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "!", "@", "#", "$", "?");

                $random_password = $array[rand(1, count($array)) - 1] . $array[rand(1, count($array)) - 1] . $array[rand(1, count($array)) - 1] . $array[rand(1, count($array)) - 1] . $array[rand(1, count($array)) - 1] . $array[rand(1, count($array)) - 1] . $array[rand(1, count($array)) - 1] . $array[rand(1, count($array)) - 1];
                $doctor = new Doctor();
                $doctor->setValue('profile_id', $profile->getValue('id'));
                $doctor->setValue('username', $profile->getValue('email'));
                $doctor->setValue('password', md5($doctor->getValue('id') . $random_password . $doctor->getValue('username')));

                $pm = PersistenceManager::getConnection();
                $pm->beginTransaction();
                $pm->saveWithAudit($address, '');
                $pm->saveWithAudit($profile, '');
                $pm->saveWithAudit($doctor, '');
                $pm->commit();
                $values = array();
                $values['subject'] = "Account Verification Required";
                $values['url'] = "email-doctor-account-creation";
                $values['email'] = $doctor->getValue('username');
                $values['placeholder']['${verification_link}'] = "<a href=\"" . CONTEXT_PATH . "/doctor-email-verification/" . $doctor->getValue('id') . "/" . md5($doctor->getValue('id') . $doctor->getValue('username') . $doctor->getValue('creation_date')) . "\">" . CONTEXT_PATH . "/doctor-email-verification/" . $doctor->getValue('id') . "/" . md5($doctor->getValue('id') . $doctor->getValue('username') . $doctor->getValue('creation_date')) . "</a>";
                $values['placeholder']['${site_name}'] = SITE_NAME;
                $values['placeholder']['${billing_email}'] = BILLING_EMAIL;
                $values['placeholder']['${support_email}'] = SUPPORT_EMAIL;
                $values['placeholder']['${password}'] = $random_password;
                $values['placeholder']['${username}'] = $values['email'];
                $values['placeholder']['${name}'] = $profile->getValue('title') . ' ' . $profile->getValue('firstname') . ' ' . $profile->getValue('lastname');
                EmailNotificationService::Sendmail($values);
                $result['status'] = 1;
                $result['address'] = new Address();
                $result['profile'] = new Profile(false);
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            if (strpos($e, '1062 Duplicate entry') != false) {
                $result['error-msg'] = "Doctor already exists";
            } else if (strpos($e, '10023') != false) {
                $result['error-msg'] = "Unable to upload Medical Profile";
            } else {
                ExceptionManager::RaiseException($e);
            }
        }

        return $result;
    }

    public static function UpdateDoctor($values) {
        $pm = null;
        $result['status'] = 0;
        $result['error-msg'] = '';
        $result['profile'] = new Profile();
        $result['address'] = new Address();
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'firstname');
            $assertProperties->addProperty($values, 'middlename');
            $assertProperties->addProperty($values, 'lastname');
            $assertProperties->addProperty($values, 'phone');
            $assertProperties->addProperty($values, 'mobile');
            $assertProperties->addProperty($values, 'line_1');
            $assertProperties->addProperty($values, 'line_2');
            $assertProperties->addProperty($values, 'city');
            $assertProperties->addProperty($values, 'state');
            $assertProperties->addProperty($values, 'country');
            $assertProperties->addProperty($values, 'bio');
            $assertProperties->addProperty($values, 'id');

            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $doctor = $pm->getObjectById('HealthCare\Site\Doctor', $values['id']);
            if ($doctor == null) {
                throw new Exception("404");
            }

            $result['profile'] = $pm->getObjectById('HealthCare\Util\Profile', $doctor->getValue('profile_id'));
            $result['address'] = $pm->getObjectById('HealthCare\Util\Address', $result['profile']->getValue('address_id'));
            $result['doctor'] = $doctor;

            $address = $result['address'];
            $profile = $result['profile'];

            $profile->setValue('firstname', $values['firstname']);
            $profile->setValue('middlename', $values['middlename']);
            $profile->setValue('lastname', $values['lastname']);
            $profile->setValue('phone', $values['phone']);
            $profile->setValue('mobile', $values['mobile']);
            $profile->setValue('bio', $values['bio']);

            $address->setValue('line_1', $values['line_1']);
            $address->setValue('line_2', $values['line_2']);
            $address->setValue('city', $values['city']);
            $address->setValue('state', $values['state']);
            $address->setValue('country', $values['country']);

            $address->CheckValidationRules(null);
            $profile->CheckValidationRules(null);

            $result['address'] = $address;
            $result['profile'] = $profile;

            if (count($address->ValidationErrors) == 0 && count($profile->ValidationErrors) == 0) {
                if (file_exists($_FILES['image_path']['tmp_name'])) {
                    $array = explode('.', $_FILES['image_path']['name']);
                    $path = 'uploads/doc_photo_' . rand(100000, 999999) . rand(100000, 999999) . rand(100000, 999999) . '.' . $array[count($array) - 1];

                    if (!FileService::Upload('image_path', $path, array('image/jpeg'))) {
                        throw new Exception('10023');
                    }

                    $profile->setValue('image_path', $path);
                }
                $pm = PersistenceManager::getConnection();
                $pm->beginTransaction();
                $pm->saveWithAudit($address, $_SESSION['IPrincipal']['id']);
                $pm->saveWithAudit($profile, $_SESSION['IPrincipal']['id']);
                $pm->commit();
                $result['status'] = 1;
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            if (strpos($e, '10023') != false) {
                $result['error-msg'] = "Unable to upload Photo, only .jpg images are allowed";
            } else if (strpos($e, '404') != false) {
                $result['error-msg'] = "Doctor not found";
            } else {
                ExceptionManager::RaiseException($e);
            }
        }
        return $result;
    }

    public static function ResendVerifcation($values) {
        $pm = null;
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'id');

            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();
            $doctor = $pm->getObjectById("HealthCare\Site\Doctor", $values['id']);

            if ($doctor->getValue('status') != 0) {
                throw new Exception("1011");
            }

            $profile = $pm->getObjectById("HealthCare\Util\Profile", $doctor->getValue('profile_id'));

            $array = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
                "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
                "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "!", "@", "#", "$", "?");

            $random_password = $array[rand(1, count($array)) - 1] . $array[rand(1, count($array)) - 1] . $array[rand(1, count($array)) - 1] . $array[rand(1, count($array)) - 1] . $array[rand(1, count($array)) - 1] . $array[rand(1, count($array)) - 1] . $array[rand(1, count($array)) - 1] . $array[rand(1, count($array)) - 1];
            $doctor->setValue('password', md5($doctor->getValue('id') . $random_password . $doctor->getValue('username')));

            $pm->save($doctor);
            
            $values['subject'] = "Account Verification Required";
            $values['url'] = "email-doctor-account-creation";
            $values['email'] = $doctor->getValue('username');
            $values['placeholder']['${verification_link}'] = "<a href=\"" . CONTEXT_PATH . "/doctor-email-verification/" . $doctor->getValue('id') . "/" . md5($doctor->getValue('id') . $doctor->getValue('username') . $doctor->getValue('creation_date')) . "\">" . CONTEXT_PATH . "/doctor-email-verification/" . $doctor->getValue('id') . "/" . md5($doctor->getValue('id') . $doctor->getValue('username') . $doctor->getValue('creation_date')) . "</a>";
            $values['placeholder']['${site_name}'] = SITE_NAME;
            $values['placeholder']['${billing_email}'] = BILLING_EMAIL;
            $values['placeholder']['${support_email}'] = SUPPORT_EMAIL;
            $values['placeholder']['${password}'] = $random_password;
            $values['placeholder']['${username}'] = $values['email'];
            $values['placeholder']['${name}'] = $profile->getValue('title') . ' ' . $profile->getValue('firstname') . ' ' . $profile->getValue('lastname');
            EmailNotificationService::Sendmail($values);
            $result['status'] = 1;
        } catch (Exception $e) {
            if (strpos($e, '1011') != false) {
                $result['error-msg'] = "Invalid State, cannot resend verification for Doctor";
            } else {
                ExceptionManager::RaiseException($e);
            }
        }
        return $result;
    }
    
    public static function AcceptDoctor($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'id');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();
            $doctor = $pm->getObjectById('HealthCare\Site\Doctor', $values['id']);
            
            $result['doctor'] = $doctor;
            
            if ($doctor->getValue('status')  != 1) {
                throw new Exception("1040");
            }

            $doctor->setValue('accepted_date', date('Y-m-d'));
            $doctor->setValue('status', 2);
            
            //$doctor->CheckValidationRules($pm);

            if (count($doctor->ValidationErrors) == 0) {
                $pm->save($doctor);

                $profile = $pm->getObjectById('HealthCare\Util\Profile', $doctor->getValue('profile_id'));
                $values['subject'] = "Doctor Acceptance";
                $values['url'] = "doctor-accepted";
                $values['email'] = $doctor->getValue('username');
                $values['placeholder']['${site_name}'] = SITE_NAME;
                $values['placeholder']['${billing_email}'] = BILLING_EMAIL;
                $values['placeholder']['${support_email}'] = SUPPORT_EMAIL;
                $values['placeholder']['${name}'] = $profile->getValue('title') . ' ' . $profile->getValue('firstname') . ' ' . $profile->getValue('lastname');
                EmailNotificationService::Sendmail($values);
                $result['status'] = 1;
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            if (strpos($e, '1040') != false) {
                $result['error-msg'] = "Invalid State";
            } else {
                ExceptionManager::RaiseException($e);
            }
        }

        return $result;
    }

    public static function GetDoctor($values) {
        $result['status'] = 0;
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'id');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();
            $doctor = $pm->getObjectById('HealthCare\Site\Doctor', $values['id']);
            if ($result != null) {
                $result['profile'] = $pm->getObjectById('HealthCare\Util\Profile', $doctor->getValue('profile_id'));
                $result['address'] = $pm->getObjectById('HealthCare\Util\Address', $result['profile']->getValue('address_id'));
                $result['doctor'] = $doctor;
                $result['status'] = 1;
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }

        return $result;
    }

    public static function VerifyEmail($values, $redirect) {
        try {
            $pm = PersistenceManager::getConnection();
            $doctor = $pm->getObjectById('HealthCare\Site\Doctor', $values['id']);
            if ($doctor != null && $doctor->getValue('status') == 0 && strcmp($values['hash'], md5($doctor->getValue('id') . $doctor->getValue('username') . $doctor->getValue('creation_date'))) == 0) {
                $doctor->setValue('status', 1);
                $pm->save($doctor);

                $profile = $pm->getObjectById('HealthCare\Util\Profile', $doctor->getValue('profile_id'));
                $values['subject'] = "Email Verification Completed";
                $values['url'] = "doctor-email-verified";
                $values['email'] = $doctor->getValue('username');
                $values['placeholder']['${site_name}'] = SITE_NAME;
                $values['placeholder']['${name}'] = 'Dr ' . $profile->getValue('firstname') . ' ' . $profile->getValue('lastname');
                EmailNotificationService::Sendmail($values);
                $values['subject'] = "Doctors Board Review Notification";
                $values['url'] = "doc-board-rnotice";
                $values['email'] = $doctor->getValue('username');
                $values['placeholder']['${site_name}'] = SITE_NAME;
                $values['email'] = NOTIFICATION_EMAIL;
                EmailNotificationService::Sendmail($values);
                self::Redirect($redirect);
            } else {
                echo "<h2>Failed</h2>";
                exit;
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }
    }

    public static function Logout() {
        try {
            if (isset($_SESSION['IDoctor'])) {
                unset($_SESSION['IDoctor']);
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }
    }

    public static function Login($username, $password, $redirect) {
        $result['status'] = 0;
        $result['error-msg'] = 'Invalid Username or Password';
        try {
            if (self::IsAuthenticated()) {
                self::Logout();
            }
            $pm = PersistenceManager::getConnection();
            $query = $pm->getQueryBuilder('HealthCare\Site\Doctor');
            $sql = 'select id,username,password,status,profile_id from ' . Doctor::GetDSN() . '_active where username = ?';
            $values = array($username);
            $logins = $query->executeQuery($sql, $values, 0, 1);
            if ($logins->count() > 0 && strcmp($logins[0]->getValue('password'), md5($logins[0]->getValue('id') . $password . strtolower($logins[0]->getValue('username')))) == 0 && $logins[0]->getValue('status') > 0 && $logins[0]->getValue('status') < 5) {
                $_SESSION['IDoctor'] = array('id' => $logins[0]->getValue('id'), 'username' => $logins[0]->getValue('username'), 'profile_id' => $logins[0]->getValue('profile_id'));
                self::Redirect($redirect);
            } else if ($logins->count() > 0 && strcmp($logins[0]->getValue('password'), md5($logins[0]->getValue('id') . $password . strtolower($logins[0]->getValue('username')))) == 0 && $logins[0]->getValue('status') == 0) {
                $result['error-msg'] = 'Email verification required. Click the link sent to your Email';
            } else if ($logins->count() > 0 && strcmp($logins[0]->getValue('password'), md5($logins[0]->getValue('id') . $password . strtolower($logins[0]->getValue('username')))) == 0 && $logins[0]->getValue('status') == 5) {
                $result['error-msg'] = 'Password Reset required';
            } else if ($logins->count() > 0 && strcmp($logins[0]->getValue('password'), md5($logins[0]->getValue('id') . $password . strtolower($logins[0]->getValue('username')))) == 0 && $logins[0]->getValue('status') == 6) {
                $result['error-msg'] = 'Account disabled';
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
            $result['error-msg'] = 'An error occurred';
        }
        return $result;
    }
    
    public static function LoginWS($values) {
        $result['status'] = 0;
        $result['error-msg'] = 'Invalid Username or Password';
        try {
            $pm = PersistenceManager::getConnection();
            $query = $pm->getQueryBuilder('HealthCare\Site\Doctor');
            $sql = "select doc.id,doc.username,doc.password,doc.status,concat(p.firstname, ' ', p.lastname) as fullname from " . Doctor::GetDSN() . "_active as doc inner join ".Profile::GetDSN()." as p on doc.profile_id = p.id where doc.username = ?";
            $logins = $query->executeQuery($sql, array($values['username']), 0, 1);
            if ($logins->count() > 0 && strcmp($logins[0]->getValue('password'), md5($logins[0]->getValue('id') . $values['password'] . strtolower($logins[0]->getValue('username')))) == 0 && $logins[0]->getValue('status') > 0 && $logins[0]->getValue('status') < 5) {
                $result['status'] = 1;
                $result['fullname'] = $logins[0]->getValue('fullname');
                $result['username'] = $values['username'];
            } else if ($logins->count() > 0 && strcmp($logins[0]->getValue('password'), md5($logins[0]->getValue('id') . $values['password'] . strtolower($logins[0]->getValue('username')))) == 0 && $logins[0]->getValue('status') == 0) {
                $result['error-msg'] = 'Email verification required. Click the link sent to your Email';
            } else if ($logins->count() > 0 && strcmp($logins[0]->getValue('password'), md5($logins[0]->getValue('id') . $values['password'] . strtolower($logins[0]->getValue('username')))) == 0 && $logins[0]->getValue('status') == 5) {
                $result['error-msg'] = 'Password Reset required';
            } else if ($logins->count() > 0 && strcmp($logins[0]->getValue('password'), md5($logins[0]->getValue('id') . $values['password'] . strtolower($logins[0]->getValue('username')))) == 0 && $logins[0]->getValue('status') == 6) {
                $result['error-msg'] = 'Account disabled';
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
            $result['error-msg'] = 'An error occurred';
        }
        return $result;
    }

    public static function ForgotPassword($username) {
        $result['status'] = 0;
        $result['error-msg'] = 'Enter your Email';
        try {
            $pm = PersistenceManager::getConnection();
            $query = $pm->getQueryBuilder('HealthCare\Site\Doctor');
            $sql = 'select * from ' . Doctor::GetDSN() . '_active where username = ?';
            $values = array($username);
            $logins = $query->executeQuery($sql, $values, 0, 1);
            if ($logins->count() > 0 && $logins[0]->getValue('status') > 0 && $logins[0]->getValue('status') < 6) {
                $doctor_password_recovery = new DoctorPasswordRecovery();
                $doctor_password_recovery->setValue('doctor_id', $logins[0]->getValue('id'));
                $doctor_password_recovery->setValue('hash', md5(date('H:i:s') . $username . $doctor_password_recovery->getValue('id')));
                $pm->beginTransaction();
                $pm->save($doctor_password_recovery);
                $logins[0]->setValue('status', 5);
                $pm->saveWithAudit($logins[0], '');
                $pm->commit();
                $profile = $pm->getObjectById('HealthCare\Util\Profile', $logins[0]->getValue('profile_id'));
                $values['subject'] = "Password Recovery Request";
                $values['url'] = "password-recovery-request";
                $values['email'] = $logins[0]->getValue('username');
                $values['placeholder']['${password_request}'] = "<a href=\"" . CONTEXT_PATH . "/doctor-complete-recovery/" . $doctor_password_recovery->getValue('id') . "/" . $doctor_password_recovery->getValue("hash") . "\">" . CONTEXT_PATH . "/doctor-complete-recovery/" . $doctor_password_recovery->getValue('id') . "/" . $doctor_password_recovery->getValue("hash") . "</a>";
                $values['placeholder']['${site_name}'] = SITE_NAME;
                $values['placeholder']['${billing_email}'] = BILLING_EMAIL;
                $values['placeholder']['${support_email}'] = SUPPORT_EMAIL;
                $values['placeholder']['${name}'] = 'Dr ' . $profile->getValue('firstname') . ' ' . $profile->getValue('lastname');
                EmailNotificationService::Sendmail($values);
                $result['error-msg'] = 'Follow the instructions sent to your Email';
            } else if ($logins->count() > 0 && $logins[0]->getValue('status') == 6) {
                $result['error-msg'] = 'Account Disabled';
            } else if ($logins->count() > 0 && $logins[0]->getValue('status') == 0) {
                $result['error-msg'] = 'Email verification required. Click the link sent to your Email';
            } else {
                $result['error-msg'] = 'Account not found';
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
            $result['error-msg'] = 'An error occurred';
        }
        return $result;
    }

    public static function CompleteRecovery($values, $redirect) {
        try {
            $pm = PersistenceManager::getConnection();
            $doctor_password_recovery = $pm->getObjectById('HealthCare\Site\DoctorPasswordRecovery', $values['id']);
            if ($doctor_password_recovery != null && $doctor_password_recovery->getValue('status') == 0 && strcmp($values['hash'], $doctor_password_recovery->getValue('hash')) == 0) {
                $doctor_password_recovery->setValue('status', 1);
                $doctor = $pm->getObjectById('HealthCare\Site\Doctor', $doctor_password_recovery->getValue('doctor_id'));
                $doctor->setValue('status', 1);
                $pm->beginTransaction();
                $pm->save($doctor_password_recovery);
                $pm->save($doctor);
                $pm->commit();
                $_SESSION['IDoctor'] = array('id' => $doctor->getValue('id'), 'username' => $doctor->getValue('username'), 'profile_id' => $doctor->getValue('profile_id'));
                self::Redirect($redirect);
            } else {
                echo "<h2>Failed</h2>";
                exit;
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }
    }

    public static function ChangePassword($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'password');
            $assertProperties->addProperty($values, 'password2');
            $assertProperties->assert();

            if (strcmp($values['password'], $values['password2']) != 0) {
                throw new Exception('1013');
            }

            $pm = PersistenceManager::getConnection();
            $doctor = $pm->getObjectById('HealthCare\Site\Doctor', $_SESSION['IDoctor']['id']);

            $doctor->setValue('confirm_username', $doctor->getValue('username'));
            $doctor->setValue('password', $values['password']);

            $doctor->CheckValidationRules($pm);

            $result['doctor'] = $doctor;

            if (count($doctor->ValidationErrors) == 0) {
                $pm->beginTransaction();
                $doctor->setValue('password', md5($doctor->getValue('id') . $values['password'] . strtolower($doctor->getValue('username'))));
                $pm->saveWithAudit($doctor, '');
                $pm->commit();
                $result['status'] = 1;
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            if (strpos($e, '1013') != false) {
                $result['error-msg'] = "New Password and Re-Password are not the same";
            } else {
                ExceptionManager::RaiseException($e);
            }
        }

        return $result;
    }

    public static function Redirect($redirect) {
        $str = '';
        $query_string = filter_input(INPUT_SERVER, 'QUERY_STRING', FILTER_SANITIZE_STRING);
        if (!empty($query_string)) {
            $str .= '?' . $query_string;
        }
        $pos = strpos($str, 'return=');
        if ($pos) {
            header('Location: ' . substr($str, $pos + 7));
            exit;
        } else {
            header('Location: ' . $redirect);
            exit;
        }
    }

    public static function RequireLogin($url) {
        if (!self::IsAuthenticated()) {
            self::RedirectBack($url);
        }
    }

    public static function GetIPrincipal() {
        if (isset($_SESSION['IDoctor'])) {
            return $_SESSION['IDoctor'];
        }
        return false;
    }

    public static function IsAuthenticated() {
        try {
            if (isset($_SESSION['IDoctor'])) {
                return true;
            }
            return false;
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }
    }

    private static function RedirectBack($url) {
        $redirect = 'http';
        $query_string = filter_input(INPUT_SERVER, 'QUERY_STRING', FILTER_SANITIZE_STRING);
        $https = filter_input(INPUT_SERVER, 'HTTPS', FILTER_SANITIZE_STRING);
        $http_host = filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING);
        if (isset($https) && $https == 'on') {
            $redirect . 's';
        }
        $redirect .= '://' . $http_host . REQUEST_URI;
        if (!empty($query_string)) {
            $redirect .= '?' . $query_string;
        }
        header('Location: ' . $url . '?return=' . $redirect);
        exit;
    }

    public static function DeleteDoctor($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            //assert account properites
            $assertProperties->addProperty($values, 'id');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $page = $pm->getObjectById('HealthCare\Site\Doctor', $values['id']);
            if ($page != null) {
                if (strcmp($page->getValue('level'), "System") == 0) {
                    $result['error-msg'] = 'Cannot Delete System Doctor';
                } else {
                    $page->setValue('deleted', 1);
                    $page->setValue('delete_timestamp', date('Y-m-d H:i:s'));
                    $pm->beginTransaction();
                    $pm->saveWithAudit($page, $_SESSION['IPrincipal']['id']);
                    $pm->commit();
                    $result['status'] = 1;
                }
            } else {
                $result['error-msg'] = 'Doctor not found';
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }
        return $result;
    }

}
