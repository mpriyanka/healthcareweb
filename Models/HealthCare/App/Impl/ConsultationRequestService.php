<?php

namespace HealthCare\App\Impl;

use Exception;
use HealthCare\App\ConsultationRequest;
use HealthCare\Dao\Exception\ExceptionManager;
use HealthCare\Dao\PersistenceManager;
use HealthCare\Util\AssertProperties;
use HealthCare\Util\Date;
use HealthCare\Util\Util;

class ConsultationRequestService {

    public static function CreateConsultationRequestWS($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'doctor_id');
            $assertProperties->addProperty($values, 'message');
            $assertProperties->addProperty($values, 'phone');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $app_user = $pm->getObjectByColumn("HealthCare\App\AppUser", "phone", $values['phone']);

            $doctor = $pm->getObjectById("HealthCare\Site\Doctor", $values['doctor_id']);

            if ($app_user == null) {
                throw new Exception("404");
            }

            if ($doctor == null) {
                throw new Exception("4041");
            }

            if ($doctor->getValue('deleted') || $doctor->getValue('status') == 6) {
                throw new Exception("4041");
            }

            $consultation = $pm->getObjectByColumns("HealthCare\App\Consultation", array('user_id', 'doctor_id', 'end_date'), array($app_user->getValue('id'), $doctor->getValue('id'), 0));

            if ($consultation != null) {
                throw new Exception("505");
            }

            $consultation_request = new ConsultationRequest();
            $consultation_request->setValue('app_user_id', $app_user->getValue('id'));
            $consultation_request->setValue('doctor_id', $values['doctor_id']);
            $consultation_request->setValue('message', $values['message']);
            $consultation_request->setValue('status', "0");

            $pm->beginTransaction();
            $pm->saveWithAudit($consultation_request, '');
            $pm->commit();
            $result['status'] = 1;

            $values = array();
            $values['gcm_tokens'] = array($doctor->getValue('gcm_token'));

            $consultation_request_array = $consultation_request->convertToNameValueArray();
            $consultation_request_array['user_name'] = $app_user->getValue('firstname') . ' ' . $app_user->getValue('lastname');

            $values['message'] = array('type' => 'ConsultationRequest', 'content' => json_encode($consultation_request_array));

            $result_gcm = Util::SendGCM($values);

            if ($result_gcm['success'] > 0) {
                $pm->beginTransaction();
                $consultation_request->setValue('status', 1);
                $pm->saveWithAudit($consultation_request, '');
                $pm->commit();
            }

            $result['consultation-request'] = $consultation_request->convertToNameValueArray();
        } catch (Exception $e) {
            if (strpos($e, '404') != false) {
                $result['status'] = 2;
            } else if (strpos($e, '4041') != false) {
                $result['status'] = 3;
            } else if (strpos($e, '505') != false) {
                $result['status'] = 5;
            } else {
                ExceptionManager::RaiseException($e);
                $result['status'] = 4;
            }
        }

        return $result;
    }

    public static function UpdateAppUserWS($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'firstname');
            $assertProperties->addProperty($values, 'lastname');
            $assertProperties->addProperty($values, 'phone');
            $assertProperties->addProperty($values, 'dob');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $app_user = $pm->getObjectByColumn('HealthCare\App\AppUser', 'phone', $values['phone']);
            if ($app_user == null) {
                throw new Exception("404");
            }

            $app_user->setValue('firstname', $values['firstname']);
            $app_user->setValue('lastname', $values['lastname']);
            $app_user->setValue('dob', $values['dob']);

            $app_user->CheckValidationRules(null);

            if (count($app_user->ValidationErrors) == 0) {
                $app_user->setValue('dob', Date::convertToMySqlDate($app_user->getValue('dob')));
                $pm = PersistenceManager::getConnection();
                $pm->beginTransaction();
                $pm->saveWithAudit($app_user, '');
                $pm->commit();
                $result['status'] = 1;
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }
        return $result;
    }

}
