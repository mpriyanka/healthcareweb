<?php

namespace HealthCare\App\Impl;

use Exception;
use HealthCare\App\AppUser;
use HealthCare\Dao\Exception\ExceptionManager;
use HealthCare\Dao\PersistenceManager;
use HealthCare\Util\AssertProperties;
use HealthCare\Util\Date;

class AppUserService {

    public static function CreateAppUserWS($values) {
        $pm = null;
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'firstname');
            $assertProperties->addProperty($values, 'lastname');
            $assertProperties->addProperty($values, 'phone');
            $assertProperties->addProperty($values, 'dob');
            $assertProperties->assert();
            
            $app_user = new AppUser();
            $app_user->setValue('firstname', $values['firstname']);
            $app_user->setValue('lastname', $values['lastname']);
            $app_user->setValue('dob', $values['dob']);
            $app_user->setValue('status', 1);
            $app_user->setValue('phone', $values['phone']);

            $app_user->CheckValidationRules();

            if (count($app_user->ValidationErrors) == 0) {
                $app_user->setValue('dob', Date::convertToMySqlDate($app_user->getValue('dob')));
                $pm = PersistenceManager::getConnection();
                $pm->beginTransaction();
                $pm->saveWithAudit($app_user, '');
                $pm->commit();
                $result['status'] = 1;
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            if (strpos($e, '1062 Duplicate entry') != false) {
                $pm->rollBack();
                return self::UpdateAppUserWS($values);
            } else {
                ExceptionManager::RaiseException($e);
            }
        }

        return $result;
    }

    public static function UpdateAppUserWS($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'firstname');
            $assertProperties->addProperty($values, 'lastname');
            $assertProperties->addProperty($values, 'phone');
            $assertProperties->addProperty($values, 'dob');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $app_user = $pm->getObjectByColumn('HealthCare\App\AppUser', 'phone', $values['phone']);
            if ($app_user == null) {
                throw new Exception("404");
            }

            $app_user->setValue('firstname', $values['firstname']);
            $app_user->setValue('lastname', $values['lastname']);
            $app_user->setValue('dob', $values['dob']);

            $app_user->CheckValidationRules(null);

            if (count($app_user->ValidationErrors) == 0) {
                $app_user->setValue('dob', Date::convertToMySqlDate($app_user->getValue('dob')));
                $pm = PersistenceManager::getConnection();
                $pm->beginTransaction();
                $pm->saveWithAudit($app_user, '');
                $pm->commit();
                $result['status'] = 1;
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }
        return $result;
    }

}
