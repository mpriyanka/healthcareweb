<?php

namespace HealthCare\App\Impl;

use Exception;
use HealthCare\App\Consultation;
use HealthCare\Dao\Exception\ExceptionManager;
use HealthCare\Dao\PersistenceManager;
use HealthCare\Util\AssertProperties;
use HealthCare\Util\Date;
use HealthCare\Util\Util;

class ConsultationService {

    public static function CreateConsultationWS($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'username');
            $assertProperties->addProperty($values, 'user_id');
            $assertProperties->addProperty($values, 'remote_doctor_id');
            $assertProperties->addProperty($values, 'start_date');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $app_user = $pm->getObjectById("HealthCare\App\AppUser", $values['user_id']);

            $doctor = $pm->getObjectByColumn("HealthCare\Site\Doctor", 'username', $values['username']);

            if ($app_user == null) {
                throw new Exception("404");
            }

            if ($doctor == null) {
                throw new Exception("4041");
            }

            if ($doctor->getValue('deleted') || $doctor->getValue('status') == 6) {
                throw new Exception("4041");
            }

            $consultation = new Consultation();
            $consultation->setValue('user_id', $app_user->getValue('id'));
            $consultation->setValue('doctor_id', $doctor->getValue('id'));
            $consultation->setValue('remote_doctor_id', $values['remote_doctor_id']);
            $consultation->setValue('start_date', $values['start_date']);

            $pm->beginTransaction();
            $pm->saveWithAudit($consultation, '');
            $pm->commit();
            $result['status'] = 1;

            $values = array();
            $values['gcm_tokens'] = array($app_user->getValue('gcm_token'));
            
            $profile = $pm->getObjectById("HealthCare\Util\Profile", $doctor->getValue('profile_id'));
            
            $consultation_array = $consultation->convertToNameValueArray();
            $consultation_array['doctor_name'] = 'Dr. '.$profile->getValue('firstname').' '.$profile->getValue('lastname');
            $consultation_array['notify_message'] = "Your Consultation request has been accepted.";
            
            $values['message'] = array('type' => 'Consultation', 'content' => json_encode($consultation_array));

            /*$result_gcm = Util::SendGCM($values);

            if(!$result_gcm['success'] > 0) {
                
            }*/      
            $result['consultation'] = $consultation->convertToNameValueArray();
        } catch (Exception $e) {
            if (strpos($e, '404') != false) {
                $result['status'] = 2;
            } else if (strpos($e, '4041') != false) {
                $result['status'] = 3;
            } else {
                ExceptionManager::RaiseException($e);
                $result['status'] = 4;
            }
        }

        return $result;
    }

    public static function UpdateAppUserWS($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'firstname');
            $assertProperties->addProperty($values, 'lastname');
            $assertProperties->addProperty($values, 'phone');
            $assertProperties->addProperty($values, 'dob');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $app_user = $pm->getObjectByColumn('HealthCare\App\AppUser', 'phone', $values['phone']);
            if ($app_user == null) {
                throw new Exception("404");
            }

            $app_user->setValue('firstname', $values['firstname']);
            $app_user->setValue('lastname', $values['lastname']);
            $app_user->setValue('dob', $values['dob']);

            $app_user->CheckValidationRules(null);

            if (count($app_user->ValidationErrors) == 0) {
                $app_user->setValue('dob', Date::convertToMySqlDate($app_user->getValue('dob')));
                $pm = PersistenceManager::getConnection();
                $pm->beginTransaction();
                $pm->saveWithAudit($app_user, '');
                $pm->commit();
                $result['status'] = 1;
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }
        return $result;
    }

}
