<?php

namespace HealthCare\App\Impl;

use Exception;
use HealthCare\App\Consultation;
use HealthCare\App\ConsultationChatMessage;
use HealthCare\Dao\Exception\ExceptionManager;
use HealthCare\Dao\PersistenceManager;
use HealthCare\Util\AssertProperties;
use HealthCare\Util\Date;
use HealthCare\Util\Util;

class ConsultationChatMessageService {

    public static function GetConsultationChatMessagesDoctorWS($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'username');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $doctor = $pm->getObjectByColumn("HealthCare\Site\Doctor", 'username', $values['username']);

            if ($doctor == null) {
                throw new Exception("4041");
            }

            if ($doctor->getValue('deleted') || $doctor->getValue('status') == 6) {
                throw new Exception("4041");
            }

            $return_data = array();

            $query = $pm->getQueryBuilder("HealthCare\App\ConsultationChatMessage");
            $sql = "Select id,consultation_id,user_id,tag,text,image_path,creation_date from " . ConsultationChatMessage::GetDSN() . " where doctor_id = ? and _from = ? and sync_required = ? and status = ? order by creation_date";
            $consultationChatMessages = $query->executeQuery($sql, array($doctor->getValue('id'), 'user', 1, 0), 0, -1);

            $pm->beginTransaction();

            $app_user = null;

            for ($i = 0; $i < $consultationChatMessages->count(); $i++) {

                if ($i == 0 || ($i > 0 && strcasecmp($consultationChatMessages[$i]->getValue('user_id'), $consultationChatMessages[$i - 1]->getValue('user_id')) != 0)) {
                    $app_user = $pm->getObjectById("HealthCare\App\AppUser", $consultationChatMessages[$i]->getValue('user_id'));
                }

                $consultationChatMessage = $pm->getObjectById("HealthCare\App\ConsultationChatMessage", $consultationChatMessages[$i]->getValue('id'));
                $consultationChatMessageArray['id'] = $consultationChatMessage->getValue('id');
                $consultationChatMessageArray['consultation_id'] = $consultationChatMessage->getValue('consultation_id');
                $consultationChatMessageArray['user_id'] = $consultationChatMessage->getValue('user_id');
                $consultationChatMessageArray['user_name'] = $app_user->getValue('firstname') . ' ' . $app_user->getValue('lastname');
                $consultationChatMessageArray['tag'] = $consultationChatMessage->getValue('tag');
                $consultationChatMessageArray['text'] = $consultationChatMessage->getValue('text');
                $consultationChatMessageArray['image_path'] = $consultationChatMessage->getValue('image_path');
                $consultationChatMessageArray['creation_date'] = $consultationChatMessage->getValue('creation_date');

                $return_data[] = $consultationChatMessageArray;

                $consultationChatMessage->setValue('status', 1);

                $pm->saveWithAudit($consultationChatMessage, '');
            }

            $pm->commit();

            $result['status'] = 1;
            $result['data'] = $return_data;
        } catch (Exception $e) {
            if (strpos($e, '404') != false) {
                $result['status'] = 4;
            } else if (strpos($e, '4041') != false) {
                $result['status'] = 5;
            } else if (strpos($e, '4042') != false) {
                $result['status'] = 6;
            } else {
                ExceptionManager::RaiseException($e);
                $result['status'] = 7;
            }
        }

        return $result;
    }

    public static function DownloadCCMImageDoctorWS($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'username');
            $assertProperties->addProperty($values, 'remote_id');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $consultationChatMessage = $pm->getObjectById("HealthCare\App\ConsultationChatMessage", $values['remote_id']);

            $image_path = BASE_PATH . '/' . $consultationChatMessage->getValue('image_path');

            if (file_exists($image_path)) {
                $result['image_data'] = base64_encode(file_get_contents($image_path));
                $result['status'] = 1;
            }
        } catch (Exception $e) {
            if (strpos($e, '404') != false) {
                $result['status'] = 4;
            } else if (strpos($e, '4041') != false) {
                $result['status'] = 5;
            } else if (strpos($e, '4042') != false) {
                $result['status'] = 6;
            } else {
                ExceptionManager::RaiseException($e);
                $result['status'] = 7;
            }
        }

        return $result;
    }
    
    public static function DownloadCCMImageUserWS($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'phone');
            $assertProperties->addProperty($values, 'remote_id');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $consultationChatMessage = $pm->getObjectById("HealthCare\App\ConsultationChatMessage", $values['remote_id']);

            $image_path = BASE_PATH . '/' . $consultationChatMessage->getValue('image_path');

            if (file_exists($image_path)) {
                $result['image_data'] = base64_encode(file_get_contents($image_path));
                $result['status'] = 1;
            }
        } catch (Exception $e) {
            if (strpos($e, '404') != false) {
                $result['status'] = 4;
            } else if (strpos($e, '4041') != false) {
                $result['status'] = 5;
            } else if (strpos($e, '4042') != false) {
                $result['status'] = 6;
            } else {
                ExceptionManager::RaiseException($e);
                $result['status'] = 7;
            }
        }

        return $result;
    }

    public static function GetCCMImageDoctorWS($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'username');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $doctor = $pm->getObjectByColumn("HealthCare\Site\Doctor", 'username', $values['username']);

            if ($doctor == null) {
                throw new Exception("4041");
            }

            if ($doctor->getValue('deleted') || $doctor->getValue('status') == 6) {
                throw new Exception("4041");
            }

            $return_data = array();

            $query = $pm->getQueryBuilder("HealthCare\App\ConsultationChatMessage");
            $sql = "Select id from " . ConsultationChatMessage::GetDSN() . " where doctor_id = ? and _from = ? and tag = ? order by creation_date";
            $consultationChatMessages = $query->executeQuery($sql, array($doctor->getValue('id'), 'user', 'image_sent'), 0, -1);

            $pm->beginTransaction();

            for ($i = 0; $i < $consultationChatMessages->count(); $i++) {

                $consultationChatMessage = $pm->getObjectById("HealthCare\App\ConsultationChatMessage", $consultationChatMessages[$i]->getValue('id'));

                $consultationChatMessageArray['id'] = $consultationChatMessage->getValue('id');

                $return_data[] = $consultationChatMessageArray;

                $consultationChatMessage->setValue('tag', 'image_delivered');

                $pm->saveWithAudit($consultationChatMessage, '');
            }

            $pm->commit();

            $result['status'] = 1;
            $result['data'] = $return_data;
        } catch (Exception $e) {
            if (strpos($e, '404') != false) {
                $result['status'] = 4;
            } else if (strpos($e, '4041') != false) {
                $result['status'] = 5;
            } else if (strpos($e, '4042') != false) {
                $result['status'] = 6;
            } else {
                ExceptionManager::RaiseException($e);
                $result['status'] = 7;
            }
        }

        return $result;
    }
    
    public static function GetCCMImageUserWS($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'phone');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $app_user = $pm->getObjectByColumn("HealthCare\App\AppUser", 'phone', $values['phone']);

            if ($app_user == null) {
                throw new Exception("4041");
            }

            $return_data = array();

            $query = $pm->getQueryBuilder("HealthCare\App\ConsultationChatMessage");
            $sql = "Select id from " . ConsultationChatMessage::GetDSN() . " where user_id = ? and _from = ? and tag = ? order by creation_date";
            $consultationChatMessages = $query->executeQuery($sql, array($app_user->getValue('id'), 'doctor', 'image_sent'), 0, -1);

            $pm->beginTransaction();

            for ($i = 0; $i < $consultationChatMessages->count(); $i++) {

                $consultationChatMessage = $pm->getObjectById("HealthCare\App\ConsultationChatMessage", $consultationChatMessages[$i]->getValue('id'));

                $consultationChatMessageArray['id'] = $consultationChatMessage->getValue('id');

                $return_data[] = $consultationChatMessageArray;

                $consultationChatMessage->setValue('tag', 'image_delivered');

                $pm->saveWithAudit($consultationChatMessage, '');
            }

            $pm->commit();

            $result['status'] = 1;
            $result['data'] = $return_data;
        } catch (Exception $e) {
            if (strpos($e, '404') != false) {
                $result['status'] = 4;
            } else if (strpos($e, '4041') != false) {
                $result['status'] = 5;
            } else if (strpos($e, '4042') != false) {
                $result['status'] = 6;
            } else {
                ExceptionManager::RaiseException($e);
                $result['status'] = 7;
            }
        }

        return $result;
    }

    public static function GetConsultationChatMessagesUserWS($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'phone');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $app_user = $pm->getObjectByColumn("HealthCare\App\AppUser", 'phone', $values['phone']);

            if ($app_user == null) {
                throw new Exception("4041");
            }

            $return_data = array();

            $query = $pm->getQueryBuilder("HealthCare\App\ConsultationChatMessage");
            $sql = "Select id,consultation_id,doctor_id,tag,text,image_path,creation_date from " . ConsultationChatMessage::GetDSN() . " where user_id = ? and _from = ? and sync_required = ? and status = ? order by creation_date";
            $consultationChatMessages = $query->executeQuery($sql, array($app_user->getValue('id'), 'doctor', 1, 0), 0, -1);

            $pm->beginTransaction();

            $doctor = null;

            for ($i = 0; $i < $consultationChatMessages->count(); $i++) {

                if ($i == 0 || ($i > 0 && strcasecmp($consultationChatMessages[$i]->getValue('doctor_id'), $consultationChatMessages[$i - 1]->getValue('doctor_id')) != 0)) {
                    $doctor = $pm->getObjectById("HealthCare\Site\Doctor", $consultationChatMessages[$i]->getValue('doctor_id'));
                    $profile = $pm->getObjectById("HealthCare\Util\Profile", $doctor->getValue('profile_id'));
                }

                $consultationChatMessage = $pm->getObjectById("HealthCare\App\ConsultationChatMessage", $consultationChatMessages[$i]->getValue('id'));
                $consultationChatMessageArray['id'] = $consultationChatMessage->getValue('id');
                $consultationChatMessageArray['consultation_id'] = $consultationChatMessage->getValue('consultation_id');
                $consultationChatMessageArray['doctor_id'] = $consultationChatMessage->getValue('doctor_id');
                $consultationChatMessageArray['doctor_name'] = 'Dr. ' . $profile->getValue('firstname') . ' ' . $profile->getValue('lastname');
                $consultationChatMessageArray['tag'] = $consultationChatMessage->getValue('tag');
                $consultationChatMessageArray['text'] = $consultationChatMessage->getValue('text');
                $consultationChatMessageArray['image_path'] = $consultationChatMessage->getValue('image_path');
                $consultationChatMessageArray['creation_date'] = $consultationChatMessage->getValue('creation_date');

                $return_data[] = $consultationChatMessageArray;

                $consultationChatMessage->setValue('status', 1);

                $pm->saveWithAudit($consultationChatMessage, '');
            }

            $pm->commit();

            $result['status'] = 1;
            $result['data'] = $return_data;
        } catch (Exception $e) {
            if (strpos($e, '404') != false) {
                $result['status'] = 4;
            } else if (strpos($e, '4041') != false) {
                $result['status'] = 5;
            } else if (strpos($e, '4042') != false) {
                $result['status'] = 6;
            } else {
                ExceptionManager::RaiseException($e);
                $result['status'] = 7;
            }
        }

        return $result;
    }

    public static function UploadCMMImageUserWS($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'phone');
            $assertProperties->addProperty($values, 'creation_date');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $app_user = $pm->getObjectByColumn("HealthCare\App\AppUser", 'phone', $values['phone']);

            if ($app_user == null) {
                throw new Exception("4041");
            }

            $consultationChatMessage = $pm->getObjectByColumns("HealthCare\App\ConsultationChatMessage", array('user_id', 'creation_date'), array($app_user->getValue('id'), $values['creation_date']));

            if ($consultationChatMessage == null) {
                throw new Exception("4041");
            }

            $path = 'uploads/ccm/image_' . rand(100000, 999999) . rand(100000, 999999) . rand(100000, 999999) . '.jpg';

            $imsrc = base64_decode($values['image_data']);
            $fp = fopen($path, 'w');
            fwrite($fp, $imsrc);
            if (fclose($fp)) {
                $pm->beginTransaction();
                $consultationChatMessage->setValue('tag', 'image_sent');
                $consultationChatMessage->setValue('image_path', $path);
                $pm->saveWithAudit($consultationChatMessage, '');
                $pm->commit();
                $result['status'] = 1;
            }

            if ($result['status'] == 1) {
                $doctor = $pm->getObjectById("HealthCare\Site\Doctor", $consultationChatMessage->getValue('doctor_id'));

                $values = array();
                $values['gcm_tokens'] = array($doctor->getValue('gcm_token'));

                $values['message'] = array('type' => 'RequireSync', 'content' => true);

                Util::SendGCM($values);
            }
        } catch (Exception $e) {
            if (strpos($e, '404') != false) {
                $result['status'] = 4;
            } else if (strpos($e, '4041') != false) {
                $result['status'] = 5;
            } else if (strpos($e, '4042') != false) {
                $result['status'] = 6;
            } else {
                ExceptionManager::RaiseException($e);
                $result['status'] = 7;
            }
        }

        return $result;
    }

    public static function UploadCMMImageDoctorWS($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'username');
            $assertProperties->addProperty($values, 'creation_date');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $doctor = $pm->getObjectByColumn("HealthCare\Site\Doctor", 'username', $values['username']);

            if ($doctor == null) {
                throw new Exception("4041");
            }

            if ($doctor->getValue('deleted') || $doctor->getValue('status') == 6) {
                throw new Exception("4041");
            }

            $consultationChatMessage = $pm->getObjectByColumns("HealthCare\App\ConsultationChatMessage", array('doctor_id', 'creation_date'), array($doctor->getValue('id'), $values['creation_date']));

            if ($consultationChatMessage == null) {
                throw new Exception("4041");
            }

            $path = 'uploads/ccm/image_' . rand(100000, 999999) . rand(100000, 999999) . rand(100000, 999999) . '.jpg';

            $imsrc = base64_decode($values['image_data']);
            $fp = fopen($path, 'w');
            fwrite($fp, $imsrc);
            if (fclose($fp)) {
                $pm->beginTransaction();
                $consultationChatMessage->setValue('tag', 'image_sent');
                $consultationChatMessage->setValue('image_path', $path);
                $pm->saveWithAudit($consultationChatMessage, '');
                $pm->commit();
                $result['status'] = 1;
            }

            if ($result['status'] == 1) {
                $app_user = $pm->getObjectById("HealthCare\App\AppUser", $consultationChatMessage->getValue('user_id'));

                $values = array();
                $values['gcm_tokens'] = array($app_user->getValue('gcm_token'));

                $values['message'] = array('type' => 'RequireSync', 'content' => true);

                Util::SendGCM($values);
            }
        } catch (Exception $e) {
            if (strpos($e, '404') != false) {
                $result['status'] = 4;
            } else if (strpos($e, '4041') != false) {
                $result['status'] = 5;
            } else if (strpos($e, '4042') != false) {
                $result['status'] = 6;
            } else {
                ExceptionManager::RaiseException($e);
                $result['status'] = 7;
            }
        }

        return $result;
    }

    public static function CreateConsultationChatMessageDoctorWS($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'username');
            $assertProperties->addProperty($values, 'data');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $doctor = $pm->getObjectByColumn("HealthCare\Site\Doctor", 'username', $values['username']);

            if ($doctor == null) {
                throw new Exception("4041");
            }

            if ($doctor->getValue('deleted') || $doctor->getValue('status') == 6) {
                throw new Exception("4041");
            }

            $return_data = array();

            $array_gcm = array();

            $array_data = json_decode($values['data'], true);

            $pm->beginTransaction();

            $user_gcm_token = "";

            foreach ($array_data as $consultation_chat_message_value) {

                $app_user = $pm->getObjectById("HealthCare\App\AppUser", $consultation_chat_message_value['user_id']);

                if ($app_user == null) {
                    throw new Exception("404");
                }

                $consultation_chat_message = new ConsultationChatMessage();
                $consultation_chat_message->setValue('consultation_id', $consultation_chat_message_value['consultation_id']);
                $consultation_chat_message->setValue('user_id', $app_user->getValue('id'));
                $consultation_chat_message->setValue('doctor_id', $doctor->getValue('id'));
                $consultation_chat_message->setValue('remote_doctor_id', $consultation_chat_message_value['_id']);
                $consultation_chat_message->setValue('creation_date', $consultation_chat_message_value['creation_date']);
                $consultation_chat_message->setValue('_from', 'doctor');
                $consultation_chat_message->setValue('tag', $consultation_chat_message_value['tag']);
                $consultation_chat_message->setValue('text', $consultation_chat_message_value['_text']);

                if (strcasecmp($consultation_chat_message->getValue('tag'), "start") == 0) {
                    $consultation = new Consultation();
                    $consultation->setValue('user_id', $app_user->getValue('id'));
                    $consultation->setValue('doctor_id', $doctor->getValue('id'));
                    $consultation->setValue('start_date', $consultation_chat_message->getValue('creation_date'));

                    $consultation_chat_message->setValue('consultation_id', $consultation->getValue('id'));
                    $pm->saveWithAudit($consultation, '');
                }

                $return_data[] = array('id' => $consultation_chat_message->getValue('id'), 'consultation_id' => $consultation_chat_message->getValue('consultation_id'));

                $pm->saveWithAudit($consultation_chat_message, '');

                if (strcasecmp($user_gcm_token, $app_user->getValue('gcm_token')) != 0) {

                    $values = array();
                    $values['gcm_tokens'] = array($app_user->getValue('gcm_token'));

                    $values['message'] = array('type' => 'RequireSync', 'content' => true);

                    $array_gcm[] = $values;
                }
            }

            $pm->commit();

            $result['status'] = 1;
            $result['data'] = $return_data;

            foreach ($array_gcm as $gcm) {

                $result_gcm = Util::SendGCM($gcm);

                if (!$result_gcm['success'] > 0) {
                    
                }
            }
        } catch (Exception $e) {
            if (strpos($e, '404') != false) {
                $result['status'] = 4;
            } else if (strpos($e, '4041') != false) {
                $result['status'] = 5;
            } else if (strpos($e, '4042') != false) {
                $result['status'] = 6;
            } else {
                ExceptionManager::RaiseException($e);
                $result['status'] = 7;
            }
        }

        return $result;
    }

    public static function CreateConsultationChatMessageUserWS($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'phone');
            $assertProperties->addProperty($values, 'data');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $app_user = $pm->getObjectByColumn("HealthCare\App\AppUser", 'phone', $values['phone']);

            if ($app_user == null) {
                throw new Exception("404");
            }

            $return_data = array();

            $array_gcm = array();

            $array_data = json_decode($values['data'], true);

            $pm->beginTransaction();

            $doctor_gcm_token = "";

            foreach ($array_data as $consultation_chat_message_value) {

                $doctor = $pm->getObjectById("HealthCare\Site\Doctor", $consultation_chat_message_value['doctor_id']);

                if ($doctor == null) {
                    throw new Exception("4041");
                }

                if ($doctor->getValue('deleted') || $doctor->getValue('status') == 6) {
                    throw new Exception("4041");
                }
                
                if(strcasecmp($consultation_chat_message_value['tag'], "end") == 0){
                    $consultation = $pm->getObjectById("HealthCare\App\Consultation", $consultation_chat_message_value['consultation_id']);
                    $consultation->setValue('end_date', $consultation_chat_message_value['creation_date']);
                    $consultation->setValue('status', 2);
                    $pm->saveWithAudit($consultation, '');
                    
                    if(strlen($consultation_chat_message_value['_text']) > 0){
                        $doctor->setValue('ratings', ($doctor->getValue('ratings') + $consultation_chat_message_value['_text']));
                        $doctor->setValue('ratings_count', ($doctor->getValue('ratings_count') + 1));
                        $pm->saveWithAudit($doctor, '');
                    }
                }

                $consultation_chat_message = new ConsultationChatMessage();
                $consultation_chat_message->setValue('consultation_id', $consultation_chat_message_value['consultation_id']);
                $consultation_chat_message->setValue('user_id', $app_user->getValue('id'));
                $consultation_chat_message->setValue('doctor_id', $doctor->getValue('id'));
                $consultation_chat_message->setValue('remote_user_id', $consultation_chat_message_value['_id']);
                $consultation_chat_message->setValue('creation_date', $consultation_chat_message_value['creation_date']);
                $consultation_chat_message->setValue('_from', 'user');
                $consultation_chat_message->setValue('tag', $consultation_chat_message_value['tag']);
                $consultation_chat_message->setValue('text', $consultation_chat_message_value['_text']);

                $return_data[] = array('id' => $consultation_chat_message->getValue('id'), 'consultation_id' => $consultation_chat_message->getValue('consultation_id'));

                $pm->saveWithAudit($consultation_chat_message, '');

                if (strcasecmp($doctor_gcm_token, $doctor->getValue('gcm_token')) != 0) {

                    $values = array();
                    $values['gcm_tokens'] = array($doctor->getValue('gcm_token'));

                    $values['message'] = array('type' => 'RequireSync', 'content' => true);

                    $array_gcm[] = $values;

                    $doctor_gcm_token = $doctor->getValue('gcm_token');
                }
            }

            $pm->commit();

            $result['status'] = 1;
            $result['data'] = $return_data;

            foreach ($array_gcm as $gcm) {

                $result_gcm = Util::SendGCM($gcm);

                if (!$result_gcm['success'] > 0) {
                    
                }
            }
        } catch (Exception $e) {
            if (strpos($e, '404') != false) {
                $result['status'] = 4;
            } else if (strpos($e, '4041') != false) {
                $result['status'] = 5;
            } else if (strpos($e, '4042') != false) {
                $result['status'] = 6;
            } else {
                ExceptionManager::RaiseException($e);
                $result['status'] = 7;
            }
        }

        return $result;
    }

    public static function UpdateAppUserWS($values) {
        $result['status'] = 0;
        $result['error-msg'] = '';
        try {
            $assertProperties = new AssertProperties();
            $assertProperties->addProperty($values, 'firstname');
            $assertProperties->addProperty($values, 'lastname');
            $assertProperties->addProperty($values, 'phone');
            $assertProperties->addProperty($values, 'dob');
            $assertProperties->assert();

            $pm = PersistenceManager::getConnection();

            $app_user = $pm->getObjectByColumn('HealthCare\App\AppUser', 'phone', $values['phone']);
            if ($app_user == null) {
                throw new Exception("404");
            }

            $app_user->setValue('firstname', $values['firstname']);
            $app_user->setValue('lastname', $values['lastname']);
            $app_user->setValue('dob', $values['dob']);

            $app_user->CheckValidationRules(null);

            if (count($app_user->ValidationErrors) == 0) {
                $app_user->setValue('dob', Date::convertToMySqlDate($app_user->getValue('dob')));
                $pm = PersistenceManager::getConnection();
                $pm->beginTransaction();
                $pm->saveWithAudit($app_user, '');
                $pm->commit();
                $result['status'] = 1;
            } else {
                $result['status'] = 0;
                $result['error-msg'] = 'Validation Failed';
            }
        } catch (Exception $e) {
            ExceptionManager::RaiseException($e);
        }
        return $result;
    }

}
