<?php

namespace HealthCare\App;

use HealthCare\Dao\IndexType;
use HealthCare\Dao\PersistableObject;
use HealthCare\Dao\Property;

class ConsultationRequest extends PersistableObject {

    function __construct() {
        //Persistable Data Members
        $this->properties['id'] = new Property('id', str_replace(".", "", uniqid(rand(), true)), true, IndexType::PrimaryKey);
        $this->properties['app_user_id'] = new Property('app_user_id', '', true, IndexType::Normal);
        $this->properties['doctor_id'] = new Property('doctor_id', '', true, IndexType::Normal);
        $this->properties['message'] = new Property('message', '', true, IndexType::Normal);
        $this->properties['status'] = new Property('status', '', true, IndexType::Normal);
        $this->properties['deleted'] = new Property('deleted', 0, true, IndexType::Normal);
        $this->properties['delete_timestamp'] = new Property('delete_timestamp', '0000-00-00 00:00:00', true, IndexType::Nullable);
        $this->properties['creation_date'] = new Property('creation_date', date('Y-m-d H:i:s'), true, IndexType::Normal);

        //Non-Persistable Data Members
        $this->properties['last_changed'] = new Property('last_changed', '', false, IndexType::Timestamp);

        //ValidationRules

        //TrackedPropertyChanges
        $this->TrackedPropertyChanges[] = 'status';
        $this->TrackedPropertyChanges[] = 'deleted';
        $this->TrackedPropertyChanges[] = 'delete_timestamp';
    }

    public static function GetDSN() {
        return DB_NAME . '.consultation_requests';
    }

}
