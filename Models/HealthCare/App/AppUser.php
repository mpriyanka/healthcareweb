<?php

namespace HealthCare\App;

use HealthCare\Dao\IndexType;
use HealthCare\Dao\PersistableObject;
use HealthCare\Dao\Property;
use HealthCare\Dao\Validation\DateValidationRule;
use HealthCare\Dao\Validation\StringValidationRule;
use HealthCare\Dao\Validation\ValidationRules;

class AppUser extends PersistableObject {

    function __construct() {
        //Persistable Data Members
        $this->properties['id'] = new Property('id', str_replace(".", "", uniqid(rand(), true)), true, IndexType::PrimaryKey);
        $this->properties['phone'] = new Property('phone', '', true, IndexType::Normal);
        $this->properties['firstname'] = new Property('firstname', '', true, IndexType::Normal);
        $this->properties['lastname'] = new Property('lastname', '', true, IndexType::Normal);
        $this->properties['dob'] = new Property('dob', '', true, IndexType::Normal);
        $this->properties['gcm_token'] = new Property('gcm_token', '', true, IndexType::Nullable);
        $this->properties['status'] = new Property('status', 0, true, IndexType::Normal);
        $this->properties['deleted'] = new Property('deleted', 0, true, IndexType::Normal);
        $this->properties['delete_timestamp'] = new Property('delete_timestamp', '0000-00-00 00:00:00', true, IndexType::Nullable);
        $this->properties['creation_date'] = new Property('creation_date', date('Y-m-d H:i:s'), true, IndexType::Normal);

        //Non-Persistable Data Members
        $this->properties['last_changed'] = new Property('last_changed', '', false, IndexType::Timestamp);

        //ValidationRules
        $this->ValidationRules = new ValidationRules();
        $this->ValidationRules->add(new StringValidationRule('firstname', 3, 40, 'firstname'));
        $this->ValidationRules->add(new DateValidationRule('dob', false, 'dob'));
        $this->ValidationRules->add(new StringValidationRule('lastname', 3, 40, 'lastname'));

        //TrackedPropertyChanges
        $this->TrackedPropertyChanges[] = 'phone';
        $this->TrackedPropertyChanges[] = 'firstname';
        $this->TrackedPropertyChanges[] = 'lastname';
        $this->TrackedPropertyChanges[] = 'dob';
        $this->TrackedPropertyChanges[] = 'gcm_token';
        $this->TrackedPropertyChanges[] = 'status';
        $this->TrackedPropertyChanges[] = 'deleted';
        $this->TrackedPropertyChanges[] = 'delete_timestamp';
    }

    public static function GetDSN() {
        return DB_NAME . '.app_users';
    }

}
