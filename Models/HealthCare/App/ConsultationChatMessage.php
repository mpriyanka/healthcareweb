<?php

namespace HealthCare\App;

use HealthCare\Dao\IndexType;
use HealthCare\Dao\PersistableObject;
use HealthCare\Dao\Property;

class ConsultationChatMessage extends PersistableObject {

    function __construct() {
        //Persistable Data Members
        $this->properties['id'] = new Property('id', str_replace(".", "", uniqid(rand(), true)), true, IndexType::PrimaryKey);
        $this->properties['consultation_id'] = new Property('consultation_id', '', true, IndexType::Normal);
        $this->properties['user_id'] = new Property('user_id', '', true, IndexType::Normal);
        $this->properties['doctor_id'] = new Property('doctor_id', '', true, IndexType::Normal);
        $this->properties['remote_user_id'] = new Property('remote_user_id', '', true, IndexType::Nullable);
        $this->properties['remote_doctor_id'] = new Property('remote_doctor_id', '', true, IndexType::Nullable);
        $this->properties['_from'] = new Property('_from', '', true, IndexType::Normal);
        $this->properties['tag'] = new Property('tag', '', true, IndexType::Normal);
        $this->properties['text'] = new Property('text', '', true, IndexType::Normal);
        $this->properties['image_path'] = new Property('image_path', '', true, IndexType::Normal);
        $this->properties['sync_required'] = new Property('sync_required', 1, true, IndexType::Normal);
        $this->properties['status'] = new Property('status', 0, true, IndexType::Normal);
        $this->properties['creation_date'] = new Property('creation_date', '', true, IndexType::Normal);

        //Non-Persistable Data Members
        $this->properties['last_changed'] = new Property('last_changed', '', false, IndexType::Timestamp);

        //ValidationRules

        //TrackedPropertyChanges
        $this->TrackedPropertyChanges[] = 'status';
        $this->TrackedPropertyChanges[] = 'sync_required';
    }

    public static function GetDSN() {
        return DB_NAME . '.consultation_chat_messages';
    }

}
