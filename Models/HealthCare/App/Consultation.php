<?php

namespace HealthCare\App;

use HealthCare\Dao\IndexType;
use HealthCare\Dao\PersistableObject;
use HealthCare\Dao\Property;

class Consultation extends PersistableObject {

    function __construct() {
        //Persistable Data Members
        $this->properties['id'] = new Property('id', str_replace(".", "", uniqid(rand(), true)), true, IndexType::PrimaryKey);
        $this->properties['user_id'] = new Property('user_id', '', true, IndexType::Normal);
        $this->properties['doctor_id'] = new Property('doctor_id', '', true, IndexType::Normal);
        $this->properties['start_date'] = new Property('start_date', '', true, IndexType::Normal);
        $this->properties['end_date'] = new Property('end_date', '0', true, IndexType::Normal);
        $this->properties['status'] = new Property('status', 1, true, IndexType::Normal);
        $this->properties['creation_date'] = new Property('creation_date', date('Y-m-d H:i:s'), true, IndexType::Normal);

        //Non-Persistable Data Members
        $this->properties['last_changed'] = new Property('last_changed', '', false, IndexType::Timestamp);

        //ValidationRules

        //TrackedPropertyChanges
        $this->TrackedPropertyChanges[] = 'status';
        $this->TrackedPropertyChanges[] = 'end_date';
    }

    public static function GetDSN() {
        return DB_NAME . '.consultations';
    }

}
