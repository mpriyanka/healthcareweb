<?php

namespace HealthCare\Dao\Exception;

use Exception;
use PHPMailer;

class ExceptionManager {

    public static function RaiseException($e) {
        if (DEBUG_MODE) {
            throw new Exception($e->getMessage());
        } else {
            $mail = new PHPMailer;

            $mail->IsSMTP();

            $mail->SMTPAuth = true;                  // enable SMTP authentication

            $mail->Host = SMTP_HOST;

            $mail->SMTPSecure = "ssl"; // sets the SMTP server

            $mail->Port = SMTP_PORT;                    // set the SMTP port for the GMAIL server

            $mail->Username = SMTP_USERNAME; // SMTP account username

            $mail->Password = SMTP_PASSWORD;

            $mail->From = SMTP_USERNAME;
            $mail->FromName = SITE_NAME;  // Add a recipient
            
            $recipients = explode(',', NOTIFICATION_EMAIL);
            
            for($i = 0; $i < count($recipients); $i++){
                $mail->AddAddress($recipients[$i]); 
            }
            
                          // Name is optional

            $mail->IsHTML(true);
            // Set email format to HTML

            $mail->Subject = "#Exception";
            $body = "URL: " .$_SERVER['REQUEST_URI'];
            $body = "Message: ".$e->getMessage()."<br/><br/>";
            $body .= "Line: ".$e->getLine()."<br/><br/>";
            $body .= "File: ".$e->getFile()."<br/><br/>";
            $body .= "Trace: ".$e->getTraceAsString()."<br/><br/>";
            $mail->MsgHTML("<html><head></head><body>" . $body . "</body></html>");

            $mail->Send();
        }
    }

}
