<?php

namespace HealthCare\Dao\Exception;

use Exception;

class StateValueException extends Exception{
    
    private $state_value = "";

    public function __construct($state_value, $message) {
        $this->state_value = $state_value;
        parent::__construct($message, null, null);
    }
    
    public function getStateValue(){
        return $this->state_value;
    }

}
