<?php
namespace HealthCare\Dao;

use HealthCare\Dao\IndexType;

class Property {

    private $name;
    private $value;
    public $IsSavable = true;
    public $IndexType = IndexType::Normal;

    public function getName() {
        return $this->name;
    }

    public function getValue() {
        return $this->value;
    }

    public function setValue($value) {
        $this->value = $value;
    }

    public function __construct($name, $value, $IsSavable, $indexType) {
        $this->name = $name;
        $this->value = $value;
        $this->IsSavable = $IsSavable;
        $this->IndexType = $indexType;
    }

}
