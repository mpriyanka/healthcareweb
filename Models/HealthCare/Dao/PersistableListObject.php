<?php
namespace HealthCare\Dao;

use HealthCare\Dao\ListObject;

class PersistableListObject extends ListObject {

    public function __construct($st, $classname) {
        if ($st != null) {
            while (($row = $st->fetch())) {
                $value = new $classname;
                $value->get($row);
                $this->add($value);
            }
        }
    }

}
