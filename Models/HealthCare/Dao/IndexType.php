<?php
namespace HealthCare\Dao;

class IndexType {

    const PrimaryKey = 'PrimaryKey';
    const Normal = 'Normal';
    const Nullable = 'Nullable';
    const Timestamp = 'Timestamp';

}

