<?php

namespace HealthCare\Dao;

use HealthCare\Dao\IndexType;
use HealthCare\Dao\Property;
use Exception;

class PersistableObject {

    protected $properties = array();
    public $IsDirty = false;
    public $IsNew = true;
    public $IsDeleted = false;
    protected $ValidationRules;
    public $ValidationErrors = array();
    public $PropertyChanges = array();
    public $TrackedPropertyChanges = array();

    public function get($row) {
        if (is_array($row)) {
            foreach ($row as $property => $value) {
                if (isset($this->properties[$property])) {
                    $this->properties[$property]->setValue($value);
                } else {
                    $this->properties[$property] = new Property($property, $value, false, IndexType::Normal);
                }
            }
            $this->markOld();
        }
    }

    public function convertToNameValueArray() {
        $name_value_array = array();
        foreach ($this->properties as $property) {
            $name_value_array[$property->getName()] = $property->getValue();
        }
        return $name_value_array;
    }

    public function markOld() {
        $this->IsDeleted = false;
        $this->IsDirty = false;
        $this->IsNew = false;
    }

    public function getValue($property) {
        if (!isset($this->properties[$property])) {
            throw new Exception('Unknown Property: ' . $property);
        }
        return $this->properties[$property]->getValue();
    }

    public function setValue($property, $value) {
        if (!isset($this->properties[$property])) {
            throw new Exception('Unknown Property: ' . $property);
        }
        $oldValue = $this->properties[$property]->getValue();
        if ($oldValue != $value) {
            $this->properties[$property]->setValue($value);
            $this->IsDirty = true;
            if (in_array($property, $this->TrackedPropertyChanges)) {
                $this->PropertyChanges[$property] = array('old_value' => $oldValue, 'new_value' => $value);
            }
        }
    }

    public function getProperties() {
        return $this->properties;
    }

    public static function GetPropertyKeys($class_name) {
        $keys = array();
        $instance = new $class_name;
        $properties = $instance->getProperties();
        foreach ($properties as $key => $value) {
            $keys[] = $key;
        }
        return $keys;
    }

    public function getPrimaryKey() {
        foreach ($this->properties as $property) {
            if ($property->IndexType == IndexType::PrimaryKey) {
                return $property;
            }
        }
    }

    public function getPrimaryKey2() {
        foreach ($this->properties as $property) {
            if ($property->IndexType == IndexType::PrimaryKey) {
                return $property;
            }
        }
    }

    public function getTimestamp() {
        foreach ($this->properties as $property) {
            if ($property->IndexType == IndexType::Timestamp) {
                return $property;
            }
        }
    }

    public function CheckValidationRules($pm = null) {
        if ($this->ValidationRules != null) {
            $this->ValidationRules->validate($pm, $this);
        }
    }

    public static function CanCreate() {
        return true;
    }

    public static function CanGet() {
        return true;
    }

    public static function CanDelete() {
        return true;
    }

    public static function CanUpdate() {
        return true;
    }

    public function validationReport($property) {
        if (isset($this->ValidationErrors[$property])) {
            return $this->ValidationErrors[$property];
        } else {
            return '';
        }
    }

}
