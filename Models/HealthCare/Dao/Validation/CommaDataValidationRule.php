<?php

namespace HealthCare\Dao\Validation;

use HealthCare\Dao\Validation\ValidationRule;

class CommaDataValidationRule extends ValidationRule {

    protected $property;
    private $min;
    private $max;
    protected $error;

    function __construct($property, $min, $max, $error) {
        $this->property = $property;
        $this->min = $min;
        $this->max = $max;
        $this->error = $error;
    }

    public function validate($pm, $object) {
        $value = $object->getValue($this->property);
        $count = count(explode(",", $value));
        if (strlen($value) < $this->min || $count < $this->min || $count > $this->max) {
            return array('property' => $this->property, 'error' => $this->error);
        } else {
            return true;
        }
    }

}

