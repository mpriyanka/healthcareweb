<?php

namespace HealthCare\Dao\Validation;

use HealthCare\Dao\Validation\ValidationRule;

class CanonicalPathValidationRule extends ValidationRule {

    protected $property;
    private $min;
    private $max;
    protected $error;

    function __construct($property, $min, $max, $error) {
        $this->property = $property;
        $this->min = $min;
        $this->max = $max;
        $this->error = $error;
    }

    public function validate($pm, $object) {
        $value = $object->getValue($this->property);
        if(!preg_match("/^[a-zA-Z0-9-]+$/", $value)){
            return array('property' => $this->property, 'error' => $this->error);
        }
        else if (strlen($value) < $this->min || strlen($value) > $this->max) {
            return array('property' => $this->property, 'error' => $this->error);
        } else {
            return true;
        }
    }

}
