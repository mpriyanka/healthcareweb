<?php

namespace HealthCare\Dao\Validation;

use HealthCare\Dao\Validation\ValidationRule;

class IllegalValueValidationRule extends ValidationRule {

    protected $property;
    protected $illegalValue;
    protected $error;

    function __construct($property, $illegalValue, $error) {
        $this->property = $property;
        $this->illegalValue = $illegalValue;
        $this->error = $error;
    }

    public function validate($pm, $object) {
        $value = $object->getValue($this->property);
        if ($value == $this->illegalValue) {
            return array('property' => $this->property, 'error' => $this->error);
        } else {
            return true;
        }
    }

}

