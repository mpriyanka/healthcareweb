<?php

namespace HealthCare\Dao\Validation;

use HealthCare\Dao\Validation\StringValidationRule;

class EmailValidationRule extends StringValidationRule {

    protected $property;
    protected $allowNull;
    protected $error;

    function __construct($property, $allowNull, $error) {
        $this->property = $property;
        $this->allowNull = $allowNull;
        $this->error = $error;
    }

    public function validate($pm, $object) {
        if ($this->allowNull) {
            if (strlen($object->getValue($this->property)) > 0 && !preg_match('/^([*+!.&#$¦\'\\%\/0-9a-z^_`{}=?~:-]+)@(([0-9a-z-]+\.)+[0-9a-z]{2,4})$/i', $object->getValue($this->property))) {
                return array('property' => $this->property, 'error' => $this->error);
            }
        }
        else {
            if (!preg_match('/^([*+!.&#$¦\'\\%\/0-9a-z^_`{}=?~:-]+)@(([0-9a-z-]+\.)+[0-9a-z]{2,4})$/i', $object->getValue($this->property))) {
                return array('property' => $this->property, 'error' => $this->error);
            }
        }
        return true;
    }

}

