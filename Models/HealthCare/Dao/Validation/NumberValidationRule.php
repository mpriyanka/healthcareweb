<?php

namespace HealthCare\Dao\Validation;

use HealthCare\Dao\Validation\ValidationRule;

class NumberValidationRule extends ValidationRule {

    protected $property;
    private $min;
    private $max;
    protected $error;

    function __construct($property, $min, $max, $error) {
        $this->property = $property;
        $this->min = $min;
        $this->max = $max;
        $this->error = $error;
    }

    public function validate($pm, $object) {
        $value = $object->getValue($this->property);
        if (is_numeric($value)) {
            if (($this->min != null && $value < $this->min) || ($this->max > 0 && $value > $this->max)) {
                return array('property' => $this->property, 'error' => $this->error);
            }
        } else {
            return array('property' => $this->property, 'error' => $this->error);
        }
    }

}

