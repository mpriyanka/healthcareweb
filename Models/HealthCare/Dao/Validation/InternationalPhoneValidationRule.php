<?php

namespace HealthCare\Dao\Validation;

use HealthCare\Dao\Validation\ValidationRule;

class InternationalPhoneValidationRule extends ValidationRule {

    protected $property;
    protected $allowNull;
    protected $error;

    function __construct($property, $allowNull, $error) {
        $this->property = $property;
        $this->allowNull = $allowNull;
        $this->error = $error;
    }

    public function validate($pm, $object) {
        $value = $object->getValue($this->property);
        if ($this->allowNull) {
            if (strlen($object->getValue($this->property)) > 0) {
                if (!is_numeric($value) || strlen($value) < 8 || strlen($value) > 15) {
                    return array('property' => $this->property, 'error' => $this->error);
                }
            }
        } else {
            if (!is_numeric($value) || strlen($value) < 8 || strlen($value) > 15) {
                return array('property' => $this->property, 'error' => $this->error);
            }
        }
        return true;
    }

}
