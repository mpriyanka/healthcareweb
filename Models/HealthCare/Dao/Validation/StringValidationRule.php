<?php

namespace HealthCare\Dao\Validation;

use HealthCare\Dao\Validation\ValidationRule;

class StringValidationRule extends ValidationRule {

    protected $property;
    private $min;
    private $max;
    protected $error;

    function __construct($property, $min, $max, $error) {
        $this->property = $property;
        $this->min = $min;
        $this->max = $max;
        $this->error = $error;
    }

    public function validate($pm, $object) {
        $value = $object->getValue($this->property);
        if (strlen($value) < $this->min || (strlen($value) > $this->max && $this->max > 0)) {
            return array('property' => $this->property, 'error' => $this->error);
        } else {
            return true;
        }
    }

}
