<?php
namespace HealthCare\Dao\Validation;

use HealthCare\Dao\ListObject;
use ReflectionClass;

class ValidationRules extends ListObject {

    public function validate($pm, $object) {
        $ERROR_CODES = json_decode(ERROR_CODES, true);
        $ref_obj = new ReflectionClass($object);
        $class_name = $ref_obj->getShortName();
        for ($i = 0; $i < $this->count(); $i++) {
            $result = $this->elements[$i]->validate($pm, $object);
            if (is_array($result)) {
                $object->ValidationErrors[$result['property']] = $ERROR_CODES["HealthCare"][$class_name][$result['error']];
            }
        }
    }

}
