<?php

namespace HealthCare\Dao\Validation;

use HealthCare\Dao\Validation\ValidationRule;

class PhoneValidationRule extends ValidationRule {

    protected $property;
    protected $allowNull;
    protected $error;

    function __construct($property, $allowNull, $error) {
        $this->property = $property;
        $this->allowNull = $allowNull;
        $this->error = $error;
    }

    public function validate($pm, $object) {
        $value = $object->getValue($this->property);
        if ($this->allowNull) {
            if (strlen($object->getValue($this->property)) > 0) {
                if (!is_numeric($value) || strlen($value) < 10 || strlen($value) > 11 || !substr($value, 0, 1) == '0') {
                    return array('property' => $this->property, 'error' => $this->error);
                }
            }
        }
        else {
            if (!is_numeric($value) || strlen($value) < 10 || strlen($value) > 11 || !substr($value, 0, 1) == '0') {
                return array('property' => $this->property, 'error' => $this->error);
            }
        }
        return true;
    }

}

