<?php
namespace HealthCare\Dao\Validation;

abstract class ValidationRule {

    public abstract function validate($pm, $object);
    
}
