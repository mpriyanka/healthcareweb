<?php

namespace HealthCare\Dao\Validation;

use HealthCare\Dao\Validation\ValidationRule;

class AmountValidationRule extends ValidationRule {

    protected $property;
    protected $error;

    function __construct($property, $error) {
        $this->property = $property;
        $this->error = $error;
    }

    public function validate($pm, $object) {
        $value = $object->getValue($this->property);
        if (!is_numeric($value)) {
            return array('property' => $this->property, 'error' => $this->error);
        }
    }

}
