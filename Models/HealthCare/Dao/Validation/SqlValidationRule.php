<?php

namespace HealthCare\Dao\Validation;

use HealthCare\Dao\Validation\ValidationRule;

class SqlValidationRule extends ValidationRule {

    private $sql;
    private $properties;
    private $boolean;
    private $error;

    function __construct($sql, $properties, $boolean, $error) {
        $this->sql = $sql;
        $this->properties = $properties;
        $this->error = $error;
        $this->boolean = $boolean;
    }

    public function validate($pm, $object) {
        $v = array();
        for ($i = 0; $i < count($this->properties); $i++) {
            $v[$i] = $object->getValue($this->properties[$i]);
        }
        $query = $pm->getQueryBuilder(get_class($object));
        $st = $query->execute($this->sql, $v);

        if (($row = $st->fetch()) != $this->boolean) {
            throw new Exception($this->error);
        }
    }

}

