<?php

namespace HealthCare\Dao\Validation;

use HealthCare\Dao\Validation\StringValidationRule;

class SameValueValidationRule extends StringValidationRule {

    protected $property;
    protected $match;
    protected $error;

    function __construct($property, $match, $error) {
        $this->property = $property;
        $this->match = $match;
        $this->error = $error;
    }

    public function validate($pm, $object) {
        if (strcmp($object->getValue($this->property), $object->getValue($this->match)) != 0) {
            return array('property' => $this->property, 'error' => $this->error);
        }
        return true;
    }

}
