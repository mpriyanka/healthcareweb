<?php

namespace HealthCare\Dao;

use HealthCare\Audit\Audit;
use HealthCare\Audit\AuditLine;
use HealthCare\Dao\IndexType;
use HealthCare\Dao\QueryBuilder;
use Exception;
use PDO;

class PersistenceManager {

    private $db;
    private static $instance;

    private function __construct() {
        try {
            $this->db = new PDO('mysql:host=' . DB_HOST . ';', DB_USER, DB_PASSWORD);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            //convertErrorToException($e, null);
        }
    }

    public function getDB() {
        return $this->db;
    }

    public static function getConnection() {
        if (self::$instance == null) {
            self::$instance = new PersistenceManager();
        }
        return self::$instance;
    }

    public static function NewPersistenceManager() {
        if (self::$instance == null) {
            self::$instance = new PersistenceManager();
        }
        return self::$instance;
    }

    public function beginTransaction() {
        $this->db->beginTransaction();
    }

    public function commit() {
        $this->db->commit();
    }

    public function rollBack() {
        $this->db->rollBack();
    }

    public function getQueryBuilder($classname) {
        return new QueryBuilder($this->db, $classname);
    }

    public function getObjectById($classname, $value) {
        $object = new $classname;
        $sql = 'select * from ' . $object::GetDSN() . ' where ' . $object->getPrimaryKey()->getName() . ' = ?';
        $st = $this->db->prepare($sql);
        $st->execute(array($value));
        if (($row = $st->fetch())) {
            $object->get($row);
            return $object;
        } else {
            return null;
        }
    }

    public function getObjectByColumn($classname, $column, $value) {
        $object = new $classname;
        $sql = 'select * from ' . $object::GetDSN() . ' where ' . $column . ' = ?';
        $st = $this->db->prepare($sql);
        $st->execute(array($value));
        if (($row = $st->fetch())) {
            $object->get($row);
            return $object;
        } else {
            return null;
        }
    }

    public function getObjectByColumns($classname, $columns, $values) {
        $object = new $classname;
        $sql = 'select * from ' . $object::GetDSN() . ' where ' . $columns[0] . ' = ?';
        for ($i = 1; $i < count($columns); $i++) {
            $sql .= ' and ' . $columns[$i] . ' = ?';
        }
        $st = $this->db->prepare($sql);
        $st->execute($values);
        if (($row = $st->fetch())) {
            $object->get($row);
            return $object;
        } else {
            return null;
        }
    }

    public function save($object) {
        ($object->IsNew) ? $this->insert($object) : $this->update($object);
    }

    public function saveWithAudit($object, $operation_by_user_id) {

        $operation = ($object->IsNew) ? "I" : "U";

        $this->save($object);

        if (count($object->PropertyChanges) > 0) {
            $audit = new Audit();
            $audit->setValue('operation', $operation);
            $audit->setValue('primary_key', $object->getValue('id'));
            $audit->setValue('table_name', $object::GetDSN());
            $audit->setValue('operation_by_user_id', $operation_by_user_id);

            $this->save($audit);

            foreach ($object->PropertyChanges as $property => $changes) {
                $audit_line = new AuditLine();
                $audit_line->setValue('audit_id', $audit->getValue('id'));
                $audit_line->setValue('column_name', $property);
                $audit_line->setValue('old_value', $changes['old_value']);
                $audit_line->setValue('new_value', $changes['new_value']);
                $this->save($audit_line);
            }
        }
    }

    private function insert($object) {
        $fields = '';
        $values = '';
        $properties = $object->getProperties();
        $array = array();
        foreach ($properties as $property) {
            if ($property->IsSavable) {
                if ($property->IndexType == IndexType::Nullable && $property->getValue() == null) {
                    
                } else {
                    $fields .= '`'.$property->getName() . '`,';
                    $values .= ':' . $property->getName() . ',';
                    $array[':' . $property->getName()] = $property->getValue();
                }
            }
        }

        $fields = substr($fields, 0, strlen($fields) - 1);
        $values = substr($values, 0, strlen($values) - 1);

        $sql = 'insert into ' . $object::GetDSN() . ' (' . $fields . ') values(' . $values . ')';
        $st = $this->db->prepare($sql);

        $st->execute($array);
        $this->getTimestamp($object);
        $object->markOld();
    }

    private function update($object) {
        $sql = 'update ' . $object::GetDSN() . ' set ';
        $properties = $object->getProperties();
        foreach ($properties as $property) {
            if ($property->IsSavable && $property->IndexType != IndexType::PrimaryKey) {
                if ($property->IndexType == IndexType::Nullable && $property->getValue() == null) {
                    
                } else {
                    $sql .= $property->getName() . ' = :' . $property->getName() . ',';
                }
            }
        }

        $array = array();
        $sql = substr($sql, 0, strlen($sql) - 1);

        $sql .= ' where ' . $object->getPrimarykey()->getName() . ' = :' . $object->getPrimarykey()->getName();
        $sql .= ' and ' . $object->getTimestamp()->getName() . ' = :' . $object->getTimestamp()->getName();

        $st = $this->db->prepare($sql);
        foreach ($properties as $property) {
            if ($property->IsSavable || $property->IndexType == IndexType::Timestamp || $property->IndexType == IndexType::PrimaryKey) {
                if ($property->IndexType == IndexType::Nullable && $property->getValue() == null) {
                    
                } else {
                    $array[':' . $property->getName()] = $property->getValue();
                }
            }
        }
        $st->execute($array);
        $this->getTimestamp($object);
        $object->markOld();
    }

    public function delete($object) {
        $sql = 'delete from ' . $object::GetDSN();
        $sql .= ' where ' . $object->getPrimarykey()->getName() . ' = :' . $object->getPrimarykey()->getName();
        $st = $this->db->prepare($sql);
        $st->execute(array(':' . $object->getPrimaryKey()->getName() => $object->getPrimaryKey()->getValue()));
    }

    public function deleteObjectColumn($classname, $column_name, $id) {
        $object = new $classname;
        $sql = 'delete from ' . $object::GetDSN();
        $sql .= ' where ' . $column_name . ' = :' . $column_name;
        $st = $this->db->prepare($sql);
        $st->execute(array(':' . $column_name => $id));
    }

    public function deleteObjectById($classname, $id) {
        $object = new $classname;
        $sql = 'delete from ' . $object::GetDSN();
        $sql .= ' where ' . $object->getPrimarykey()->getName() . ' = :' . $object->getPrimarykey()->getName();
        $st = $this->db->prepare($sql);
        $st->execute(array(':' . $object->getPrimaryKey()->getName() => $id));
    }

    private function getTimestamp($object) {
        $sql = 'select ' . $object->getTimestamp()->getName() . ' from ' . $object::GetDSN();
        $sql .= ' where ' . $object->getPrimaryKey()->getName() . ' = :' . $object->getPrimaryKey()->getName();

        $st = $this->db->prepare($sql);
        $array = array();
        $array[':' . $object->getPrimaryKey()->getName()] = $object->getPrimaryKey()->getValue();
        $st->execute($array);
        $row = $st->fetch();

        $object->setValue($object->getTimestamp()->getName(), $row[$object->getTimestamp()->getName()]);
    }

    public function close() {
        
    }

}
