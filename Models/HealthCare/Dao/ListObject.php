<?php
namespace HealthCare\Dao;

use ArrayAccess;

abstract class ListObject implements ArrayAccess {

    public $elements;

    public function offsetExists($offset) {
        return isset($this->elements[$offset]);
    }

    public function offsetGet($offset) {
        return $this->elements[$offset];
    }

    public function offsetSet($offset, $value) {
        return $this->elements[$offset] = $value;
    }

    public function offsetUnset($offset) {
        unset($this->elements[$offset]);
    }

    public function count() {
        return count($this->elements);
    }

    public function add($value) {
        return $this->offsetSet($this->count(), $value);
    }

}