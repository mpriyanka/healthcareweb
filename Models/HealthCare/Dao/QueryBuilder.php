<?php

namespace HealthCare\Dao;

use HealthCare\Dao\PersistableListObject;

class QueryBuilder {

    private $db;
    private $classname;

    public function  __construct($db, $classname) {
        $this->db = $db;
        $this->classname = $classname;
    }

    public function executeQuery($sql, $values, $index, $size){
        if($size > 0){
            $st = $this->db->prepare($sql.' limit '.$index.','.$size);
        }
        else{
            $st = $this->db->prepare($sql);
        }
        $st->execute($values);
        return new PersistableListObject($st, $this->classname);
    }

    public function execute($sql, $values){
        $st = $this->db->prepare($sql);
        $st->execute($values);
        return $st;
    }
}