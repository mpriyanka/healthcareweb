<?php

function autoload($class) {
    $class_path = str_replace('\\', '/', __DIR__ . "/$class.php");
    if (is_readable($class_path)) {
        require_once $class_path;
    }
}

spl_autoload_register("autoload");


