<?php
    // Replace with the real server API key from Google APIs
    $apiKey = "AIzaSyC1mI0Iu7n957q8DgDeyrBcWrpxX61bxd0";

    // Replace with the real client registration IDs
    $registrationIDs = array( "f8JTNPDaAkI:APA91bHfR2SIg9VVenzitNWWjO6bBgHCbVD43_T_zaUXiMoUtDXt67G2q4FK19zDVosWM_5z4V2MP_4QdfJ4xIrjss_nVyMGALeZVSg-3tqIZrm4LNoTxarHcnEiL36xTDUiLCqj3_Z6", "cWk8pXa39b8:APA91bGXiYDzGYWpTCs05e1WzGIuyyVfnq5cMXWnCL1kyA5d079iLyKJoMrZUsV85hMZEdZYbjCI1eC_tN7n3_h-HpEwUMiOfti-ect_PH2mBVpIwyYLCyjYLggoAaC60WrdSFqHexj7");

    // Message to be sent
    $message = "Fun with Push Notification2";

    // Set POST variables
    $url = 'https://android.googleapis.com/gcm/send';

    $fields = array(
        'registration_ids' => $registrationIDs,
        'data' => array( "message" => $message ),
    );
    $headers = array(
        'Authorization: key=' . API_KEY,
        'Content-Type: application/json'
    );

    // Open connection
    $ch = curl_init();

    // Set the URL, number of POST vars, POST data
    curl_setopt( $ch, CURLOPT_URL, $url);
    curl_setopt( $ch, CURLOPT_POST, true);
    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
    //curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields));

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    // curl_setopt($ch, CURLOPT_POST, true);
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode( $fields));

    // Execute post
    $result = curl_exec($ch);

    // Close connection
    curl_close($ch);
    echo $result;
    //print_r($result);
    //var_dump($result);