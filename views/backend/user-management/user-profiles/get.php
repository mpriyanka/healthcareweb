<?php

use HealthCare\Util\DataTable;
use HealthCare\Util\Date;

$data_table = new DataTable();
$data_table->SetDefaultOrderByColumn('name');
$data_table->SetColumns($REQUEST_ATTRIBUTES['columns']);

?>
<div class="ats-ui-datatable" id="datatable">
    <div class="toolbar">
        <a class="btn" href="<?php echo CONTEXT_PATH ?>/backend/user-management/user-profiles/create" title="Create">Create</a>
        <?php $data_table->SetFilter() ?>
        <div class="clear"></div>
    </div>
    <div class="datasource">
        <script language="javascript" type="text/javascript">
            var contextmenuurl = new Array();
            contextmenuurl["edit"] = {window: "same", url: "<?php echo CONTEXT_PATH; ?>/backend/user-management/user-profiles/edit/"};
            contextmenuurl["delete"] = {window: "same", url: "<?php echo CONTEXT_PATH; ?>/backend/user-management/user-profiles/delete/"};

            var contextmenu = [
                {title: 'Edit', cmd: 'edit'},
                {title: 'Delete', cmd: 'delete'}
            ];
            
            var filter_columns = jQuery.parseJSON('<?php echo json_encode($data_table->getJavaScriptColumns()) ?>');
        </script>
    </div>
    <div class="scrollpane">
        <div class="rows" style="min-width:1500px;">
            <div class="th">
                <span class="column w120" style="text-align:center"><?php $data_table->getColumnHeader('username') ?></span>
                <span class="column w150" style="text-align:center"><?php $data_table->getColumnHeader('name') ?></span>
                <span class="column w150" style="text-align:center"><?php $data_table->getColumnHeader('phone') ?></span>
                <span class="column w120" style="text-align:center"><?php $data_table->getColumnHeader('email') ?></span>
                <span class="column w300" style="text-align:center"><?php $data_table->getColumnHeader('address') ?></span>
                <span class="column w100" style="text-align:center"><?php $data_table->getColumnHeader('city') ?></span>
                <span class="column w100" style="text-align:center"><?php $data_table->getColumnHeader('state') ?></span>
                <span class="column w100" style="text-align:center"><?php $data_table->getColumnHeader('country') ?></span>
                <span class="column w120" style="text-align:center"><?php $data_table->getColumnHeader('creation_date') ?></span>
                <span class="column w120" style="text-align:center;border:0"><?php $data_table->getColumnHeader('last_changed') ?></span>
            </div>
            <div class=th_fix></div>
            <?php
            $user_profiles = $REQUEST_ATTRIBUTES['user-profiles'];
            for ($i = 0; $i < $user_profiles->count(); $i++) {
                if ($i == ($REQUEST_ATTRIBUTES['pagination']->getSize() - 1)) {
                    $style = "style=\"border:0;\"";
                } else {
                    $style = "";
                }
                ?>
                <div class="row" <?php echo $style ?> id="<?php echo $user_profiles[$i]->getValue('id') ?>" contextmenu="contextmenu">
                    <div class="columns">
                        <span class="column w120" title="<?php echo $user_profiles[$i]->getValue('username') ?>"><?php echo $user_profiles[$i]->getValue('username') ?></span>
                        <span class="column w150" title="<?php echo $user_profiles[$i]->getValue('name') ?>"><?php echo $user_profiles[$i]->getValue('name') ?></span>
                        <?php
                        if (strlen($user_profiles[$i]->getValue('phone2')) > 0) {
                            ?><span class="column w150" title="<?php echo $user_profiles[$i]->getValue('phone1') . ', ' . $user_profiles[$i]->getValue('phone2') ?>"><?php echo $user_profiles[$i]->getValue('phone1') . ', ' . $user_profiles[$i]->getValue('phone2') ?></span><?php
                        } else {
                            ?><span class="column w150" title="<?php echo $user_profiles[$i]->getValue('phone1') ?>"><?php echo $user_profiles[$i]->getValue('phone1') ?></span><?php
                        }
                        ?>
                        <span class="column w120" title="<?php echo $user_profiles[$i]->getValue('email') ?>"><?php echo $user_profiles[$i]->getValue('email') ?></span>
                        <?php
                        if (strlen($user_profiles[$i]->getValue('line_2')) > 0) {
                            ?><span class="column w300" title="<?php echo $user_profiles[$i]->getValue('line_1') . ', ' . $user_profiles[$i]->getValue('line_2') ?>"><?php echo $user_profiles[$i]->getValue('line_1') . ', ' . $user_profiles[$i]->getValue('line_2') ?></span><?php
                        } else {
                            ?><span class="column w300" title="<?php echo $user_profiles[$i]->getValue('line_1') ?>"><?php echo $user_profiles[$i]->getValue('line_1') ?></span><?php
                        }
                        ?>
                        <span class="column w100" style="text-align:center" title="<?php echo $user_profiles[$i]->getValue('city') ?>"><?php echo $user_profiles[$i]->getValue('city') ?></span>
                        <span class="column w100" style="text-align:center" title="<?php echo $user_profiles[$i]->getValue('state') ?>"><?php echo $user_profiles[$i]->getValue('state') ?></span>
                        <span class="column w100" style="text-align:center" title="<?php echo $user_profiles[$i]->getValue('country') ?>"><?php echo $user_profiles[$i]->getValue('country') ?></span>
                        <span class="column w120" style="text-align:center"><?php echo Date::convertFromMySqlDate($user_profiles[$i]->getValue('creation_date')) ?></span>
                        <span class="column w120" style="text-align:center;border:0"><?php echo Date::convertFromMySqlDate($user_profiles[$i]->getValue('last_changed')) ?></span>
                    </div>
                </div>
                <?php
            }
            require BASE_PATH . '/views/backend/includes/datatable-default-rows.php';
            ?>
        </div>
    </div>
    <?php require BASE_PATH . '/views/backend/includes/pagination.php'; ?>
</div>