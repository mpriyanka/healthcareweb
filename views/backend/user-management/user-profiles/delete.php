<form class="ats-ui-form" method="post" action="">
    <h4>User Profile Deletion</h4>
    <div class="row">
        <?php
        if ($REQUEST_ATTRIBUTES['status'] == 0) {
            ?><p class="msg"><b>Error</b><?php
            echo $REQUEST_ATTRIBUTES['error-msg']
            ?></p><div class="clear"></div><?php
            } else {
                ?><p class="msg2">Deleted</p><div class="clear"></div><?php
            }
            ?>
    </div>
    <div class="row">
        <label class="label w150"></label>
        <a class="cancel" style="margin-left: 0;" href="<?php echo CONTEXT_PATH ?>/backend/user-management/user-profiles"><?php if(isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 1) { echo "Return"; } else{ echo "Cancel"; }?></a>
    </div>
</form>