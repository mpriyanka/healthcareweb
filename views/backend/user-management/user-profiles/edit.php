<form class="ats-ui-form" method="post" action="">
    <h4>Edit User Profile</h4>
    <div class="row">
        <?php
        if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 0) {
            ?><p class="msg"><b>Error</b><?php
            echo $REQUEST_ATTRIBUTES['error-msg']
            ?></p><div class="clear"></div><?php
            }
            if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 1) {
                ?><p class="msg2">Updated</p><div class="clear"></div><?php
            }
            ?>
    </div>
    <div class="row">
        <label class="label w150" for="firstname">Firstname</label>
        <input inputtype="textbox" id="firstname" name="firstname" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['user-profile']->getValue('firstname') ?>" autocomplete="off" maxlength="25"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['user-profile']->validationReport('firstname') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="lastname">Lastname</label>
        <input inputtype="textbox" id="lastname" name="lastname" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['user-profile']->getValue('lastname') ?>" autocomplete="off" maxlength="25"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['user-profile']->validationReport('lastname') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="phone1">Phone1</label>
        <input inputtype="textbox" id="phone1" name="phone1" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['user-profile']->getValue('phone1') ?>" autocomplete="off" maxlength="15"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['user-profile']->validationReport('phone1') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="phone2">Phone2</label>
        <input inputtype="textbox" id="phone2" name="phone2" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['user-profile']->getValue('phone2') ?>" autocomplete="off" maxlength="15"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['user-profile']->validationReport('phone2') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="email">Email</label>
        <input inputtype="textbox" id="email" name="email" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['user-profile']->getValue('email') ?>" autocomplete="off" maxlength="200"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['user-profile']->validationReport('email') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="line_1">Address</label>
        <input inputtype="textbox" id="line_1" name="line_1" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['address']->getValue('line_1') ?>" autocomplete="off" maxlength="100"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['address']->validationReport('line_1') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="line_2"></label>
        <input inputtype="textbox" id="line_2" name="line_2" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['address']->getValue('line_2') ?>" autocomplete="off" maxlength="100"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['address']->validationReport('line_2') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="city">City</label>
        <input inputtype="textbox" id="city" name="city" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['address']->getValue('city') ?>" autocomplete="off" maxlength="25"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['address']->validationReport('city') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="state">State</label>
        <input inputtype="textbox" id="state" name="state" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['address']->getValue('state') ?>" autocomplete="off" maxlength="30"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['address']->validationReport('state') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="country">Country</label>
        <select inputtype="_default" id="country" name="country" class="textbox w140" style="width: 200px" autocomplete="off">
            <option value="-">- Select -</option>
            <?php
            $countries = $REQUEST_ATTRIBUTES['country-names'];
            for ($i = 0; $i < $countries->count(); $i++) {
                ?>
                <option <?php if (strcmp(ucfirst(strtolower($countries[$i]->getValue('name'))), $REQUEST_ATTRIBUTES['address']->getValue('country')) == 0) { ?>selected <?php } ?>><?php echo ucfirst(strtolower($countries[$i]->getValue('name'))) ?></option>
                <?php
            }
            ?>
        </select>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['address']->validationReport('country') ?></span>
    </div>
    <div class="row">
        <label class="label w150"></label>
        <button class="submit">Update</button>
        <a class="cancel" href="<?php echo CONTEXT_PATH ?>/backend/user-management/user-profiles"><?php if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 1) {
                echo "Return";
            } else {
                echo "Cancel";
            } ?></a>
        <div class="clear"></div>
    </div>
</form>