<?php

use HealthCare\Util\DataTable;
use HealthCare\Util\Date;

$data_table = new DataTable();
$data_table->SetDefaultOrderByColumn('username');
$data_table->SetColumns($REQUEST_ATTRIBUTES['columns']);
?>
<div class="ats-ui-datatable" id="datatable">
    <div class="toolbar">
        <a class="btn" href="<?php echo CONTEXT_PATH ?>/backend/user-management/users/create" title="Create">Create</a>
        <?php $data_table->SetFilter() ?>
        <div class="clear"></div>
    </div>
    <div class="datasource">
        <script language="javascript" type="text/javascript">
            var contextmenuurl = new Array();
            contextmenuurl["enable"] = {window: "same", url: "<?php echo CONTEXT_PATH; ?>/backend/user-management/users/enable/"};
            contextmenuurl["disable"] = {window: "same", url: "<?php echo CONTEXT_PATH; ?>/backend/user-management/users/disable/"};
            contextmenuurl["change-password"] = {window: "same", url: "<?php echo CONTEXT_PATH; ?>/backend/user-management/users/change-password/"};
            contextmenuurl["delete"] = {window: "same", url: "<?php echo CONTEXT_PATH; ?>/backend/user-management/users/delete/"};

            var contextmenu_enable = [
                {title: 'Enable', cmd: 'enable'},
                {title: 'Change Password', cmd: 'change-password'},
                {title: 'Delete', cmd: 'delete'}
            ];
            
            var contextmenu_disable = [
                {title: 'Disable', cmd: 'disable'},
                {title: 'Change Password', cmd: 'change-password'},
                {title: 'Delete', cmd: 'delete'}
            ];
            
            var filter_columns = jQuery.parseJSON('<?php echo json_encode($data_table->getJavaScriptColumns()) ?>');
        </script>
    </div>
    <div class="scrollpane">
        <div class="rows" style="min-width:1000px;">
            <div class="th">
                <span class="column w300" style="text-align:center"><?php $data_table->getColumnHeader('username') ?></span>
                <span class="column w100" style="text-align:center"><?php $data_table->getColumnHeader('enabled') ?></span>
                <span class="column w120" style="text-align:center"><?php $data_table->getColumnHeader('creation_date') ?></span>
                <span class="column w120" style="text-align:center;border:0"><?php $data_table->getColumnHeader('last_changed') ?></span>
            </div>
            <div class=th_fix></div>
            <?php
            $users = $REQUEST_ATTRIBUTES['users'];
            for ($i = 0; $i < $users->count(); $i++) {
                if ($i == ($REQUEST_ATTRIBUTES['pagination']->getSize() - 1)) {
                    $style = "style=\"border:0;\"";
                } else {
                    $style = "";
                }
                ?>
                <div class="row" <?php echo $style ?> id="<?php echo $users[$i]->getValue('id') ?>" contextmenu="<?php if($users[$i]->getValue('enabled')){ echo "contextmenu_disable";} else { echo "contextmenu_enable";} ?>">
                    <div class="columns">
                        <span class="column w300" title="<?php echo $users[$i]->getValue('username') ?>"><?php echo $users[$i]->getValue('username') ?></span>
                        <?php
                        if($users[$i]->getValue('enabled')){
                            ?><span class="column w100" style="text-align:center" title="Yes">Yes</span><?php
                        }
                        else{
                            ?><span class="column w100" style="text-align:center" title="No">No</span><?php
                        }
                        ?>
                        <span class="column w120" style="text-align:center"><?php echo Date::convertFromMySqlDate($users[$i]->getValue('creation_date')) ?></span>
                        <span class="column w120" style="text-align:center;border:0"><?php echo Date::convertFromMySqlDate($users[$i]->getValue('last_changed')) ?></span>
                    </div>
                </div>
                <?php
            }
            require BASE_PATH . '/views/backend/includes/datatable-default-rows.php';
            ?>
        </div>
    </div>
<?php require BASE_PATH . '/views/backend/includes/pagination.php'; ?>
</div>