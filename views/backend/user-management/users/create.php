<form class="ats-ui-form" method="post" action="">
    <h4>Create User</h4>
    <div class="row">
        <?php
        if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 0) {
            ?><p class="msg"><b>Error</b><?php
            echo $REQUEST_ATTRIBUTES['error-msg']
            ?></p><div class="clear"></div><?php
            }
            if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 1) {
                ?><p class="msg2">Created</p><div class="clear"></div><?php
            }
            ?>
    </div>
    <div class="row">
        <label class="label w150" for="username">Username</label>
        <input inputtype="textbox" id="username" name="username" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['user']->getValue('username') ?>" autocomplete="off" maxlength="250"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['user']->validationReport('username') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="confirm_username">Confirm Username</label>
        <input inputtype="textbox" id="confirm_username" name="confirm_username" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['user']->getValue('confirm_username') ?>" autocomplete="off" maxlength="250"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['user']->validationReport('confirm_username') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="password">Password</label>
        <input inputtype="textbox" type="password" id="password" name="password" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['user']->getValue('password') ?>" autocomplete="off" maxlength="40"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['user']->validationReport('password') ?></span>
    </div>
    <div class="row">
        <label class="label w150"></label>
        <button class="submit">Create</button>
        <a class="cancel" href="<?php echo CONTEXT_PATH ?>/backend/user-management/users"><?php if(isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 1) { echo "Return"; } else{ echo "Cancel"; }?></a>
        <div class="clear"></div>
    </div>
</form>