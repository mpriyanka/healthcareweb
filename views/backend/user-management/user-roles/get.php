<?php

use HealthCare\Util\DataTable;
use HealthCare\Util\Date;

$data_table = new DataTable();
$data_table->SetDefaultOrderByColumn('username');
$data_table->SetColumns($REQUEST_ATTRIBUTES['columns']);

?>
<div class="ats-ui-datatable" id="datatable">
    <div class="toolbar">
        <a class="btn" href="<?php echo CONTEXT_PATH ?>/backend/user-management/user-roles/create" title="Create">Create</a>
        <?php $data_table->SetFilter() ?>
        <div class="clear"></div>
    </div>
    <div class="datasource">
        <script language="javascript" type="text/javascript">
            var contextmenuurl = new Array();
            contextmenuurl["edit"] = {window: "same", url: "<?php echo CONTEXT_PATH; ?>/backend/user-management/user-roles/edit/"};
            contextmenuurl["delete"] = {window: "same", url: "<?php echo CONTEXT_PATH; ?>/backend/user-management/user-roles/delete/"};

            var contextmenu = [
                {title: 'Edit', cmd: 'edit'},
                {title: 'Delete', cmd: 'delete'}
            ];
            
            var filter_columns = jQuery.parseJSON('<?php echo json_encode($data_table->getJavaScriptColumns()) ?>');
        </script>
    </div>
    <div class="scrollpane">
        <div class="rows" style="min-width:1000px;">
            <div class="th">
                <span class="column w200" style="text-align:center"><?php $data_table->getColumnHeader('username') ?></span>
                <span class="column w200" style="text-align:center"><?php $data_table->getColumnHeader('role') ?></span>
                <span class="column w120" style="text-align:center"><?php $data_table->getColumnHeader('creation_date') ?></span>
                <span class="column w120" style="text-align:center;border:0"><?php $data_table->getColumnHeader('last_changed') ?></span>
            </div>
            <div class=th_fix></div>
            <?php
            $user_roles = $REQUEST_ATTRIBUTES['user-roles'];
            for ($i = 0; $i < $user_roles->count(); $i++) {
                if ($i == ($REQUEST_ATTRIBUTES['pagination']->getSize() - 1)) {
                    $style = "style=\"border:0;\"";
                } else {
                    $style = "";
                }
                ?>
                <div class="row" <?php echo $style ?> id="<?php echo $user_roles[$i]->getValue('id') ?>" contextmenu="contextmenu">
                    <div class="columns">
                        <span class="column w200" title="<?php echo $user_roles[$i]->getValue('user') ?>"><?php echo $user_roles[$i]->getValue('user') ?></span>
                        <span class="column w200" title="<?php echo $user_roles[$i]->getValue('role') ?>"><?php echo $user_roles[$i]->getValue('role') ?></span>
                        <span class="column w120" style="text-align:center"><?php echo Date::convertFromMySqlDate($user_roles[$i]->getValue('creation_date')) ?></span>
                        <span class="column w120" style="text-align:center;border:0"><?php echo Date::convertFromMySqlDate($user_roles[$i]->getValue('last_changed')) ?></span>
                    </div>
                </div>
                <?php
            }
            require BASE_PATH . '/views/backend/includes/datatable-default-rows.php';
            ?>
        </div>
    </div>
<?php require BASE_PATH . '/views/backend/includes/pagination.php'; ?>
</div>