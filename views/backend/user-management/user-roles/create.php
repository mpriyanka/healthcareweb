<form class="ats-ui-form" method="post" action="">
    <h4>Create User Role</h4>
    <div class="row">
        <?php
        if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 0) {
            ?><p class="msg"><b>Error</b><?php
            echo $REQUEST_ATTRIBUTES['error-msg']
            ?></p><div class="clear"></div><?php
            }
            if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 1) {
                ?><p class="msg2">Created</p><div class="clear"></div><?php
            }
            ?>
    </div>
    <div class="row">
        <label class="label w150" for="user_id">User</label>
        <select inputtype="_default" id="user_id" name="user_id" class="textbox w140" style="width: 200px" autocomplete="off">
            <option value="-">- Select -</option>
            <?php
            $user_names = $REQUEST_ATTRIBUTES['user-names'];
            for ($i = 0; $i < $user_names->count(); $i++) {
                ?>
                <option <?php if (strcmp($user_names[$i]->getValue('id'), $REQUEST_ATTRIBUTES['user-role']->getValue('user_id')) == 0) { ?>selected <?php } ?> value="<?php echo $user_names[$i]->getValue('id') ?>"><?php echo $user_names[$i]->getValue('username') ?></option>
                <?php
            }
            ?>
        </select>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['user-role']->validationReport('user_id') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="role_id">Role</label>
        <select inputtype="_default" id="role_id" name="role_id" class="textbox w140" style="width: 200px" autocomplete="off">
            <option value="-">- Select -</option>
            <?php
            $role_names = $REQUEST_ATTRIBUTES['role-names'];
            for ($i = 0; $i < $role_names->count(); $i++) {
                ?>
                <option <?php if (strcmp($role_names[$i]->getValue('id'), $REQUEST_ATTRIBUTES['user-role']->getValue('role_id')) == 0) { ?>selected <?php } ?> value="<?php echo $role_names[$i]->getValue('id') ?>"><?php echo $role_names[$i]->getValue('name') ?></option>
                <?php
            }
            ?>
        </select>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['user-role']->validationReport('role_id') ?></span>
    </div>
    <div class="row">
        <label class="label w150"></label>
        <button class="submit">Create</button>
        <a class="cancel" href="<?php echo CONTEXT_PATH ?>/backend/user-management/user-roles"><?php if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 1) {
                echo "Return";
            } else {
                echo "Cancel";
            } ?></a>
        <div class="clear"></div>
    </div>
</form>