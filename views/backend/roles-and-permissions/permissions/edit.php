<form class="ats-ui-form" method="post" action="">
    <h4>Edit Permission</h4>
    <div class="row">
        <?php
        if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 0) {
            ?><p class="msg"><b>Error</b><?php
            echo $REQUEST_ATTRIBUTES['error-msg']
            ?></p><div class="clear"></div><?php
            }
            if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 1) {
                ?><p class="msg2">Updated</p><div class="clear"></div><?php
            }
            ?>
    </div>
    <div class="row">
        <label class="label w150" for="name">Name</label>
        <input inputtype="textbox" id="name" name="name" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['permission']->getValue('name') ?>" autocomplete="off" maxlength="30"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['permission']->validationReport('name') ?></span>
    </div>
    <div class="row">
        <label class="label w150"></label>
        <button class="submit">Update</button>
        <a class="cancel" href="<?php echo CONTEXT_PATH ?>/backend/roles-and-permissions/permissions"><?php if(isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 1) { echo "Return"; } else{ echo "Cancel"; }?></a>
        <div class="clear"></div>
    </div>
</form>