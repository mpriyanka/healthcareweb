<form class="ats-ui-form" method="post" action="">
    <h4>Edit Permission</h4>
    <div class="row">
        <?php
        if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 0) {
            ?><p class="msg"><b>Error</b><?php
            echo $REQUEST_ATTRIBUTES['error-msg']
            ?></p><div class="clear"></div><?php
            }
            if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 1) {
                ?><p class="msg2">Updated</p><div class="clear"></div><?php
            }
            ?>
    </div>
    <div class="row">
        <label class="label w150" for="role_id">Role</label>
        <select inputtype="_default" id="role_id" name="role_id" class="textbox w140" style="width: 200px" autocomplete="off">
            <option value="-">- Select -</option>
            <?php
            $role_names = $REQUEST_ATTRIBUTES['role-names'];
            for ($i = 0; $i < $role_names->count(); $i++) {
                ?>
                <option <?php if (strcmp($role_names[$i]->getValue('id'), $REQUEST_ATTRIBUTES['role-permission']->getValue('role_id')) == 0) { ?>selected <?php } ?> value="<?php echo $role_names[$i]->getValue('id') ?>"><?php echo $role_names[$i]->getValue('name') ?></option>
                <?php
            }
            ?>
        </select>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['role-permission']->validationReport('role_id') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="permission_id">Permission</label>
        <select inputtype="_default" id="permission_id" name="permission_id" class="textbox w140" style="width: 200px" autocomplete="off">
            <option value="-">- Select -</option>
            <?php
            $permission_names = $REQUEST_ATTRIBUTES['permission-names'];
            for ($i = 0; $i < $permission_names->count(); $i++) {
                ?>
                <option <?php if (strcmp($permission_names[$i]->getValue('id'), $REQUEST_ATTRIBUTES['role-permission']->getValue('permission_id')) == 0) { ?>selected <?php } ?> value="<?php echo $permission_names[$i]->getValue('id') ?>"><?php echo $permission_names[$i]->getValue('name') ?></option>
                <?php
            }
            ?>
        </select>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['role-permission']->validationReport('permission_id') ?></span>
    </div>
    <div class="row">
        <label class="label w150"></label>
        <button class="submit">Update</button>
        <a class="cancel" href="<?php echo CONTEXT_PATH ?>/backend/roles-and-permissions/role-permissions"><?php if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 1) {
                echo "Return";
            } else {
                echo "Cancel";
            } ?></a>
        <div class="clear"></div>
    </div>
</form>