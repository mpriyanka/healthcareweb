<form class="ats-ui-form" method="post" action="">
    <h4>Confirm UnPublishing of Page "<b><?php echo $REQUEST_ATTRIBUTES['page']->getValue('title') ?></b>"</h4>
    <input type="hidden" name="id" id="id" value="<?php echo $REQUEST_ATTRIBUTES['page']->getValue('id') ?>"/>
    <input type="hidden" name="confirmed" id="confirmed"/>
    <div class="row">
        <label class="label w150"></label>
        <button class="submit">Confirm</button>
        <a class="cancel" href="<?php echo CONTEXT_PATH ?>/backend/site-management/pages"><?php if(isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 1) { echo "Return"; } else{ echo "Cancel"; }?></a>
    </div>
</form>