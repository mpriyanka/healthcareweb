<?php
$display = (strcasecmp($REQUEST_ATTRIBUTES['page']->getValue('level'), "System") == 0) ? 'none' : 'block';
?>
<form class="ats-ui-form" method="post" action="" enctype="multipart/form-data">
    <h4>Edit Page</h4>
    <div class="row">
        <?php
        if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 0) {
            ?><p class="msg"><b>Error</b><?php
            echo $REQUEST_ATTRIBUTES['error-msg']
            ?></p><div class="clear"></div><?php
            }
            if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 1) {
                ?><p class="msg2">Updated</p><div class="clear"></div><?php
            }
            ?>
    </div>
    <div class="row">
        <label class="label w150" for="title">Title</label>
        <input inputtype="textbox" id="title" name="title" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['page']->getValue('title') ?>" autocomplete="off" maxlength="650"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['page']->validationReport('title') ?></span>
    </div>
    <div class="row" style="display: <?php echo $display ?>">
        <label class="label w150" for="url">URL Path</label>
        <input inputtype="textbox" id="url" name="url" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['page']->getValue('url') ?>" autocomplete="off" maxlength="650"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['page']->validationReport('url') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="tags">Tags</label>
        <input inputtype="textbox" id="tags" name="tags" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['page']->getValue('tags') ?>" autocomplete="off" maxlength="200"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['page']->validationReport('tags') ?></span>
    </div>
    <div class="row" style="display: <?php echo $display ?>">
        <label class="label w150" for="type">Type</label>
        <select inputtype="_default" id="type" name="type" class="textbox w140" style="width: 205px" autocomplete="off">
            <option value="-">- Select -</option>
            <?php
            $page_types = json_decode(MODEL_PAGE_TYPES, true);
            foreach ($page_types as $key => $value) {
                ?><option <?php if (strcmp($key, $REQUEST_ATTRIBUTES['page']->getValue('type')) == 0) { ?>selected <?php } ?> value="<?php echo $key ?>"><?php echo $value ?></option><?php
            }
            ?>
        </select>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['page']->validationReport('type') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="keywords">Keywords</label>
        <textarea inputtype="textbox" id="keywords" name="keywords" rows="4" class="textbox w200"><?php echo $REQUEST_ATTRIBUTES['page']->getValue('keywords') ?></textarea>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['page']->validationReport('keywords') ?></span>
    </div>
    <div class="row">
        <label class="label w150"></label>
        <button class="submit">Update</button>
        <a class="cancel" href="<?php echo CONTEXT_PATH ?>/backend/site-management/pages"><?php
            if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 1) {
                echo "Return";
            } else {
                echo "Cancel";
            }
            ?></a>
        <div class="clear"></div>
    </div>
</form>