<?php

use HealthCare\Util\DataTable;
use HealthCare\Util\Date;

$data_table = new DataTable();
$data_table->SetDefaultOrderByColumn('title');
$data_table->SetColumns($REQUEST_ATTRIBUTES['columns']);

?>
<div class="ats-ui-datatable" id="datatable">
    <div class="toolbar">
        <a class="btn" href="<?php echo CONTEXT_PATH ?>/backend/site-management/pages/create" title="Create">Create</a>
        <?php $data_table->SetFilter() ?>
        <div class="clear"></div>
    </div>
    <div class="datasource">
        <script language="javascript" type="text/javascript">
            var contextmenuurl = new Array();
            contextmenuurl["edit"] = {window: "same", url: "<?php echo CONTEXT_PATH; ?>/backend/site-management/pages/edit/"};
            contextmenuurl["edit-page-body"] = {window: "same", url: "<?php echo CONTEXT_PATH; ?>/backend/site-management/pages/edit-page-body/"};
            contextmenuurl["publish"] = {window: "same", url: "<?php echo CONTEXT_PATH; ?>/backend/site-management/pages/publish/"};
            contextmenuurl["unpublish"] = {window: "same", url: "<?php echo CONTEXT_PATH; ?>/backend/site-management/pages/unpublish/"};
            contextmenuurl["delete"] = {window: "same", url: "<?php echo CONTEXT_PATH; ?>/backend/site-management/pages/delete/"};

            var contextmenu_publish = [
                {title: 'Edit', cmd: 'edit'},
                {title: 'Edit Body', cmd: 'edit-page-body'},
                {title: 'Publish', cmd: 'publish'},
                {title: 'Delete', cmd: 'delete'}
            ];
            
            var contextmenu_unpublish = [
                {title: 'Edit', cmd: 'edit'},
                {title: 'Edit Body', cmd: 'edit-page-body'},
                {title: 'UnPublish', cmd: 'unpublish'},
                {title: 'Delete', cmd: 'delete'}
            ];
            
            var contextmenu_system = [
                {title: 'Edit', cmd: 'edit'},
                {title: 'Edit Body', cmd: 'edit-page-body'}
            ];
            
            var filter_columns = jQuery.parseJSON('<?php echo json_encode($data_table->getJavaScriptColumns()) ?>');
        </script>
    </div>
    <div class="scrollpane">
        <div class="rows" style="min-width:1000px;">
            <div class="th">
                <span class="column w400" style="text-align:center"><?php $data_table->getColumnHeader('title') ?></span>
                <span class="column w200" style="text-align:center"><?php $data_table->getColumnHeader('tags') ?></span>
                <span class="column w120" style="text-align:center"><?php $data_table->getColumnHeader('level') ?></span>
                <span class="column w120" style="text-align:center"><?php $data_table->getColumnHeader('creation_date') ?></span>
                <span class="column w120" style="text-align:center;border:0"><?php $data_table->getColumnHeader('last_changed') ?></span>
            </div>
            <div class=th_fix></div>
            <?php
            $pages = $REQUEST_ATTRIBUTES['pages'];
            for ($i = 0; $i < $pages->count(); $i++) {
                if ($i == ($REQUEST_ATTRIBUTES['pagination']->getSize() - 1)) {
                    $style = "style=\"border:0;\"";
                } else {
                    $style = "";
                }
                if(strcasecmp($pages[$i]->getValue('level'), "System") === 0){
                    $context_menu_str = "contextmenu_system";
                }
                else if(strcasecmp($pages[$i]->getValue('published'), 0) === 0){
                    $context_menu_str = "contextmenu_publish";
                }
                else{
                    $context_menu_str = "contextmenu_unpublish";
                }
                ?>
                <div class="row" <?php echo $style ?> id="<?php echo $pages[$i]->getValue('id') ?>" contextmenu="<?php echo $context_menu_str ?>">
                    <div class="columns">
                        <span class="column w400" title="<?php echo $pages[$i]->getValue('title') ?>"><span class="action-btn" title="Click here for more Actions"></span><?php echo $pages[$i]->getValue('title') ?></span>
                        <span class="column w200" title="<?php echo $pages[$i]->getValue('tags') ?>"><?php echo $pages[$i]->getValue('tags') ?></span>
                        <span class="column w120" style="text-align:center" title="<?php echo $pages[$i]->getValue('level') ?>"><?php echo $pages[$i]->getValue('level') ?></span>
                        <span class="column w120" style="text-align:center"><?php echo Date::convertFromMySqlDate($pages[$i]->getValue('creation_date')) ?></span>
                        <span class="column w120" style="text-align:center;border:0"><?php echo Date::convertFromMySqlDate($pages[$i]->getValue('last_changed')) ?></span>
                    </div>
                </div>
                <?php
            }
            require BASE_PATH . '/views/backend/includes/datatable-default-rows.php';
            ?>
        </div>
    </div>
<?php require BASE_PATH . '/views/backend/includes/pagination.php'; ?>
</div>