<script type="text/javascript" src="<?php echo CDN_CONTEXT_PATH ?>/js/ext/ckeditor/ckeditor.js"></script>
<form class="ats-ui-form" method="post" action="">
    <h4 style="margin-left: 0;">Edit Page Body</h4>
    <div class="row">
        <?php
        if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 0) {
            ?><p style="margin-left: 0;" class="msg"><b>Error</b><?php
            echo $REQUEST_ATTRIBUTES['error-msg']
            ?></p><div class="clear"></div><?php
            }
            if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 1) {
                ?><p style="margin-left: 0;" class="msg2">Updated</p><div class="clear"></div><?php
            }
            ?>
    </div>
    <div class="row">
        <input type="hidden" id="id" name="id" value="<?php echo $REQUEST_ATTRIBUTES['page']->getValue('id') ?>"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['page']->validationReport('id') ?></span>
    </div>
    <div class="row">
        <textarea class="ckeditor" name="body" id="body"><?php echo stripslashes(html_entity_decode($REQUEST_ATTRIBUTES['page']->getValue('body'))) ?></textarea>
    </div>
    <div class="row">
        <button class="submit">Update</button>
        <a class="cancel" href="<?php echo CONTEXT_PATH ?>/backend/site-management/pages"><?php
            if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 1) {
                echo "Return";
            } else {
                echo "Cancel";
            }
            ?></a>
        <div class="clear"></div>
    </div>
</form>
<script>
    CKEDITOR.replace('body');
</script>