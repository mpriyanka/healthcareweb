<form class="ats-ui-form" method="post" action="" enctype="multipart/form-data">
    <h4>Create Gallery Widget</h4>
    <div class="row">
        <?php
        if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 0) {
            ?><p class="msg"><b>Error</b><?php
            echo $REQUEST_ATTRIBUTES['error-msg']
            ?></p><div class="clear"></div><?php
            }
            if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 1) {
                ?><p class="msg2">Copy snippet code</p><div class="clear"></div><?php
            }
            ?>
    </div>
    <div class="row">
        <label class="label w150" for="folders">Folders</label>
        <select inputtype="_default" id="folders" name="folders" class="textbox w140" style="width: 200px" autocomplete="off">
            <option value="-">- Select -</option>
            <?php
            $folder_names = $REQUEST_ATTRIBUTES['folder-names'];
            for ($i = 0; $i < $folder_names->count(); $i++) {
                ?>
                <option <?php if (strcmp($folder_names[$i]->getValue('folder'), $REQUEST_ATTRIBUTES['gallery-widget']->getValue('folder')) == 0) { ?>selected <?php } ?> value="<?php echo $folder_names[$i]->getValue('folder') ?>"><?php echo $folder_names[$i]->getValue('folder') ?></option>
                <?php
            }
            ?>
        </select>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['gallery-widget']->validationReport('folder') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="height">Height (px)</label>
        <input inputtype="textbox" id="height" name="height" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['gallery-widget']->getValue('height') ?>" autocomplete="off" maxlength="100"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['gallery-widget']->validationReport('height') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="width">Width (px)</label>
        <input inputtype="textbox" id="width" name="width" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['gallery-widget']->getValue('width') ?>" autocomplete="off" maxlength="100"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['gallery-widget']->validationReport('width') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="index">Start Index</label>
        <input inputtype="textbox" id="index" name="index" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['gallery-widget']->getValue('index') ?>" autocomplete="off" maxlength="100"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['gallery-widget']->validationReport('index') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="size">Size</label>
        <input inputtype="textbox" id="size" name="size" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['gallery-widget']->getValue('size') ?>" autocomplete="off" maxlength="100"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['gallery-widget']->validationReport('size') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="snippet">Snippet</label>
        <textarea inputtype="textbox" style="height: 60px" id="snippet" name="snippet" class="textbox w200"><?php echo $REQUEST_ATTRIBUTES['gallery-widget']->getValue('snippet') ?></textarea>
    </div>
    <div class="row">
        <label class="label w150"></label>
        <button class="submit">Get Snippet</button>
        <a class="cancel" href="<?php echo CONTEXT_PATH ?>/backend/site-management/images"><?php if(isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 1) { echo "Return"; } else{ echo "Cancel"; }?></a>
        <div class="clear"></div>
    </div>
</form>