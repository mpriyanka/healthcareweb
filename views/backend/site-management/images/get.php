<?php

use HealthCare\Util\DataTable;
use HealthCare\Util\Date;

$data_table = new DataTable();
$data_table->SetDefaultOrderByColumn('title');
$data_table->SetColumns($REQUEST_ATTRIBUTES['columns']);

?>
<div class="ats-ui-datatable" id="datatable">
    <div class="toolbar">
        <a class="btn" href="<?php echo CONTEXT_PATH ?>/backend/site-management/images/create" title="Create">Create</a>
        <a class="btn" style="padding: 0 10px; margin: 0 10px;" href="<?php echo CONTEXT_PATH ?>/backend/site-management/images/create-gallery-widget" title="Create Gallery Widget">Create Gallery Widget</a>
        <?php $data_table->SetFilter() ?>
        <div class="clear"></div>
    </div>
    <div class="datasource">
        <script language="javascript" type="text/javascript">
            var contextmenuurl = new Array();
            contextmenuurl["edit"] = {window: "same", url: "<?php echo CONTEXT_PATH; ?>/backend/site-management/images/edit/"};
            contextmenuurl["delete"] = {window: "same", url: "<?php echo CONTEXT_PATH; ?>/backend/site-management/images/delete/"};

            var contextmenu = [
                {title: 'Edit', cmd: 'edit'},
                {title: 'Delete', cmd: 'delete'}
            ];
            
            var filter_columns = jQuery.parseJSON('<?php echo json_encode($data_table->getJavaScriptColumns()) ?>');
        </script>
    </div>
    <div class="scrollpane">
        <div class="rows" style="min-width:1000px;">
            <div class="th">
                <span class="column w300" style="text-align:center"><?php $data_table->getColumnHeader('title') ?></span>
                <span class="column w200" style="text-align:center"><?php $data_table->getColumnHeader('folder') ?></span>
                <span class="column w300" style="text-align:center">URL</span>
                <span class="column w120" style="text-align:center"><?php $data_table->getColumnHeader('creation_date') ?></span>
                <span class="column w120" style="text-align:center;border:0"><?php $data_table->getColumnHeader('last_changed') ?></span>
            </div>
            <div class=th_fix></div>
            <?php
            $images = $REQUEST_ATTRIBUTES['images'];
            for ($i = 0; $i < $images->count(); $i++) {
                if ($i == ($REQUEST_ATTRIBUTES['pagination']->getSize() - 1)) {
                    $style = "style=\"border:0;\"";
                } else {
                    $style = "";
                }
                ?>
                <div class="row" <?php echo $style ?> id="<?php echo $images[$i]->getValue('id') ?>" contextmenu="contextmenu">
                    <div class="columns">
                        <span class="column w300" title="<?php echo $images[$i]->getValue('title') ?>"><?php echo $images[$i]->getValue('title') ?></span>
                        <span class="column w200" title="<?php echo $images[$i]->getValue('folder') ?>"><?php echo $images[$i]->getValue('folder') ?></span>
                        <span class="column w300" title="dynamic-images/<?php echo $images[$i]->getValue('path') ?>">dynamic-images/<?php echo $images[$i]->getValue('path') ?></span>
                        <span class="column w120" style="text-align:center"><?php echo Date::convertFromMySqlDate($images[$i]->getValue('creation_date')) ?></span>
                        <span class="column w120" style="text-align:center;border:0"><?php echo Date::convertFromMySqlDate($images[$i]->getValue('last_changed')) ?></span>
                    </div>
                </div>
                <?php
            }
            require BASE_PATH . '/views/backend/includes/datatable-default-rows.php';
            ?>
        </div>
    </div>
<?php require BASE_PATH . '/views/backend/includes/pagination.php'; ?>
</div>