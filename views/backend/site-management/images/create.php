<form class="ats-ui-form" method="post" action="" enctype="multipart/form-data">
    <h4>Create Image</h4>
    <div class="row">
        <?php
        if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 0) {
            ?><p class="msg"><b>Error</b><?php
            echo $REQUEST_ATTRIBUTES['error-msg']
            ?></p><div class="clear"></div><?php
            }
            if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 1) {
                ?><p class="msg2">Created</p><div class="clear"></div><?php
            }
            ?>
    </div>
    <div class="row">
        <label class="label w150" for="title">Title</label>
        <input inputtype="textbox" id="title" name="title" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['image']->getValue('title') ?>" autocomplete="off" maxlength="100"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['image']->validationReport('title') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="folders">Existing Folders</label>
        <select inputtype="_default" id="folders" name="folders" class="textbox w140" style="width: 200px" autocomplete="off">
            <option value="-">- Select -</option>
            <?php
            $folder_names = $REQUEST_ATTRIBUTES['folder-names'];
            for ($i = 0; $i < $folder_names->count(); $i++) {
                ?>
                <option <?php if (strcmp($folder_names[$i]->getValue('folder'), $REQUEST_ATTRIBUTES['image']->getValue('folder')) == 0) { ?>selected <?php } ?> value="<?php echo $folder_names[$i]->getValue('folder') ?>"><?php echo $folder_names[$i]->getValue('folder') ?></option>
                <?php
            }
            ?>
        </select>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['image']->validationReport('folder') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="folder">New Folder</label>
        <input inputtype="textbox" id="folder" name="folder" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['image']->getValue('folder') ?>" autocomplete="off" maxlength="100"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['image']->validationReport('folder') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="file">Image</label>
        <input inputtype="_default" type="file" id="file" name="file" class="textbox w240" style="border: 0;"/>
    </div>
    <div class="row">
        <label class="label w150"></label>
        <button class="submit">Create</button>
        <a class="cancel" href="<?php echo CONTEXT_PATH ?>/backend/site-management/images"><?php if(isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 1) { echo "Return"; } else{ echo "Cancel"; }?></a>
        <div class="clear"></div>
    </div>
</form>