<?php

use HealthCare\Util\DataTable;
use HealthCare\Util\Date;

$data_table = new DataTable();
$data_table->SetDefaultOrderByColumn('name');
$data_table->SetColumns($REQUEST_ATTRIBUTES['columns']);

$array_status = array('Pending Verification', 'Pending Review', 'Approved', 'Rejected', 'NA', 'Pending Password Reset', 'Disabled');
?>
<div class="ats-ui-datatable" id="datatable">
    <div class="toolbar">
        <a class="btn" href="<?php echo CONTEXT_PATH ?>/backend/site-management/doctors/create" title="Create">Create</a>
        <?php $data_table->SetFilter() ?>
        <div class="clear"></div>
    </div>
    <div class="datasource">
        <script language="javascript" type="text/javascript">
            var contextmenuurl = new Array();
            contextmenuurl["resend-verification"] = {window: "same", url: "<?php echo CONTEXT_PATH; ?>/backend/site-management/doctors/resend-verification/"};
            contextmenuurl["edit"] = {window: "same", url: "<?php echo CONTEXT_PATH; ?>/backend/site-management/doctors/edit/"};
            contextmenuurl["accept"] = {window: "same", url: "<?php echo CONTEXT_PATH; ?>/backend/site-management/doctors/accept/"};
            contextmenuurl["reject"] = {window: "same", url: "<?php echo CONTEXT_PATH; ?>/backend/site-management/doctors/reject/"};
            contextmenuurl["disable"] = {window: "same", url: "<?php echo CONTEXT_PATH; ?>/backend/site-management/doctors/disable/"};
            contextmenuurl["enable"] = {window: "same", url: "<?php echo CONTEXT_PATH; ?>/backend/site-management/doctors/enable/"};

            var contextmenu = [
                {title: 'Accept', cmd: 'accept'},
                {title: 'Reject', cmd: 'reject'},
                {title: 'Edit', cmd: 'edit'}
            ];
            
            var contextmenu_pending_verification = [
                {title: 'Resend Verification', cmd: 'resend-verification'}
            ];
            
            var contextmenu_accepted = [
                {title: 'Disable', cmd: 'disable'},
                {title: 'Edit', cmd: 'edit'}
            ];
            
            var contextmenu_disbaled = [
                {title: 'Enable', cmd: 'enable'},
                {title: 'Edit', cmd: 'edit'}
            ];
            
            var filter_columns = jQuery.parseJSON('<?php echo json_encode($data_table->getJavaScriptColumns()) ?>');
        </script>
    </div>
    <div class="scrollpane">
        <div class="rows" style="min-width:1950px;">
            <div class="th">
                <span class="column w220" style="text-align:center"><?php $data_table->getColumnHeader('name') ?></span>
                <span class="column w200" style="text-align:center"><?php $data_table->getColumnHeader('username') ?></span>
                <span class="column w120" style="text-align:center"><?php $data_table->getColumnHeader('status') ?></span>
                <span class="column w100" style="text-align:center"><?php $data_table->getColumnHeader('accepted_date') ?></span>
                <span class="column w100" style="text-align:center"><?php $data_table->getColumnHeader('phone') ?></span>
                <span class="column w100" style="text-align:center"><?php $data_table->getColumnHeader('mobile') ?></span>
                <span class="column w300" style="text-align:center"><?php $data_table->getColumnHeader('address') ?></span>
                <span class="column w100" style="text-align:center"><?php $data_table->getColumnHeader('city') ?></span>
                <span class="column w100" style="text-align:center"><?php $data_table->getColumnHeader('state') ?></span>
                <span class="column w150" style="text-align:center"><?php $data_table->getColumnHeader('country') ?></span>
                <span class="column w120" style="text-align:center"><?php $data_table->getColumnHeader('creation_date') ?></span>
                <span class="column w120" style="text-align:center;border:0"><?php $data_table->getColumnHeader('last_changed') ?></span>
            </div>
            <div class=th_fix></div>
            <?php
            $doctors = $REQUEST_ATTRIBUTES['doctors'];
            for ($i = 0; $i < $doctors->count(); $i++) {
                if ($i == ($REQUEST_ATTRIBUTES['pagination']->getSize() - 1)) {
                    $style = "style=\"border:0;\"";
                } else {
                    $style = "";
                }
                $context_menu_str = "contextmenu";
                switch($doctors[$i]->getValue('status')){
                    case 0:
                        $context_menu_str = "contextmenu_pending_verification";
                        break;
                    case 2:
                        $context_menu_str = "contextmenu_accepted";
                        break;
                    case 6:
                        $context_menu_str = "contextmenu_disbaled";
                        break;
                }
                ?>
            <div class="row" <?php echo $style ?> id="<?php echo $doctors[$i]->getValue('id') ?>" contextmenu="<?php echo $context_menu_str ?>">
                    <div class="columns">
                        <span class="column w220" title="<?php echo 'Dr ' . $doctors[$i]->getValue('firstname'). ' ' . $doctors[$i]->getValue('middlename'). ' ' . $doctors[$i]->getValue('lastname') ?>"><?php echo 'Dr. ' . $doctors[$i]->getValue('firstname'). ' ' . $doctors[$i]->getValue('middlename') . ' ' . $doctors[$i]->getValue('lastname') ?></span>
                        <span class="column w200" title="<?php echo $doctors[$i]->getValue('username') ?>"><?php echo $doctors[$i]->getValue('username') ?></span>
                        <span class="column w120" style="text-align:center" title="<?php echo $array_status[$doctors[$i]->getValue('status')] ?>"><?php echo $array_status[$doctors[$i]->getValue('status')] ?></span>
                        <span class="column w100" style="text-align:center" title="<?php echo $doctors[$i]->getValue('accepted_date') ?>"><?php echo $doctors[$i]->getValue('accepted_date') ?></span>
                        <span class="column w100" title="<?php echo $doctors[$i]->getValue('phone') ?>"><?php echo $doctors[$i]->getValue('phone') ?></span>
                        <span class="column w100" title="<?php echo $doctors[$i]->getValue('mobile') ?>"><?php echo $doctors[$i]->getValue('mobile') ?></span>
                            <?php
                        if (strlen($doctors[$i]->getValue('line_2')) > 0) {
                            ?><span class="column w300" title="<?php echo $doctors[$i]->getValue('line_1') . ', ' . $doctors[$i]->getValue('line_2') ?>"><?php echo $doctors[$i]->getValue('line_1') . ', ' . $doctors[$i]->getValue('line_2') ?></span><?php
                        } else {
                            ?><span class="column w300" title="<?php echo $doctors[$i]->getValue('line_1') ?>"><?php echo $doctors[$i]->getValue('line_1') ?></span><?php
                        }
                        ?>
                        <span class="column w100" style="text-align:center" title="<?php echo $doctors[$i]->getValue('city') ?>"><?php echo $doctors[$i]->getValue('city') ?></span>
                        <span class="column w100" style="text-align:center" title="<?php echo $doctors[$i]->getValue('state') ?>"><?php echo $doctors[$i]->getValue('state') ?></span>
                        <span class="column w150" style="text-align:center" title="<?php echo $doctors[$i]->getValue('country') ?>"><?php echo $doctors[$i]->getValue('country') ?></span>
                        <span class="column w120" style="text-align:center"><?php echo Date::convertFromMySqlDate($doctors[$i]->getValue('creation_date')) ?></span>
                        <span class="column w120" style="text-align:center;border:0"><?php echo Date::convertFromMySqlDate($doctors[$i]->getValue('last_changed')) ?></span>
                    </div>
                </div>
                <?php
            }
            require BASE_PATH . '/views/backend/includes/datatable-default-rows.php';
            ?>
        </div>
    </div>
    <?php require BASE_PATH . '/views/backend/includes/pagination.php'; ?>
</div>