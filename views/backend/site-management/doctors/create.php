<form class="ats-ui-form" method="post" action="">
    <h4>Create Member</h4>
    <div class="row">
        <?php
        if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 0) {
            ?><p class="msg"><b>Error</b><?php
            echo $REQUEST_ATTRIBUTES['error-msg']
            ?></p><div class="clear"></div><?php
            }
            if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 1) {
                ?><p class="msg2">Created</p><div class="clear"></div><?php
            }
            ?>
    </div>
    <div class="row">
        <label class="label w150" for="title">Title</label>
        <select inputtype="_default" id="title" name="title" class="textbox w140" style="width: 200px" autocomplete="off">
            <option value="-">- Select -</option>
            <option <?php if (strcmp("Prof", $REQUEST_ATTRIBUTES['profile']->getValue('title')) == 0) { ?>selected <?php } ?> value="Prof">Prof</option>
            <option <?php if (strcmp("Dr", $REQUEST_ATTRIBUTES['profile']->getValue('title')) == 0) { ?>selected <?php } ?> value="Dr">Dr</option>
            <option <?php if (strcmp("Mr", $REQUEST_ATTRIBUTES['profile']->getValue('title')) == 0) { ?>selected <?php } ?> value="Mr">Mr</option>
            <option <?php if (strcmp("Mrs", $REQUEST_ATTRIBUTES['profile']->getValue('title')) == 0) { ?>selected <?php } ?> value="Mrs">Mrs</option>
            <option <?php if (strcmp("Miss", $REQUEST_ATTRIBUTES['profile']->getValue('title')) == 0) { ?>selected <?php } ?> value="Miss">Miss</option>
            <option <?php if (strcmp("Engr", $REQUEST_ATTRIBUTES['profile']->getValue('title')) == 0) { ?>selected <?php } ?> value="Engr">Engr</option>
        </select>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['profile']->validationReport('title') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="firstname">First Name</label>
        <input inputtype="textbox" id="firstname" name="firstname" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['profile']->getValue('firstname') ?>" autocomplete="off" maxlength="25"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['profile']->validationReport('firstname') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="middlename">Middle Name</label>
        <input inputtype="textbox" id="middlename" name="middlename" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['profile']->getValue('middlename') ?>" autocomplete="off" maxlength="25"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['profile']->validationReport('middlename') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="lastname">Last Name</label>
        <input inputtype="textbox" id="lastname" name="lastname" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['profile']->getValue('lastname') ?>" autocomplete="off" maxlength="25"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['profile']->validationReport('lastname') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="phone">Phone</label>
        <input inputtype="textbox" id="phone" name="phone" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['profile']->getValue('phone') ?>" autocomplete="off" maxlength="15"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['profile']->validationReport('phone') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="mobile">Mobile</label>
        <input inputtype="textbox" id="mobile" name="mobile" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['profile']->getValue('mobile') ?>" autocomplete="off" maxlength="15"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['profile']->validationReport('mobile') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="affiliation">Affiliation</label>
        <input inputtype="textbox" id="affiliations" name="affiliations" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['profile']->getValue('affiliations') ?>" autocomplete="off" maxlength="600"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['profile']->validationReport('affiliations') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="line_1">Address</label>
        <input inputtype="textbox" id="line_1" name="line_1" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['address']->getValue('line_1') ?>" autocomplete="off" maxlength="100"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['address']->validationReport('line_1') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="line_2"></label>
        <input inputtype="textbox" id="line_2" name="line_2" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['address']->getValue('line_2') ?>" autocomplete="off" maxlength="100"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['address']->validationReport('line_2') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="city">City</label>
        <input inputtype="textbox" id="city" name="city" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['address']->getValue('city') ?>" autocomplete="off" maxlength="25"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['address']->validationReport('city') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="state">State</label>
        <input inputtype="textbox" id="state" name="state" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['address']->getValue('state') ?>" autocomplete="off" maxlength="30"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['address']->validationReport('state') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="country">Country</label>
        <select inputtype="_default" id="country" name="country" class="textbox w140" style="width: 200px" autocomplete="off">
            <option value="-">- Select -</option>
            <?php
            $countries = $REQUEST_ATTRIBUTES['country-names'];
            for ($i = 0; $i < $countries->count(); $i++) {
                ?>
                <option <?php if (strcmp(ucfirst(strtolower($countries[$i]->getValue('name'))), $REQUEST_ATTRIBUTES['address']->getValue('country')) == 0) { ?>selected <?php } ?>><?php echo ucfirst(strtolower($countries[$i]->getValue('name'))) ?></option>
                <?php
            }
            ?>
        </select>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['address']->validationReport('country') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="username">Email/Username</label>
        <input inputtype="textbox" id="username" name="username" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['member']->getValue('username') ?>" autocomplete="off" maxlength="200"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['member']->validationReport('username') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="confirm_username">Confirm Username</label>
        <input inputtype="textbox" id="confirm_username" name="confirm_username" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['member']->getValue('confirm_username') ?>" autocomplete="off" maxlength="200"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['member']->validationReport('confirm_username') ?></span>
    </div>
    <div class="row">
        <label class="label w150" for="password">Password</label>
        <input inputtype="textbox" type="password" id="password" name="password" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['member']->getValue('password') ?>" autocomplete="off" maxlength="40"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['member']->validationReport('password') ?></span>
    </div>
    <div class="row">
        <label class="label w150"></label>
        <button class="submit">Create</button>
        <a class="cancel" href="<?php echo CONTEXT_PATH ?>/backend/site-management/members"><?php
            if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 1) {
                echo "Return";
            } else {
                echo "Cancel";
            }
            ?></a>
        <div class="clear"></div>
    </div>
</form>