<form class="ats-ui-form" method="post" action="">
    <h4>Confirm Resending of Verification</h4>
    <input type="hidden" name="id" id="id" value="<?php echo $REQUEST_ATTRIBUTES['doctor']->getValue('id') ?>"/>
    <input type="hidden" name="confirmed" id="confirmed"/>
    <div class="row">
        <label class="label w150"></label>
        <button class="submit">Send</button>
        <a class="cancel" href="<?php echo CONTEXT_PATH ?>/backend/site-management/doctors"><?php if(isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 1) { echo "Return"; } else{ echo "Cancel"; }?></a>
    </div>
</form>