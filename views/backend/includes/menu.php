<?php

use HealthCare\User\Impl\UserService;

global $REQUEST_PATHS;
?>
<div class="menu">
    <?php
    if (UserService::HasPermission("Manage Page|View Page|Manage Setting|View Setting|Manage Widget|View Widget", null)) {
        ?>
        <a class="tab tag-icon">Site Management</a>
        <dl <?php if (isset($REQUEST_PATHS[1]) && strcmp($REQUEST_PATHS[1], "site-management") == 0) { ?>style="display: block;"<?php } ?>>
            <?php
            if (UserService::HasPermission("Manage Page|View Page", null)) {
                ?><dd><a class="<?php if (isset($REQUEST_PATHS[2]) && strcmp($REQUEST_PATHS[2], "pages") == 0) { ?>active<?php } ?>" href="<?php echo CONTEXT_PATH ?>/backend/site-management/pages">Pages</a></dd><?php
            }
            if (UserService::HasPermission("Manage Doctor|View Doctor", null)) {
                ?><dd><a class="<?php if (isset($REQUEST_PATHS[2]) && strcmp($REQUEST_PATHS[2], "doctors") == 0) { ?>active<?php } ?>" href="<?php echo CONTEXT_PATH ?>/backend/site-management/doctors">Doctors</a></dd><?php
            }
            if (UserService::HasPermission("Manage Image|View Image", null)) {
                ?><dd><a class="<?php if (isset($REQUEST_PATHS[2]) && strcmp($REQUEST_PATHS[2], "images") == 0) { ?>active<?php } ?>" href="<?php echo CONTEXT_PATH ?>/backend/site-management/images">Images</a></dd><?php
            }
            ?>
        </dl>
        <?php
    }
    
    if (UserService::HasPermission("Manage User|Manage User Role|Manage User Profile|Manage User Activity", null)) {
        ?>
        <a class="tab tag-icon">User Management</a>
        <dl <?php if (isset($REQUEST_PATHS[1]) && strcmp($REQUEST_PATHS[1], "user-management") == 0) { ?>style="display: block;"<?php } ?>>
            <?php
            if (UserService::HasPermission("Manage User", null)) {
                ?><dd><a class="<?php if (isset($REQUEST_PATHS[2]) && strcmp($REQUEST_PATHS[2], "users") == 0) { ?>active<?php } ?>" href="<?php echo CONTEXT_PATH ?>/backend/user-management/users">Users</a></dd><?php
            }
            if (UserService::HasPermission("Manage User Profile", null)) {
                ?><dd><a class="<?php if (isset($REQUEST_PATHS[2]) && strcmp($REQUEST_PATHS[2], "user-profiles") == 0) { ?>active<?php } ?>" href="<?php echo CONTEXT_PATH ?>/backend/user-management/user-profiles">User Profiles</a></dd><?php
            }
            if (UserService::HasPermission("Manage User Role", null)) {
                ?><dd><a class="<?php if (isset($REQUEST_PATHS[2]) && strcmp($REQUEST_PATHS[2], "user-roles") == 0) { ?>active<?php } ?>" href="<?php echo CONTEXT_PATH ?>/backend/user-management/user-roles">User Roles</a></dd><?php
            }
            
            ?>
        </dl>
        <?php
    }

    if (UserService::HasPermission("Manage Role|Manage Role Permission", null)) {
        ?>
        <a class="tab tag-icon">Roles & Permissions</a>
        <dl <?php if (isset($REQUEST_PATHS[1]) && strcmp($REQUEST_PATHS[1], "roles-and-permissions") == 0) { ?>style="display: block;"<?php } ?>>
            <?php
            if (UserService::HasPermission("Manage Role", null)) {
                ?><dd><a class="<?php if (isset($REQUEST_PATHS[2]) && strcmp($REQUEST_PATHS[2], "roles") == 0) { ?>active<?php } ?>" href="<?php echo CONTEXT_PATH ?>/backend/roles-and-permissions/roles">Roles</a></dd><?php
            }
            ?>
            <dd><a class="<?php if (isset($REQUEST_PATHS[2]) && strcmp($REQUEST_PATHS[2], "permissions") == 0) { ?>active<?php } ?>" href="<?php echo CONTEXT_PATH ?>/backend/roles-and-permissions/permissions">Permissions</a></dd>
            <?php
            if (UserService::HasPermission("Manage Role Permission", null)) {
                ?><dd><a class="<?php if (isset($REQUEST_PATHS[2]) && strcmp($REQUEST_PATHS[2], "role-permissions") == 0) { ?>active<?php } ?>" href="<?php echo CONTEXT_PATH ?>/backend/roles-and-permissions/role-permissions">Role Permissions</a></dd><?php
            }
            ?>
        </dl>
        <?php
    }
    ?>
</div>