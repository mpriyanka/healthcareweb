<script type="text/javascript">
    WebFontConfig = {
        google: {families: ['Lato::latin', 'Noto+Serif:400,700:latin', 'Montserrat:400,700:latin']}
    };
    (function() {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
                '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })();
</script>
<section id="header">
    <section class="menu">
        <span class="short-cut">
            <div class="dropdown">
                
            </div>
        </span>
    </section>
    <section class="account">
        <span>Sign in as  <?php echo $_SESSION['IPrincipal']['username'] ?></span>
        <div class="list">
            <a href="<?php echo CONTEXT_PATH ?>/backend/change-password">Change Password</a>
            <a href="<?php echo CONTEXT_PATH ?>/logout">Sign Out</a>
        </div>
    </section>
</section>