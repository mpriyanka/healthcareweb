<?php

$pagination = $REQUEST_ATTRIBUTES['pagination'];

for ($i = $pagination->getPageCount(); $i < $pagination->getSize(); $i++) {
    if ($i == ($pagination->getSize() - 1)) {
        $style = "style=\"border:0;\"";
    } else {
        $style = "";
    }
    ?>
    <div class="row" <?php echo $style ?>><div class="columns"></div></div>
    <?php
}