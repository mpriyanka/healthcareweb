<script type="text/javascript">
    WebFontConfig = {
        google: {families: ['Lato::latin', 'Noto+Serif:400,700:latin', 'Montserrat:400,700:latin']}
    };
    (function() {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
                '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })();
</script>
<div id="header">
    <div class="wrapper">
        <a class="logo">Health Care</a>
        <ul class="nav">
            <li><a class="<?php echo (count($REQUEST_PATHS) == 2)? "active" :  '' ?>" href="<?php echo CONTEXT_PATH ?>/">Home</a></li>
            <li><a class="<?php echo (count($REQUEST_PATHS) > 2 && strcmp($REQUEST_PATHS[0], "about") == 0)? "active" :  '' ?>" href="<?php echo CONTEXT_PATH ?>/about">About</a></li>
            <li><a class="<?php echo (count($REQUEST_PATHS) > 2 && strcmp($REQUEST_PATHS[0], "how-it-works") == 0)? "active" :  '' ?>" href="<?php echo CONTEXT_PATH ?>/how-it-works">How It Works</a></li>
            <li><a class="<?php echo (count($REQUEST_PATHS) > 2 && strcmp($REQUEST_PATHS[0], "pricing") == 0)? "active" :  '' ?>" href="<?php echo CONTEXT_PATH ?>/pricing">Pricing</a></li>
            <li><a class="<?php echo (count($REQUEST_PATHS) > 2 && strcmp($REQUEST_PATHS[0], "guidelines-for-doctors") == 0)? "active" :  '' ?>" href="<?php echo CONTEXT_PATH ?>/guidelines-for-doctors">Guidelines For Doctors</a></li>
            <li><a class="<?php echo (count($REQUEST_PATHS) > 2 && strcmp($REQUEST_PATHS[0], "contact") == 0)? "active" :  '' ?>" href="<?php echo CONTEXT_PATH ?>/contact">Contact</a></li>
            <li><a class="<?php echo (count($REQUEST_PATHS) > 2 && strcmp($REQUEST_PATHS[0], "faqs") == 0)? "active" :  '' ?>" href="<?php echo CONTEXT_PATH ?>/faqs">FAQs</a></li>
            <li><a class="doc-link" href="<?php echo CONTEXT_PATH ?>/doctor-signup">Doctor? Start Here</a></li>
        </ul>
        <div class="clear"></div>
    </div>
</div>