<div id="footer">
    <div class="wrapper">
        <div class="top">
            <a class="playstore-logo" href="#"></a>
            <div class="clear"></div>
            <div class="social-icons">
                <a class="facebook" href="#"></a>
                <a class="twitter" href="#"></a>
                <a class="google-plus" href="#"></a>
                <div class="clear"></div>
                <a class="linkedin" href="#"></a>
                <a class="youtube" href="#"></a>
                <a class="email" href="#"></a>
            </div>
            <div class="clear"></div>
        </div>
        <div class="bottom">
            <p class="copyright"><?php echo date('Y') ?> &copy; <?php echo SITE_NAME ?>. All Rights Reserved.</p>
            <ul class="nav">
                <li><a href="<?php echo CONTEXT_PATH ?>/terms">Terms & Condition</a></li>
                <li><a href="<?php echo CONTEXT_PATH ?>/privacy-policy">Privacy Policy</a></li>
                <li style="border:0;"><a href="<?php echo CONTEXT_PATH ?>/faqs">FAQs</a></li>
            </ul>
            <div class="clear"></div>
        </div>
    </div>
</div>