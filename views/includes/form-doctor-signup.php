<form class="ats-ui-form" method="post" action="" enctype="multipart/form-data">
    <h4>Fill in Details</h4>
    <div class="row" style="margin-left: -4em;">
        <?php
        if (isset($REQUEST_ATTRIBUTES['status']) && $REQUEST_ATTRIBUTES['status'] == 0) {
            ?><p class="msg"><b>Error</b><?php
            echo $REQUEST_ATTRIBUTES['error-msg']
            ?></p><div class="clear"></div><?php
            }
            ?>
    </div>
    <div class="row">
        <label class="label w120" for="firstname">First Name</label>
        <input inputtype="textbox" id="firstname" name="firstname" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['profile']->getValue('firstname') ?>" autocomplete="off" maxlength="25"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['profile']->validationReport('firstname') ?></span>
    </div>
    <div class="row">
        <label class="label w120" for="middlename">Middle Name</label>
        <input inputtype="textbox" id="middlename" name="middlename" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['profile']->getValue('middlename') ?>" autocomplete="off" maxlength="25"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['profile']->validationReport('middlename') ?></span>
    </div>
    <div class="row">
        <label class="label w120" for="lastname">Last Name</label>
        <input inputtype="textbox" id="lastname" name="lastname" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['profile']->getValue('lastname') ?>" autocomplete="off" maxlength="25"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['profile']->validationReport('lastname') ?></span>
    </div>
    <div class="row">
        <label class="label w120" for="phone">Phone</label>
        <input inputtype="textbox" id="phone" name="phone" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['profile']->getValue('phone') ?>" autocomplete="off" maxlength="15"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['profile']->validationReport('phone') ?></span>
    </div>
    <div class="row">
        <label class="label w120" for="mobile">Mobile</label>
        <input inputtype="textbox" id="mobile" name="mobile" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['profile']->getValue('mobile') ?>" autocomplete="off" maxlength="15"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['profile']->validationReport('mobile') ?></span>
    </div>
    <div class="row">
        <label class="label w120" for="email">Email</label>
        <input inputtype="textbox" id="email" name="email" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['profile']->getValue('email') ?>" autocomplete="off" maxlength="250"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['profile']->validationReport('email') ?></span>
    </div>
    <div class="row">
        <label class="label w120" for="line_1">Address</label>
        <input inputtype="textbox" id="line_1" name="line_1" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['address']->getValue('line_1') ?>" autocomplete="off" maxlength="100"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['address']->validationReport('line_1') ?></span>
    </div>
    <div class="row">
        <label class="label w120" for="line_2"></label>
        <input inputtype="textbox" id="line_2" name="line_2" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['address']->getValue('line_2') ?>" autocomplete="off" maxlength="100"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['address']->validationReport('line_2') ?></span>
    </div>
    <div class="row">
        <label class="label w120" for="city">City</label>
        <input inputtype="textbox" id="city" name="city" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['address']->getValue('city') ?>" autocomplete="off" maxlength="25"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['address']->validationReport('city') ?></span>
    </div>
    <div class="row">
        <label class="label w120" for="state">State</label>
        <input inputtype="textbox" id="state" name="state" class="textbox w200" value="<?php echo $REQUEST_ATTRIBUTES['address']->getValue('state') ?>" autocomplete="off" maxlength="30"/>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['address']->validationReport('state') ?></span>
    </div>
    <div class="row">
        <label class="label w120" for="country">Country</label>
        <select inputtype="_default" id="country" name="country" class="textbox w140" style="width: 200px" autocomplete="off">
            <option value="-">- Select -</option>
            <?php
            $countries = $REQUEST_ATTRIBUTES['country-names'];
            for ($i = 0; $i < $countries->count(); $i++) {
                ?>
                <option <?php if (strcmp(ucfirst(strtolower($countries[$i]->getValue('name'))), $REQUEST_ATTRIBUTES['address']->getValue('country')) == 0) { ?>selected <?php } ?>><?php echo ucfirst(strtolower($countries[$i]->getValue('name'))) ?></option>
                <?php
            }
            ?>
        </select>
        <span class="error"><?php echo $REQUEST_ATTRIBUTES['address']->validationReport('country') ?></span>
    </div>
    <div class="row">
        <label class="label w120" for="file">Medical Profile</label>
        <input inputtype="_default" type="file" id="file" name="file" class="textbox w240" style="border: 0;"/>
    </div>
    <div class="row">
        <label class="label w120"></label>
        <button class="submit">Sign Up</button>
        <div class="clear"></div>
    </div>
</form>