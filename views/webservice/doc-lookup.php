<?php

$doctors = $REQUEST_ATTRIBUTES['doctors'];

$json_dotors = array();
$json_dotors['total'] = $REQUEST_ATTRIBUTES['pagination']->getTotal();
$json_dotors['doctors'] = array();

for($i = 0; $i < $doctors->count(); $i++){
    $doctor['id'] = $doctors[$i]->getValue('id');
    $doctor['firstname'] = $doctors[$i]->getValue('firstname');
    $doctor['middlename'] = $doctors[$i]->getValue('middlename');
    $doctor['lastname'] = $doctors[$i]->getValue('lastname');
    $doctor['rating'] = rand(1, 5);
    $doctor['availability'] = 1;
    $doctor['consultations'] = rand(1, 300);
    $doctor['image_path'] = $doctors[$i]->getValue('image_path');
    
    $json_dotors['doctors'][] = $doctor;
}

echo json_encode($json_dotors, true);
