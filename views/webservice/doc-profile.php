<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Doctor Profile</title>
        <link href="<?php echo CDN_CONTEXT_PATH ?>/css/style.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo CDN_CONTEXT_PATH ?>/css/data.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo CDN_CONTEXT_PATH ?>/css/fonts.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div id="content">
            <div class="block-content" style="padding: 0 1em;">
                <?php echo stripslashes(html_entity_decode($REQUEST_ATTRIBUTES['profile']->getValue('bio'))) ?>
            </div>
        </div>
    </body>
</html>
