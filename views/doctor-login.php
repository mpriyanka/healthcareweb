<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Login</title>
        <link href="<?php echo CDN_CONTEXT_PATH ?>/css/login.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div id="content">
            <div class="wrapper">
                <form id="login" action="" method="post">
                    <div class="row error">
                        <center><?php
                            if (isset($REQUEST_ATTRIBUTES['error-msg'])) {
                                echo $REQUEST_ATTRIBUTES['error-msg'];
                            } else {
                                echo "Enter your login details";
                            }
                            ?></center>
                    </div>
                    <div class="row">
                        <label>Username:</label>
                        <input type="text" name="username"/>
                    </div>
                    <div class="row">
                        <label>Password:</label>
                        <input type="password" name="password"/>
                    </div>
                    <div><center><button class="btn">Login</button>&nbsp; or &nbsp;<a href="<?php echo CONTEXT_PATH ?>/doctor-signup">Sign Up</a></center></div>
                    <br/>
                    <div><center><a href="<?php echo CONTEXT_PATH ?>/doc-forgot-password">Forgot Password?</a></center></div>
                </form>
                <div class="clear"></div>
            </div>
            <center class="copyright">Copyright &copy; 2016. <?php echo SITE_NAME ?>.</center>
        </div>
    </body>
</html>
