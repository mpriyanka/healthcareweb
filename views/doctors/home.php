<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Dashboard</title>
        <link href="<?php echo CDN_CONTEXT_PATH ?>/css/doctor.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo CDN_CONTEXT_PATH ?>/css/data.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo CDN_CONTEXT_PATH ?>/css/datagridview.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo CDN_CONTEXT_PATH ?>/js/ext/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="<?php echo CDN_CONTEXT_PATH ?>/favicon.ico" type="image/ico" />
        <script type="text/javascript" src="<?php echo CDN_CONTEXT_PATH ?>/js/ext/jquery-1.7.1.min.js" ></script>
        <script type="text/javascript" src="<?php echo CDN_CONTEXT_PATH ?>/js/ext/jquery-ui-1.10.4.custom.min.js" ></script>
        <script type="text/javascript" src="<?php echo CDN_CONTEXT_PATH ?>/js/ext/jquery.ui-contextmenu.min.js" ></script>
        <script type="text/javascript" src="<?php echo CDN_CONTEXT_PATH ?>/js/ext/json.js" ></script>
        <script type="text/javascript" src="<?php echo CDN_CONTEXT_PATH ?>/js/core.js" ></script>
        <script type="text/javascript" src="<?php echo CDN_CONTEXT_PATH ?>/js/datatable.js" ></script>
        <script type="text/javascript" src="<?php echo CDN_CONTEXT_PATH ?>/js/form.js" ></script>
        <script type="text/javascript" src="<?php echo CDN_CONTEXT_PATH ?>/js/history.js" ></script>
        <script type="text/javascript" src="<?php echo CDN_CONTEXT_PATH ?>/js/anchor.js" ></script>
        <script type="text/javascript" src="<?php echo CDN_CONTEXT_PATH ?>/js/validation.js" ></script>
        <script type="text/javascript" src="<?php echo CDN_CONTEXT_PATH ?>/js/dialog.js" ></script>
    </head>
    <body id="body">
        <section id="wrapper">
            <section class="navigation">
                <a class="logo" href="<?php echo CONTEXT_PATH ?>/doctors"><img src="<?php echo CONTEXT_PATH ?>/images/logo.png" alt="logo"/></a>
                <?php require BASE_PATH . '/views/doctors/includes/menu.php' ?>
            </section>
            <section class="content">
                <?php require BASE_PATH . '/views/doctors/includes/header.php' ?>
                <div class="dashboard">
                    <a class="dashboard" href="<?php echo CONTEXT_PATH ?>/doctors/profile">Update Profile</a>
                    <a class="dashboard" href="<?php echo CONTEXT_PATH ?>/guidelines-for-doctors/">Guidelines For Doctors</a>
                    <a class="dashboard" href="<?php echo CONTEXT_PATH ?>/doctors/change-password">Change Password</a>
                    <div class="clear"></div>
                    <a class="dashboard" href="<?php echo CONTEXT_PATH ?>/doctor-logout">Sign Out</a>
                    <div class="clear"></div>
                </div>
            </section>
        </section>
        <iframe id="iframe_fix" style="display: none"></iframe>
        <script language="javascript" type="text/javascript">
            var _base = '<?php echo CONTEXT_PATH ?>/';
            $(document).ready(function() {
                _plugins = new NT.Core.hashTable();
                var _history = $n(document.body).history({});
                $('.menu .tab').click(function(e) {
                    $(e.target).next('dl').siblings('dl').slideUp();
                    $(e.target).next('dl').slideToggle();
                });
            });
        </script>
    </body>
</html>