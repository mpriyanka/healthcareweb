<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title><?php echo $REQUEST_ATTRIBUTES['page']->getValue('title') ?></title>
        <link href="<?php echo CDN_CONTEXT_PATH ?>/css/style.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo CDN_CONTEXT_PATH ?>/css/data.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo CDN_CONTEXT_PATH ?>/css/fonts.css" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="<?php echo CDN_CONTEXT_PATH ?>/favicon.ico" type="image/ico" />
        <script type="text/javascript" src="<?php echo CDN_CONTEXT_PATH ?>/js/ext/jquery-1.7.1.min.js" ></script>
        <script type="text/javascript" src="<?php echo CDN_CONTEXT_PATH ?>/js/ext/fadeSlideShow-minified.js" ></script>
    </head>
    <body id="body">
        <?php
        require BASE_PATH . '/views/includes/header.php';
        require BASE_PATH . '/views/includes/banner.php';
        ?>
        <div id="content">
            <div class="wrapper">
                <div class="split-3-2">
                    <div class="block-content">
                        <h4><?php echo $REQUEST_ATTRIBUTES['page']->getValue('title') ?></h4>
                        <?php echo stripslashes(html_entity_decode($REQUEST_ATTRIBUTES['page']->getValue('body'))) ?>
                    </div>
                </div>
                <div class="split-3-1">
                    <?php require BASE_PATH . '/views/includes/right-ad.php'; ?>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <?php
        require BASE_PATH . '/views/includes/footer.php';
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                jQuery('#slides').fadeSlideShow({
                    PlayPauseElement: false,
                    NextElement: false,
                    PrevElement: false,
                    ListElement: false,
                    allowKeyboardCtrl: false,
                    interval: 5000,
                    width: '100%',
                    height: '25em'
                });
                jQuery('.article-highlights dd').click(function() {
                    jQuery('.article-highlights dd').removeClass('active');
                    $(this).addClass('active');
                    jQuery('.tabs .tab').hide();
                    jQuery('.tabs .' + $(this).attr('data')).show();
                })
            });
        </script>
    </body>
</html>