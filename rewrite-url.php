<?php

$url = strtok($_SERVER['REQUEST_URI'], "?");

$REQUEST_PATHS = $REQUEST_ATTRIBUTES = array();

$REQUEST_PATHS['QUERY_STRING'] = $_SERVER['QUERY_STRING'];


$REQUEST_PATHS['REQUEST_URI'] = $url;

define('REQUEST_URI', $REQUEST_PATHS['REQUEST_URI']);
define('QUERY_STRING', $REQUEST_PATHS['QUERY_STRING']);

$paths = explode('/', $REQUEST_PATHS['REQUEST_URI']);

for ($i = 0; $i < count($paths); $i++) {
    if (strpos($paths[$i], '?') > 0 && strpos($paths[$i], '.php') == false) {
        $paths[$i] = substr($paths[$i], 0, strpos($paths[$i], '?'));
    }
    if (strlen($paths[$i]) > 0 && substr($paths[$i], 0, 1) != '?' && strpos($paths[$i], '.php') == false) {
        if (strpos($paths[$i], '.htm') != false) {
            $paths[$i] = substr($paths[$i], 0, strpos($paths[$i], '.htm'));
        }
        $REQUEST_PATHS[] = $paths[$i];
    }
}

