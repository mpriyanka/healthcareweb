<?php

ini_set('session.use_only_cookies', true);
date_default_timezone_set('Africa/Lagos');
//ini_set('post_max_size', 100000);
//ini_set('upload_max_filesize', 100000);

define('BASE_PATH', __DIR__);

define('DEBUG_MODE', true);
define('CONTEXT_PATH', 'http://' . $_SERVER['SERVER_NAME']);
define('DB_USERNAME', '');
define('CONTACT_EMAIL', '');
define('SITE_NAME', "iDocare Health Care Application");
define('BILLING_EMAIL', "accounts@healthcare.org");
define('SUPPORT_EMAIL', "info@healthcare.org");
define('NOTIFICATION_EMAIL', "chat4zeal@yahoo.com,mishra.priyanka99@gmail.com");

define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASSWORD', '');
define('DB_NAME', 'health_care');
define('CDN_CONTEXT_PATH', 'http://' . $_SERVER['SERVER_NAME']);

define('DATA_PAGE_SIZE', 15);

define("MODEL_PAGE_TYPES", '{"Page":"Page"}');
define("ERROR_CODES", file_get_contents(BASE_PATH.'/error-codes.json'));

//SMTP
define('SMTP_HOST', 'smtp.gmail.com');
define('SMTP_PORT', 465);
define('SMTP_USERNAME', 'mail-forward@anytimestream.com');
define('SMTP_PASSWORD', 'adminadmin@2@2');

//GCM
define('API_KEY', 'AIzaSyC1mI0Iu7n957q8DgDeyrBcWrpxX61bxd0');
define('GCM_URL', 'https://android.googleapis.com/gcm/send');