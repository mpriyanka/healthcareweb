<?php

require 'config.php';
require 'rewrite-url.php';
include 'lib/phpmailer/class.phpmailer.php';
include 'lib/easyphpthumbnail.class.php';
require 'Models/autoloader.php';

session_start();

$viewControllers = array();

//frontend
$viewControllers[] = array('path' => '/about/*', 'controller' => 'page-controller.php');
$viewControllers[] = array('path' => '/contact/*', 'controller' => 'page-controller.php');
$viewControllers[] = array('path' => '/how-it-works/*', 'controller' => 'page-controller.php');
$viewControllers[] = array('path' => '/pricing/*', 'controller' => 'page-controller.php');
$viewControllers[] = array('path' => '/faqs/*', 'controller' => 'page-controller.php');
$viewControllers[] = array('path' => '/guidelines-for-doctors/*', 'controller' => 'page-controller.php');
$viewControllers[] = array('path' => '/terms/*', 'controller' => 'page-controller.php');
$viewControllers[] = array('path' => '/privacy-policy/*', 'controller' => 'page-controller.php');
$viewControllers[] = array('path' => '/doctor-signup/complete', 'controller' => 'doctor-signup-complete-controller.php');
$viewControllers[] = array('path' => '/doctor-signup', 'controller' => 'doctor-signup-controller.php');
$viewControllers[] = array('path' => '/doctor-email-verification/*', 'controller' => 'doctor-email-verification-controller.php');
$viewControllers[] = array('path' => '/dynamic-images/*', 'controller' => 'dynamic-image-controller.php');

//doctors
$viewControllers[] = array('path' => '/doctors', 'controller' => 'doctors/home-controller.php');

//Webservices
$viewControllers[] = array('path' => '/webservice/verify-phone', 'controller' => 'webservice/verify-phone-controller.php');
$viewControllers[] = array('path' => '/webservice/user-registration', 'controller' => 'webservice/user-registration-controller.php');
$viewControllers[] = array('path' => '/webservice/doc-login', 'controller' => 'webservice/doc-login-controller.php');
$viewControllers[] = array('path' => '/webservice/user-update-gcm-token', 'controller' => 'webservice/user-update-gcm-token-controller.php');
$viewControllers[] = array('path' => '/webservice/doc-update-gcm-token', 'controller' => 'webservice/doc-update-gcm-token-controller.php');
$viewControllers[] = array('path' => '/webservice/doc-lookup', 'controller' => 'webservice/doc-lookup-controller.php');
$viewControllers[] = array('path' => '/webservice/doc-profile-lookup/*', 'controller' => 'webservice/doc-profile-lookup-controller.php');
$viewControllers[] = array('path' => '/webservice/consultation-request', 'controller' => 'webservice/consultation-request-controller.php');
$viewControllers[] = array('path' => '/webservice/consultation', 'controller' => 'webservice/consultation-controller.php');
$viewControllers[] = array('path' => '/webservice/consultation-chat-message-doctor', 'controller' => 'webservice/consultation-chat-message-doctor-controller.php');
$viewControllers[] = array('path' => '/webservice/consultation-chat-message-doctor-get', 'controller' => 'webservice/consultation-chat-message-doctor-get-controller.php');
$viewControllers[] = array('path' => '/webservice/consultation-chat-message-user', 'controller' => 'webservice/consultation-chat-message-user-controller.php');
$viewControllers[] = array('path' => '/webservice/consultation-chat-message-user-get', 'controller' => 'webservice/consultation-chat-message-user-get-controller.php');
$viewControllers[] = array('path' => '/webservice/ccm-user-upload-image', 'controller' => 'webservice/ccm-user-upload-image-controller.php');
$viewControllers[] = array('path' => '/webservice/ccm-user-get-image', 'controller' => 'webservice/ccm-user-get-image-controller.php');
$viewControllers[] = array('path' => '/webservice/ccm-user-download-image', 'controller' => 'webservice/ccm-user-download-image-controller.php');
$viewControllers[] = array('path' => '/webservice/ccm-doctor-upload-image', 'controller' => 'webservice/ccm-doctor-upload-image-controller.php');
$viewControllers[] = array('path' => '/webservice/ccm-doctor-get-image', 'controller' => 'webservice/ccm-doctor-get-image-controller.php');
$viewControllers[] = array('path' => '/webservice/ccm-doctor-download-image', 'controller' => 'webservice/ccm-doctor-download-image-controller.php');

//site management
$viewControllers[] = array('path' => '/backend/site-management/pages/*', 'controller' => 'backend/site-management/page.php');
$viewControllers[] = array('path' => '/backend/site-management/images/*', 'controller' => 'backend/site-management/image.php');
$viewControllers[] = array('path' => '/backend/site-management/doctors/*', 'controller' => 'backend/site-management/doctor.php');

//roles & permissions
$viewControllers[] = array('path' => '/backend/roles-and-permissions/roles/*', 'controller' => 'backend/roles-and-permissions/role.php');
$viewControllers[] = array('path' => '/backend/roles-and-permissions/permissions/*', 'controller' => 'backend/roles-and-permissions/permission.php');
$viewControllers[] = array('path' => '/backend/roles-and-permissions/role-permissions/*', 'controller' => 'backend/roles-and-permissions/role-permission.php');

//user management
$viewControllers[] = array('path' => '/backend/user-management/users/*', 'controller' => 'backend/user-management/user.php');
$viewControllers[] = array('path' => '/backend/user-management/user-profiles/*', 'controller' => 'backend/user-management/user-profile.php');
$viewControllers[] = array('path' => '/backend/user-management/user-roles/*', 'controller' => 'backend/user-management/user-role.php');

//login/logout controllers
$viewControllers[] = array('path' => '/login', 'controller' => 'login-controller.php');
$viewControllers[] = array('path' => '/backend/change-password', 'controller' => 'backend/change-password-controller.php');
$viewControllers[] = array('path' => '/logout', 'controller' => 'logout-controller.php');
$viewControllers[] = array('path' => '/doctor-login', 'controller' => 'doctor-login-controller.php');
$viewControllers[] = array('path' => '/doctor-logout', 'controller' => 'doctor-logout-controller.php');
$viewControllers[] = array('path' => '/doc-forgot-password', 'controller' => 'doc-forgot-password-controller.php');
$viewControllers[] = array('path' => '/doctor-complete-recovery/*', 'controller' => 'doctor-complete-recovery-controller.php');
$viewControllers[] = array('path' => '/doctors/change-password', 'controller' => 'doctors/change-password-controller.php');

$viewControllers[] = array('path' => '/backend/*', 'controller' => 'backend/dashboard.php');

//default controller
$defaultViewController = array('path' => '/', 'controller' => 'home.php');


//404 controller
$_404ViewController = array('path' => '/', 'controller' => '404-controller.php');;

dispatchRequest();

function dispatchRequest() {
    $uri = REQUEST_URI;
    if (strpos($uri, 'dispatcher.php') > 0) {
        header('location: ' . CONTEXT_PATH);
        exit;
    }
    $strpos = strpos($uri, '?');
    if (strlen($strpos) > 0) {
        $uri = substr($uri, 0, $strpos);
    }
    $viewController = getViewController($uri);
    if (isset($viewController['controller'])) {
        require 'controllers/' . $viewController['controller'];
    } else {
        require 'views/' . $viewController['view'];
    }
}

function getViewController($uri) {
    global $viewControllers;
    global $defaultViewController;
    global $_404ViewController;

    if ($uri == '/') {
        return $defaultViewController;
    }

    foreach ($viewControllers as $viewController) {
        $strpos = strpos($viewController['path'], '/*');
        if (strlen($strpos) > 0) {
            if (substr($uri, 0, $strpos) == substr($viewController['path'], 0, $strpos)) {
                return $viewController;
            }
        } else {
            if ($uri == $viewController['path'] || $uri == $viewController['path'] . '/') {
                return $viewController;
            }
        }
    }

    return $_404ViewController;
}
